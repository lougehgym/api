const request = require('supertest')
const express = require('express')
const app = express();
const routeMerchandise = require('../../../routes/merchandise');
const routeLogin = require('../../../routes/login');
const bodyParser = require('body-parser')
app.use(bodyParser.json())

app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(routeMerchandise)

app.use(routeLogin)
let token;
beforeEach((done) => {
    return request(app)
        .post('/login')
        .send({ id: 2, password: "1234" })
        .type('form')
        .end((err, response) => {
            if (err) {
                console.log(err)
                done();
            }
            token = response.body.token;
            done();
        })
})
describe('put /api/merchandise', () => {
    it('should update new Merchandise', (done) => {
        return request(app)
            .put('/api/merchandise/11',
            ).send({ name: "Gatorade", price: 1200, }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(200, done)
    })

    it('should not update if id is a character', (done) => {

        return request(app)
            .put('/api/merchandise/abc',
            ).send({ name: "Ice", price: 6400 }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, 'ID must be Number', done)
    })
    it('should not update if name is null', (done) => {

        return request(app)
            .put('/api/merchandise/24',
            ).send({ name: null, price: 6400 }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, 'Name must be provided', done)
    })
    it('should not update if name is a Number', (done) => {

        return request(app)
            .put('/api/merchandise/24',
            ).send({ name: 1234, price: 6400, }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, 'Name must not be a number', done)
    })
    it('should not update if price is a character', (done) => {

        return request(app)
            .put('/api/merchandise/24',
            ).send({ name: "asd", price: "qwe", }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, 'Price must be a Number', done)
    })
    it('should not update if price is null', (done) => {

        return request(app)
            .put('/api/merchandise/24',
            ).send({ name: null, price: null, }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })

    it('should not update if name is too long', (done) => {

        return request(app)
            .put('/api/merchandise/24',
            ).send({
                name: `Lorem ipsum dolor sit amet,
                consectetur adipiscing elit, sed do eiusmod 
                tempor incididunt ut labore et dolore magna
                aliqua. Ut enim ad minim veniam, quis 
                nostrud exercitation ullamco laboris nisi
                ut aliquip ex ea commodo consequat. 
                Duis aute irure dolor in reprehenderit
                in voluptate velit esse cillum dolore
                eu fugiat nulla pariatur. Excepteur
                sint occaecat cupidatat non proident, 
                sunt in culpa qui officia deserunt 
                mollit anim id est laborum.`,
                price: null,
            }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
})