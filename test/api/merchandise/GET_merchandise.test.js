const request = require('supertest')
const express = require('express')
const app = express();
const routeMerchandise = require('../../../routes/merchandise');
const routeMerchandise = require('../../../routes/login');
const bodyParser = require('body-parser')
app.use(bodyParser.json())

app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(routeMerchandise)

app.use(routeLogin)
let token;
beforeEach((done) => {
    return request(app)
        .post('/login')
        .send({ id: 2, password: "1234" })
        .type('form')
        .end((err, response) => {
            if (err) {
                console.log(err)
                done();
            }
            token = response.body.token;
            done();
        })
})
describe('GET /api/merchandise/', () => {
    // afterAll(async done=>done())

    it('should display 400 status code if no merchandise found ', (done) => {

        return request(app)
            .get('/api/merchandise/45')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, 'No Merchandise found', done)
    })


    it(`Should not allow characters when fetching a merchandise`, (done) => {

        return request(app)
            .get('/api/merchandise/abc')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, `ID must be a number`, done)
    })

    it('should fetch all data', (done) => {
        return request(app)
        .get('/api/merchandise')
        .set('Authorization', `Bearer ${token}`)
        .expect(200, done)
    })

    it('should fetch a specific data', (done) => {
        return request(app)
        .get('/api/merchandise/7')
        .set('Authorization', `Bearer ${token}`)
        .expect(200, done)
    })
})