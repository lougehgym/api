const request = require('supertest')
const express = require('express')
const app = express();
const routeMerchandise = require('../../../routes/merchandise');
const routeLogin = require('../../../routes/login');
const bodyParser = require('body-parser')
app.use(bodyParser.json())

app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(routeMerchandise)
app.use(routeLogin)
let token;
beforeEach((done) => {
    return request(app)
        .post('/login')
        .send({ id: 2, password: "1234" })
        .type('form')
        .end((err, response) => {
            if (err) {
                console.log(err)
                done();
            }
            token = response.body.token;
            done();
        })
})

describe('DELETE /api/merchandise/', () => {
    // it('should delete a member', (done) => {
    //     return request(app)
    //         .delete('/api/members/45')
    //         .expect(200, done)
    // })
    it('should receive status code 400 if merchandise not found', (done) => {
        return request(app)
            .delete('/api/merchandise/68')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
    it('should not allow null values(400)', (done) => {
        return request(app)
            .delete(`/api/merchandise/${null}`)
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
    it('should not allow characters to delete a merchandise(400)', (done) => {
        return request(app)
            .delete('/api/merchandise/abc')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })

    it('should not allow to delete a merchandise without token', (done) => {
        return request(app)
            .delete('/api/merchandise/abc')
            .expect(403, done)
    })

})
