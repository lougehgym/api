
const request = require('supertest')
const express = require('express')
const app = express();
const routeMember = require('../../../routes/members');
const routeLogin = require('../../../routes/login')
const bodyParser = require('body-parser')
app.use(bodyParser.json())

app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(routeMember)
app.use(routeLogin)
let token
beforeEach((done) => {
    return request(app)
        .post('/login')
        .send({ id: 2, password: "1234" })
        .type('form')
        .end((err, response) => {
            if (err) {
                console.log(err)
                done();
            }
            token = response.body.token;
            done();
        })
})
describe('PUT /api/members', () => {

    it('should update a member', (done) => {
        request(app)
            .put('/api/members/68')
            .send({ name: "John", zip: 5432, membership_date: '2019-05-30' })
            .type('form')
            .set({ "authorization": `Bearer ${token}` })
            .expect(200, done)
    })
    it('should not allow characters', (done) => {
        request(app)
            .put('/api/members/asd')
            .send({ name: "John", zip: 5432, membership_date: '2019-05-30' })
            .type('form')
            .set({ "authorization": `Bearer ${token}` })
            .expect(400, done)
    })
    it('should not update a member if it does not exists', (done) => {
        request(app)
            .put('/api/members/64')
            .send({ name: "John", zip: 5432, membership_date: '2019-05-30' })
            .type('form')
            .set({ "authorization": `Bearer ${token}` })
            .expect(400, done)
    })

    it('should not update a name is Null', (done) => {
        request(app)
            .put('/api/members/68')
            .send({ name: null, zip: 5432, membership_date: '2019-05-30' })
            .type('form')
            .set({ "authorization": `Bearer ${token}` })
            .expect(400, done)
    })
    it('should not update a name is Empty', (done) => {
        request(app)
            .put('/api/members/68')
            .send({ name: "", zip: 5432, membership_date: '2019-05-30' })
            .type('form')
            .set({ "authorization": `Bearer ${token}` })
            .expect(400, done)
    })

    it('should not update a zip if it is a String', (done) => {
        request(app)
            .put('/api/members/24')
            .send({ name: "abc", zip: "abc", membership_date: '2019-05-30' })
            .type('form')
            .set({ "authorization": `Bearer ${token}` })
            .expect(400, done)
    })
    it('should not update a zip if Date is incorrect format', (done) => {
        request(app)
            .put('/api/members/24')
            .send({ name: null, zip: "5432", membership_date: 'abc-05-30' })
            .type('form')
            .set({ "authorization": `Bearer ${token}` })
            .expect(400, done)
    })
    it('should not update if no token provided', (done) => {
        request(app)
            .put('/api/members/24')
            .send({ name: null, zip: "5432", membership_date: 'abc-05-30' })
            .type('form')
            .set({ "authorization": `Bearer ${token}` })
            .expect(400, done)
    })
})
