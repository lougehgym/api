const request = require('supertest')
const express = require('express')
const app = express();
const routeMember = require('../../../routes/members');
const routeLogin = require('../../../routes/login');
const bodyParser = require('body-parser')
const pgtool = require('pgtools')
const members = require('../../../database/members')
app.use(bodyParser.json())

app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(routeLogin)
app.use(routeMember)
let token;

beforeAll((done) => {

    pgtool.createdb({ ...config.test }, 'fitness_db_test')
    done();
})

beforeEach((done) => {
    members.createTable();
    done();
})
describe('Initialize Database Test', () => {

    beforeEach((done) => {
        request(app)
            .post('/login')
            .send({ id: 1, password: "admin" })
            .expect(200)
            .end((err, response) => {
                if (err) {
                    console.error(err)
                    done();
                }
                token = response.body.token;
                done();
            })
    })
    describe('POST MEMBERS', () => {
        it('should add new member', (done) => {
            return request(app)
                .post('/api/members/')
                .send({ name: "Paul", zip: 5000, membership_date: "2019-04-05" }).type('form')
                .set({ "authorization": `Bearer ${token}` })
                .expect(201, { data: `Added Paul with zipcode 5000` }, done)
        })

        it('should not add if name is null', (done) => {

            return request(app)
                .post('/api/members/')
                .send({ name: null, zip: "asd", membership_date: "2019-04-05" })
                .type('form')
                .set({ "authorization": `Bearer ${token}` })
                .expect(400, 'Name must be provided', done)
        })
        it('should not add if zip is String', (done) => {

            return request(app)
                .post('/api/members/')
                .send({ name: null, zip: "qwe", membership_date: "2019-04-05" })
                .set({ "authorization": `Bearer ${token}` })
                .type('form')
                .expect(400, done)
        })
        it('should not add if Date format is incorrect', (done) => {

            return request(app)
                .post('/api/members/')
                .send({ name: "Joseph", zip: 1234, membership_date: "1234" })
                .type('form')
                .set({ "authorization": `Bearer ${token}` })
                .expect(400, "Membership Date must be in a proper format", done)
        })
    })
})