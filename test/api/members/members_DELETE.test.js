const request = require('supertest')
const express = require('express')
const app = express();
const routeMember = require('../../../routes/members');
const routeLogin = require('../../../routes/login')
const bodyParser = require('body-parser')
const pgtool = require('pgtools')
const config = require('../../../database/config')
const members = require('../../../database/members')
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(routeMember)
app.use(routeLogin)
let token
beforeAll((done) => {
    pgtool.createdb({ ...config.test }, 'fitness_db_test')
    done();
})
beforeEach((done) => {
    members.createTable();
    done();
})
describe('Initialize Test Database', () => {
    beforeAll(async (done) => {
        await members.insertQuery({ name: "John", membership_date: null, zip: 3600 })
        done();
    })
    describe('DELETE members', () => {

        beforeEach((done) => {
            return request(app)
                .post('/login')
                .send({ id: 2, password: "1234" })
                .type('form')
                .end((err, response) => {
                    if (err) {
                        done();
                    }
                    token = response.body.token;
                    done();
                })
        })
        it('should delete a member', (done) => {
            request(app)
                .delete('/api/members/45')
                .set({ "authorization": `Bearer ${token}` })
                .expect(200, done)
        })
        it('should receive status code 400 if member not found', (done) => {
            request(app)
                .delete('/api/members/999')
                .set({ "authorization": `Bearer ${token}` })
                .expect(400, done)
        })
        it('should not allow null values(400)', (done) => {
            request(app)
                .delete(`/api/members/${null}`)
                .set({ "authorization": `Bearer ${token}` })
                .expect(400, done)
        })
        it('should not allow characters to delete a member(400)', (done) => {
            request(app)
                .delete('/api/members/abc')
                .set({ "authorization": `Bearer ${token}` })
                .expect(400, done)
        })
    })
})
