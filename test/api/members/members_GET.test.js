const request = require('supertest')
const express = require('express')
const app = express();
const routeMember = require('../../../routes/members');
const routeLogin = require('../../../routes/login')
const bodyParser = require('body-parser')
app.use(bodyParser.json())

app.use(bodyParser.urlencoded({
        extended: true
}))
app.use(routeMember)
app.use(routeLogin)
let token;
beforeEach((done) => {
        return request(app)
                .post('/login')
                .send({ id: 2, password: "1234" })
                .type('form')
                .end((err, response) => {
                        if (err) {
                                console.log(err)
                                done();
                        }
                        token = response.body.token;
                        done();
                })
})
describe('GET /api/members/', () => {


        it('should display 400 status code if no member found ', (done) => {

                request(app)
                        .get('/api/members/7')
                        .set('Authorization', `Bearer ${token}`)
                        .expect(400, done)
        })


        it(`Should not allow characters when fetching a member`, (done) => {

                request(app)
                        .get('/api/members/abc')
                        .set('Authorization', `Bearer ${token}`)
                        .expect(400, done)
        })

        it('should fetch all data', (done) => {
                request(app)
                        .get('/api/members')
                        .set('Authorization', `Bearer ${token}`)
                        .expect(200, done)
        })


        it('should fetch a specific data', (done) => {
                request(app)
                        .get('/api/members/68')
                        .set('Authorization', `Bearer ${token}`)
                        .expect(200, done)
        })
        it('should not fetch without JWT', (done) => {
                request(app)
                        .get('/api/members')
                        .expect(403, done)
        })
})