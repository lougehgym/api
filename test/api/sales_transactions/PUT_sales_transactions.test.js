const request = require('supertest')
const express = require('express')
const app = express();
const routeSalesTransaction = require('../../../routes/sales_transaction');
const routeLogin = require('../../../routes/login');
const bodyParser = require('body-parser')
app.use(bodyParser.json())

app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(routeSalesTransaction)
app.use(routeLogin)

let token;
beforeEach((done) => {
    return request(app)
        .post('/login')
        .send({ id: 2, password: "1234" })
        .type('form')
        .end((err, response) => {
            if (err) {
                console.log(err)
                done();
            }
            token = response.body.token;
            done();
        })
})
describe('put /api/sales_transaction', () => {
    // it('should update new sales_transaction', (done) => {
    //     return request(app)
    //         .put('/api/sales_transaction/26',
    //         ).send({ merchandise_id: "2019-09-23", transaction_id: 32 }).type('form')
    //         .set('Authorization', `Bearer ${token}`)
    //         .expect(200, done)
    // })

    it('should not update if sales_transaction not found', (done) => {

        return request(app)
            .put('/api/sales_transaction/100',
            ).send({ transaction_id: 12, merchandise_id: 4, quantity_items: 2 }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
    it('should not update if id is a character', (done) => {

        return request(app)
            .put('/api/sales_transaction/abc',
            ).send({ transaction_id: 12, merchandise_id: 4, quantity_items: 2 }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
    it('should not update if transaction id is null', (done) => {

        return request(app)
            .put('/api/sales_transaction/2',
            ).send({ transaction_id: null, merchandise_id: 4, quantity_items: 2 }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })

    it('should not update if transaction id is character', (done) => {

        return request(app)
            .put('/api/sales_transaction/2',
            ).send({ transaction_id: "abc", merchandise_id: 4, quantity_items: 2 }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
    it('should not update if merchandise ID is null', (done) => {

        return request(app)
            .put('/api/sales_transaction/2',
            ).send({ transaction_id: 12, merchandise_id: null, quantity_items: 2 }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
    it('should not update if merchandise_id is character', (done) => {

        return request(app)
            .put('/api/sales_transaction/2',
            ).send({ transaction_id: 12, merchandise_id: "abc", quantity_items: 2 }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
    it('should not update if quantity items is character', (done) => {

        return request(app)
            .put('/api/sales_transaction/2',
            ).send({ transaction_id: 12, merchandise_id: 4, quantity_items: "abc" }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
    it('should not update if quantity items null', (done) => {

        return request(app)
            .put('/api/sales_transaction/2',
            ).send({ transaction_id: 12, merchandise_id: 4, quantity_items: null }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
})