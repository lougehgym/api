const request = require('supertest')
const express = require('express')
const app = express();
const routeSalesTransaction = require('../../../routes/sales_transaction');
const routeLogin = require('../../../routes/login');
const bodyParser = require('body-parser')
app.use(bodyParser.json())

app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(routeSalesTransaction)
app.use(routeLogin)

let token;
beforeEach((done) => {
    return request(app)
        .post('/login')
        .send({ id: 2, password: "1234" })
        .type('form')
        .end((err, response) => {
            if (err) {
                console.log(err)
                done();
            }
            token = response.body.token;
            done();
        })
})
describe('POST /api/sales_transaction/', () => {
    // it('should add new sales_transaction', (done) => {
    //     return request(app)
    //         .post('/api/sales_transaction/',
    //         ).send({ merchandise_id: "2019-09-23", transaction_id: 32 }).type('form')
    // .set('Authorization', `Bearer ${token}`)
    //         .expect(201, done)
    // })

    it('should not add if transaction_id is null', (done) => {

        return request(app)
            .post('/api/sales_transaction/',
            ).send({ transaction_id: null, merchandise_id: 4, quantity_items: 2 }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
    it('should not add if transaction_id is a character', (done) => {

        return request(app)
            .post('/api/sales_transaction/',
            ).send({ merchandise_id: "2019-09-16", transaction_id: "abc", quantity_items: 4 }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
    it('should not add if merchandise_id is a character', (done) => {

        return request(app)
            .post('/api/sales_transaction/',
            ).send({ merchandise_id: "qwe", transaction_id: 4, quantity_items: 2 }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
    it('should not add if merchandise_id is null', (done) => {

        return request(app)
            .post('/api/sales_transaction/',
            ).send({ merchandise_id: null, transaction_id: 4, quantity_items: 2 }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })

    it('should not add if quantity items is a character', (done) => {

        return request(app)
            .post('/api/sales_transaction/',
            ).send({

                merchandise_id: 12345,
                transaction_id: 4,
                quantity_items: "abc"
            }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })

    it('should not add if quantity items is a null', (done) => {

        return request(app)
            .post('/api/sales_transaction/',
            ).send({

                merchandise_id: 12345,
                transaction_id: 4,
                quantity_items: null
            }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
})