const request = require('supertest')
const express = require('express')
const app = express();
const routeOneDayPass = require('../../../routes/oneDayPass');
const routeLogin = require('../../../routes/login');
const bodyParser = require('body-parser')
app.use(bodyParser.json())

app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(routeOneDayPass)

app.use(routeLogin)
let token;
beforeEach((done) => {
    return request(app)
        .post('/login')
        .send({ id: 2, password: "1234" })
        .type('form')
        .end((err, response) => {
            if (err) {
                console.log(err)
                done();
            }
            token = response.body.token;
            done();
        })
})
describe('POST /api/one_day_pass/', () => {
    beforEach(() => {
        //Create table
    })
    // it('should add new one_day_pass', (done) => {
    //     return request(app)
    //         .post('/api/one_day_pass/',
    //         ).send({ member_id: 32, pass_date: `2019-09-23`, passcat_id: 9 }).type('form')
    //         .expect(201, done)
    // })

    it('should not add if member_id is null', (done) => {

        return request(app)
            .post('/api/one_day_pass/',
            ).send({ member_id: null, pass_date: `2019-09-23`, passcat_id: 9 }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
    it('should not add if member is a character', (done) => {

        return request(app)
            .post('/api/one_day_pass/',
            ).send({ member_id: "abc", pass_date: `2019-09-23`, passcat_id: 9 }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
    it('should not add if date is a character', (done) => {

        return request(app)
            .post('/api/one_day_pass/',
            ).send({ member_id: 32, pass_date: `abc`, passcat_id: 9 }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
    it('should not add if date is null', (done) => {

        return request(app)
            .post('/api/one_day_pass/',
            ).send({ member_id: 32, pass_date: null, passcat_id: 9 }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })

    it('should not add if date is a number', (done) => {

        return request(app)
            .post('/api/one_day_pass/',
            ).send({ member_id: 32, pass_date: 12345, passcat_id: 9 }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
    it('should not add if date is incorrect format', (done) => {

        return request(app)
            .post('/api/one_day_pass/',
            ).send({ member_id: 32, pass_date: `01-01-1998`, passcat_id: 9 }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
})