const request = require('supertest')
const express = require('express')
const app = express();
const routeMember = require('../../../routes/oneDayPass');
const routeLogin = require('../../../routes/login');
const bodyParser = require('body-parser')
app.use(bodyParser.json())

app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(routeMember)

app.use(routeLogin)
let token;
beforeEach((done) => {
    return request(app)
        .post('/login')
        .send({ id: 2, password: "1234" })
        .type('form')
        .end((err, response) => {
            if (err) {
                console.log(err)
                done();
            }
            token = response.body.token;
            done();
        })
})
describe('DELETE /api/one_day_pass/', () => {
    // it('should delete a member', (done) => {
    //     return request(app)
    //         .delete('/api/members/45')
    //         .expect(200, done)
    // })
    it('should receive status code 400 if Day Pass not found', (done) => {
        return request(app)
            .delete('/api/one_day_pass/68')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
    it('should not allow null values(400)', (done) => {
        return request(app)
            .delete(`/api/one_day_pass/${null}`)
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
    it('should not allow characters to delete a oneDayPass(400)', (done) => {
        return request(app)
            .delete('/api/one_day_pass/abc')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })

    it('should  delete a oneDayPass without token', (done) => {
        return request(app)
            .delete('/api/one_day_pass/abc')
            .expect(403, done)
    })

})
