const request = require('supertest')
const express = require('express')
const app = express();
const routeOneDayPass = require('../../../routes/oneDayPass');
const routeLogin = require('../../../routes/login');
const bodyParser = require('body-parser')
app.use(bodyParser.json())

app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(routeOneDayPass)

app.use(routeLogin)
let token;
beforeEach((done) => {
    return request(app)
        .post('/login')
        .send({ id: 2, password: "1234" })
        .type('form')
        .end((err, response) => {
            if (err) {
                console.log(err)
                done();
            }
            token = response.body.token;
            done();
        })
})
describe('GET /api/one_day_pass/', () => {
    // afterAll(async done=>done())

    it('should display 400 status code if no one_day_pass found ', (done) => {

        return request(app).get('/api/one_day_pass/100')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })

    it(`Should not allow characters when fetching a one_day_pass`, (done) => {

        return request(app).get('/api/one_day_pass/abc')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })

    it('should fetch all data', (done) => {
        return request(app).get('/api/one_day_pass')
            .set('Authorization', `Bearer ${token}`)
            .expect(200, done)
    })

    it('should fetch a specific data', (done) => {
        return request(app).get('/api/one_day_pass/10')
            .set('Authorization', `Bearer ${token}`).expect(200, done)
    })
    it('should not fetch a specific data if token is not provided', (done) => {
        return request(app).get('/api/one_day_pass/10').expect(403, done)
    })
    it('should not fetch a specific data if token is not decodable', (done) => {
        return request(app).get('/api/one_day_pass/10')
            .set('Authorization', `Bearer eyfasklfdja`).expect(403, done)
    })

    it('should not fetch a specific data if token is not decodable', (done) => {
        return request(app).get('/api/one_day_pass/10')
            .set('Authorization', `Bearer eyfasklfdja`).expect(403, done)
    })
})