const request = require('supertest')
const express = require('express')
const app = express();
const routePassCategory = require('../../../routes/passCategory');
const routeLogin = require('../../../routes/login');
const bodyParser = require('body-parser')
app.use(bodyParser.json())

app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(routePassCategory)

app.use(routeLogin)
let token;
beforeEach((done) => {
    return request(app)
        .post('/login')
        .send({ id: 2, password: "1234" })
        .type('form')
        .end((err, response) => {
            if (err) {
                console.log(err)
                done();
            }
            token = response.body.token;
            done();
        })
})

describe('GET /api/pass_category/', () => {
    // afterAll(async done=>done())

    it('should display 400 status code if no Pass Category found ', (done) => {

        return request(app).get('/api/pass_category/45')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })


    it(`Should not allow characters when fetching a Pass Category`, (done) => {

        return request(app).get('/api/pass_category/abc')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })

    it('should fetch all data', (done) => {
        return request(app).get('/api/pass_category')
            .set('Authorization', `Bearer ${token}`).expect(200, done)
    })

    it('should fetch a specific data', (done) => {
        return request(app).get('/api/pass_category/9')
            .set('Authorization', `Bearer ${token}`).expect(200, done)
    })
    it('should fetch not fetch specific data if no token provided', (done) => {
        return request(app).get('/api/pass_category/9').expect(403, done)
    })
    it('should fetch not fetch  data if no token provided', (done) => {
        return request(app).get('/api/pass_category').expect(403, done)
    })
})