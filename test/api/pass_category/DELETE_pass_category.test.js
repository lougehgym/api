const request = require('supertest')
const express = require('express')
const app = express();
const routePassCategory = require('../../../routes/passCategory');
const routeLogin = require('../../../routes/login');
const bodyParser = require('body-parser')
app.use(bodyParser.json())

app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(routePassCategory)

app.use(routeLogin)
let token;
beforeEach((done) => {
    return request(app)
        .post('/login')
        .send({ id: 2, password: "1234" })
        .type('form')
        .end((err, response) => {
            if (err) {
                console.log(err)
                done();
            }
            token = response.body.token;
            done();
        })
})

describe('DELETE /api/pass_category/', () => {
    // it('should delete a member', (done) => {
    //     return request(app)
    //         .delete('/api/members/45')
    //         .expect(200, done)
    // })
    it('should receive status code 400 if pass_category not found', (done) => {
        return request(app)
            .delete('/api/pass_category/68')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
    it('should not allow null values(400)', (done) => {
        return request(app)
            .delete(`/api/pass_category/${null}`)
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
    it('should not allow characters to delete a pass_category(400)', (done) => {
        return request(app)
            .delete('/api/pass_category/abc')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
    it('should not to delete a pass_category if no token provided ', (done) => {
        return request(app)
            .delete('/api/pass_category/abc')
            .expect(403, done)
    })


})
