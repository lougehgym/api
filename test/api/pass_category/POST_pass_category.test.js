const request = require('supertest')
const express = require('express')
const app = express();
const routePassCategory = require('../../../routes/passCategory');
const bodyParser = require('body-parser')
app.use(bodyParser.json())

app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(routePassCategory)

app.use(routeLogin)
let token;
beforeEach((done) => {
    return request(app)
        .post('/login')
        .send({ id: 2, password: "1234" })
        .type('form')
        .end((err, response) => {
            if (err) {
                console.log(err)
                done();
            }
            token = response.body.token;
            done();
        })
})
describe('POST /api/pass_category/', () => {
    // it('should add new Pass Category', (done) => {
    //     return request(app)
    //         .post('/api/pass_category/',
    //         ).send({ name: "Paul", price: 5000, member_id: "2019-04-05" }).type('form')
    //         .expect(201, { data: `Added Paul with pricecode 5000` }, done)
    // })

    it('should not add if name is null', (done) => {

        return request(app)
            .post('/api/pass_category/',
            ).send({ name: null, price: 6400, member_id: 24 }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
    it('should not add if name is a Number', (done) => {

        return request(app)
            .post('/api/pass_category/',
            ).send({ name: 1234, price: 6400, member_id: 24 }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
    it('should not add if price is a character', (done) => {

        return request(app)
            .post('/api/pass_category/',
            ).send({ name: "asdasd", price: "qwe", member_id: 4 }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
    it('should not add if price is null', (done) => {

        return request(app)
            .post('/api/pass_category/',
            ).send({ name: null, price: null, member_id: 4 }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })

    it('should not add if name is too long', (done) => {

        return request(app)
            .post('/api/pass_category/',
            ).send({
                name: `Lorem ipsum dolor sit amet,
                consectetur adipiscing elit, sed do eiusmod 
                tempor incididunt ut labore et dolore magna
                aliqua. Ut enim ad minim veniam, quis 
                nostrud exercitation ullamco laboris nisi
                ut aliquip ex ea commodo consequat. 
                Duis aute irure dolor in reprehenderit
                in voluptate velit esse cillum dolore
                eu fugiat nulla pariatur. Excepteur
                sint occaecat cupidatat non proident, 
                sunt in culpa qui officia deserunt 
                mollit anim id est laborum.`,
                price: null,
                member_id: 4
            }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
})