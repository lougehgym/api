const request = require('supertest')
const express = require('express')
const app = express();
const routePassCategory = require('../../../routes/passCategory');
const routeLogin = require('../../../routes/login');
const bodyParser = require('body-parser')
app.use(bodyParser.json())

app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(routePassCategory)
app.use(routeLogin)


beforeEach((done) => {
    return request(app)
        .post('/login')
        .send({ id: 2, password: "1234" })
        .type('form')
        .end((err, response) => {
            if (err) {
                console.log(err)
                done();
            }
            token = response.body.token;
            done();
        })
})

describe('put /api/pass_category', () => {
    // it('should update new member', (done) => {
    //     return request(app)
    //         .put('/api/pass_category/',
    //         ).send({ category_name: "Paul", price: 5000, 019-04-05" }).type('form')
    //         .expect(201, { data: `updateed Paul with pricecode 5000` }, done)
    // })

    it('should not update if Pass Category does not exists', (done) => {

        return request(app)
            .put('/api/pass_category/123',
            ).send({ category_name: "Ice", price: 6400 }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
    it('should not update if id is a character', (done) => {

        return request(app)
            .put('/api/pass_category/abc',
            ).send({ category_name: "Ice", price: 6400 }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
    it('should not update if category_name is null', (done) => {

        return request(app)
            .put('/api/pass_category/24',
            ).send({ category_name: null, price: 6400 }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
    it('should not update if category_name is a Number', (done) => {

        return request(app)
            .put('/api/pass_category/24',
            ).send({ category_name: 1234, price: 6400, }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
    it('should not update if price is a character', (done) => {

        return request(app)
            .put('/api/pass_category/24',
            ).send({ category_name: "asd", price: "qwe", }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
    it('should not update if price is null', (done) => {

        return request(app)
            .put('/api/pass_category/24',
            ).send({ category_name: null, price: null, }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })

    it('should not update if category_name is too long', (done) => {

        return request(app)
            .put('/api/pass_category/24',
            ).send({
                category_name: `Lorem ipsum dolor sit amet,
                consectetur adipiscing elit, sed do eiusmod 
                tempor incididunt ut labore et dolore magna
                aliqua. Ut enim ad minim veniam, quis 
                nostrud exercitation ullamco laboris nisi
                ut aliquip ex ea commodo consequat. 
                Duis aute irure dolor in reprehenderit
                in voluptate velit esse cillum dolore
                eu fugiat nulla pariatur. Excepteur
                sint occaecat cupidatat non proident, 
                sunt in culpa qui officia deserunt 
                mollit anim id est laborum.`,
                price: null,
            }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
})