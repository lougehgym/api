const request = require('supertest')
const express = require('express')
const app = express();
const routeLogin = require('../../../routes/login');
const bodyParser = require('body-parser')
let token = require('../../token');
app.use(bodyParser.json())

app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(routeLogin)


describe('POST /login', () => {
    test('Should log in', (done) => {
        return request(app)
            .post('/login')
            .send({ id: 1, password: "admin" })
            .expect(200)
            .end((err, response) => {
                token = response.body.token;
                done();
            })
    })
})