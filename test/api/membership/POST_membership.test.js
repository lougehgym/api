const request = require('supertest')
const express = require('express')
const app = express();
const routeMembership = require('../../../routes/membership');
const routeLogin = require('../../../routes/login')
const bodyParser = require('body-parser')
app.use(bodyParser.json())

app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(routeMembership)
app.use(routeLogin)
beforeEach((done) => {
    return request(app)
        .post('/login')
        .send({ id: 2, password: "1234" })
        .type('form')
        .end((err, response) => {
            if (err) {
                console.log(err)
                done();
            }
            token = response.body.token;
            done();
        })
})

describe('POST /api/membership/add', () => {
    it('should add new member', (done) => {
        return request(app)
            .post('/api/membership/',
            ).send({ name: "6 months", price: 1200, member_id: "2019-04-05" }).type('form')
            .expect(201, { data: `Added Paul with pricecode 5000` }, done)
    })

    it('should not add if name is null', (done) => {

        return request(app)
            .post('/api/membership/',
            ).send({ name: null, price: 6400, member_id: 24 }).type('form')
            .set({ "authorization": `Bearer ${token}` })
            .expect(400, 'Membership name must be provided', done)
    })
    it('should not add if name is a Number', (done) => {

        return request(app)
            .post('/api/membership/',
            ).send({ name: 1234, price: 6400, member_id: 24 }).type('form')
            .set({ "authorization": `Bearer ${token}` })
            .expect(400, 'Membership name must not be a number', done)
    })
    it('should not add if price is String', (done) => {

        return request(app)
            .post('/api/membership/',
            ).send({ name: "abc", price: "qwe", member_id: 4 }).type('form')
            .set({ "authorization": `Bearer ${token}` })
            .expect(400, 'Price must be a number', done)
    })
    it('should not add if price is null', (done) => {

        return request(app)
            .post('/api/membership/',
            ).send({ name: null, price: null, member_id: 4 }).type('form')
            .set({ "authorization": `Bearer ${token}` })
            .expect(400, done)
    })
    it('should not add if Member ID is a character', (done) => {

        return request(app)
            .post('/api/membership/',
            ).send({ name: "FULL", price: 1234, member_id: "abc" }).type('form')
            .set({ "authorization": `Bearer ${token}` })
            .expect(400, "Invalid Input for Member", done)
    })

    it('should not add if Member ID is a null', (done) => {

        return request(app)
            .post('/api/membership/',
            ).send({ name: "FULL", price: 1234, member_id: null }).type('form')
            .set({ "authorization": `Bearer ${token}` })
            .expect(400, "Member must be provided", done)
    })
    it('should not add if no token provided', (done) => {

        return request(app)
            .post('/api/membership/',
            ).send({ name: "FULL", price: 1234, member_id: null }).type('form')
            .expect(403, done)
    })
})