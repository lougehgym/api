const request = require('supertest')
const express = require('express')
const app = express();
const routeMember = require('../../../routes/membership');
const routeLogin = require('../../../routes/login')
const bodyParser = require('body-parser')
app.use(bodyParser.json())

app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(routeMember)
app.use(routeLogin)
beforeEach((done) => {
    return request(app)
        .post('/login')
        .send({ id: 2, password: "1234" })
        .type('form')
        .end((err, response) => {
            if (err) {
                console.log(err)
                done();
            }
            token = response.body.token;
            done();
        })
})
describe('GET /api/membership/', () => {
    // afterAll(async done=>done())

    it('should display 400 status code if no membership found ', (done) => {

        return request(app).get('/api/membership/7')
            .set({ "authorization": `Bearer ${token}` })
            .expect(400, done)
    })


    it(`Should not allow characters when fetching a membership`, (done) => {

        return request(app).get('/api/membership/abc')
            .set({ "authorization": `Bearer ${token}` })
            .expect(400, done)
    })

    it('should fetch all data', (done) => {
        return request(app).get('/api/membership')
            .set({ "authorization": `Bearer ${token}` }).expect(200, done)
    })

    it(`can't fetch data that does not exist`, (done) => {
        return request(app).get('/api/membership/456')
            .set({ "authorization": `Bearer ${token}` }).expect(400, done)
    })

    it(`can't fetch data that does not exist`, (done) => {
        return request(app).get('/api/membership/50')
            .set({ "authorization": `Bearer ${token}` }).expect(400, done)
    })
    it(`can't fetch data if no token provided`, (done) => {
        return request(app).get('/api/membership/50')
            .expect(403, done)
    })
})