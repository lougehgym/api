const request = require('supertest')
const express = require('express')
const app = express();
const routeMember = require('../../../routes/membership');
const routeLogin = require('../../../routes/login')
const bodyParser = require('body-parser')
app.use(bodyParser.json())

app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(routeMember)
app.use(routeLogin)
let token
beforeEach((done) => {
    return request(app)
        .post('/login')
        .send({ id: 2, password: "1234" })
        .type('form')
        .end((err, response) => {
            if (err) {
                console.log(err)
                done();
            }
            token = response.body.token;
            done();
        })
})

describe('DELETE /api/membership/', () => {
    // it('should delete a member', (done) => {
    //     return request(app)
    //         .delete('/api/members/45')
    //         .expect(200, done)
    // })
    it('should receive status code 400 if membership not found', (done) => {
        return request(app)
            .delete('/api/membership/68')
            .set({ "authorization": `Bearer ${token}` })
            .expect(400, done)
    })
    it('should not allow null values(400)', (done) => {
        return request(app)
            .delete(`/api/membership/${null}`)
            .set({ "authorization": `Bearer ${token}` })
            .expect(400, done)
    })
    it('should not allow characters to delete a membership(400)', (done) => {
        return request(app)
            .delete('/api/membership/abc')
            .set({ "authorization": `Bearer ${token}` })
            .expect(400, done)
    })

    it('should not allow  to delete a membership without token', (done) => {
        return request(app)
            .delete('/api/membership/abc')
            .expect(403, done)
    })

})
