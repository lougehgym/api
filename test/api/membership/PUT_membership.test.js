
const request = require('supertest')
const express = require('express')
const app = express();
const routeMembership = require('../../../routes/membership');
const routeLogin = require('../../../routes/login');
const bodyParser = require('body-parser')
app.use(bodyParser.json())

app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(routeMembership)
app.use(routeLogin)
beforeEach((done) => {
    return request(app)
        .post('/login')
        .send({ id: 2, password: "1234" })
        .type('form')
        .end((err, response) => {
            if (err) {
                console.log(err)
                done();
            }
            token = response.body.token;
            done();
        })
})

describe('PUT /api/membership', () => {
    // it('should update membership', (done) => {
    //     return request(app).put('/api/membership/24')
    //         .send({ name: "FULL", price: 5432, member_id: 4 }).type('form')
    //         .expect(200, done)
    // })
    it('should not update membership if name is a number', (done) => {
        return request(app).put('/api/membership/24')
            .send({ name: 747, price: 5432, member_id: 4 }).type('form')
            .set({ "authorization": `Bearer ${token}` })
            .expect(400, 'Membership name must not be a number', done)
    })
    it('should not update membership if ID is a character', (done) => {
        return request(app).put('/api/membership/abc')
            .send({ name: "Full", price: 5432, member_id: 4 }).type('form')
            .set({ "authorization": `Bearer ${token}` })
            .expect(400, 'ID must be integer', done)
    })
    it('should not update a membership if it does not exists', (done) => {
        return request(app).put('/api/membership/64')
            .send({ name: "Partial", price: 5432, member_id: 4 }).type('form')
            .set({ "authorization": `Bearer ${token}` })
            .expect(400, 'No membership found', done)
    })

    it('should not update  name is Null', (done) => {
        return request(app).put('/api/membership/40')
            .send({ name: null, price: 5432, member_id: 4 }).type('form')
            .set({ "authorization": `Bearer ${token}` })
            .expect(400, `Membership name must be provided`, done)
    })
    it('should not update  name is Empty', (done) => {
        return request(app).put('/api/membership/24')
            .send({ name: "", price: 5432, member_id: 4 }).type('form')
            .set({ "authorization": `Bearer ${token}` })
            .expect(400, done)
    })

    it('should not update price if it is a character', (done) => {
        return request(app).put('/api/membership/24')
            .send({ name: "abc", price: "abasd", member_id: 4 }).type('form')
            .set({ "authorization": `Bearer ${token}` })
            .expect(400, `Price must be a number`, done)
    })
    it('should not update  if price is null', (done) => {
        return request(app).put('/api/membership/24')
            .send({ name: "John", price: null, member_id: 'abc-05-30' }).type('form')
            .set({ "authorization": `Bearer ${token}` })
            .expect(400, `price must be provided`, done)
    })
    
    it('should not update if no token provided', (done) => {
        return request(app).put('/api/membership/24')
            .send({ name: "John", price: null, member_id: 'abc-05-30' }).type('form')
            .set({ "authorization": `Bearer ${token}` })
            .expect(400, `price must be provided`, done)
    })
})
