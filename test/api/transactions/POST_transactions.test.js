const request = require('supertest')
const express = require('express')
const app = express();
const routeTransaction = require('../../../routes/transaction');
const routeLogin = require('../../../routes/login');
const bodyParser = require('body-parser')
app.use(bodyParser.json())

app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(routeTransaction)

app.use(routeLogin)

let token;
beforeEach((done) => {
    return request(app)
        .post('/login')
        .send({ id: 2, password: "1234" })
        .type('form')
        .end((err, response) => {
            if (err) {
                console.log(err)
                done();
            }
            token = response.body.token;
            done();
        })
})
describe('POST /api/transaction/', () => {
    it('should add new Transaction', (done) => {
        return request(app)
            .post('/api/transaction/',
            ).send({ date: "2019-09-23", member_id: 32 }).type('form')

            .set('Authorization', `Bearer ${token}`)
            .expect(201, done)
    })

    it('should not add if member_id is null', (done) => {

        return request(app)
            .post('/api/transaction/',
            ).send({ date: "2019-09-16", member_id: null }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
    it('should not add if member is a character', (done) => {

        return request(app)
            .post('/api/transaction/',
            ).send({ date: "2019-09-16", member_id: "abc" }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
    it('should not add if date is a character', (done) => {

        return request(app)
            .post('/api/transaction/',
            ).send({ date: "qwe", member_id: 4 }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
    it('should not add if date is null', (done) => {

        return request(app)
            .post('/api/transaction/',
            ).send({ date: null, member_id: 4 }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })

    it('should not add if date is a number', (done) => {

        return request(app)
            .post('/api/transaction/',
            ).send({

                date: 12345,
                member_id: 4
            }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
    it('should not add if date is incorrect format', (done) => {

        return request(app)
            .post('/api/transaction/',
            ).send({

                date: "04-05-1992",
                member_id: 4
            }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
})