const request = require('supertest')
const express = require('express')
const app = express();
const routeMember = require('../../../routes/transaction');
const routeLogin = require('../../../routes/login');
const bodyParser = require('body-parser')
app.use(bodyParser.json())

app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(routeMember)

app.use(routeLogin)

let token;
beforeEach((done) => {
    return request(app)
        .post('/login')
        .send({ id: 2, password: "1234" })
        .type('form')
        .set('Authorization', `Bearer ${token}`)
        .end((err, response) => {
            if (err) {
                console.log(err)
                done();
            }
            token = response.body.token;
            done();
        })
})
describe('put /api/transaction', () => {
    it('should update new Transaction', (done) => {
        return request(app)
            .put('/api/transaction/26',
            ).send({ member_id: 32, date: "2019-09-23" }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(200, done)
    })

    it('should not update if transaction not found', (done) => {

        return request(app)
            .put('/api/transaction/100',
            ).send({ member_id: 4, date: "2019-09-23" }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
    it('should not update if id is a character', (done) => {

        return request(app)
            .put('/api/transaction/abc',
            ).send({ member_id: 4, date: "2019-09-23" }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
    it('should not update if member_id is null', (done) => {

        return request(app)
            .put('/api/transaction/24',
            ).send({ member_id: null, date: "2019-09-23" }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
    it('should not update if date is incorrect format', (done) => {

        return request(app)
            .put('/api/transaction/24',
            ).send({ member_id: 4, date: "abc", }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
    it('should not update if date is null', (done) => {

        return request(app)
            .put('/api/transaction/24',
            ).send({ member_id: 4, date: null, }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
    it('should not update if member_id is character', (done) => {

        return request(app)
            .put('/api/transaction/24',
            ).send({
                member_id: "abc",
                date: "2019-09-23"
            }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })

    it('should not update if  date is a number', (done) => {

        return request(app)
            .put('/api/transaction/24',
            ).send({
                date: 1234,
                member_id: null,
            }).type('form')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
})