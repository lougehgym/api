const request = require('supertest')
const express = require('express')
const app = express();
const routeTransaction = require('../../../routes/transaction');
const routeLogin = require('../../../routes/login');
const bodyParser = require('body-parser')
app.use(bodyParser.json())

app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(routeTransaction)

app.use(routeLogin)

let token;
beforeEach((done) => {
    return request(app)
        .post('/login')
        .send({ id: 2, password: "1234" })
        .type('form')
        .end((err, response) => {
            if (err) {
                console.log(err)
                done();
            }
            token = response.body.token;
            done();
        })
})

describe('GET /api/transaction/', () => {

    it('should display 400 status code if no transaction found ', (done) => {

        return request(app).get('/api/transaction/45')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })


    it(`Should not allow characters when fetching a transaction`, (done) => {

        return request(app).get('/api/transaction/abc')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })

    it('should fetch all data', (done) => {
        return request(app).get('/api/transaction')
            .set('Authorization', `Bearer ${token}`).expect(200, done)
    })

    it('should fetch a specific data', (done) => {
        return request(app).get('/api/transaction/26')
            .set('Authorization', `Bearer ${token}`).expect(200, "data", done)
    })
})