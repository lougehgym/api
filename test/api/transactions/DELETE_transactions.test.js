const request = require('supertest')
const express = require('express')
const app = express();
const routeTransaction = require('../../../routes/transaction');
const routeLogin = require('../../../routes/login');
const bodyParser = require('body-parser')
app.use(bodyParser.json())

app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(routeTransaction)
app.use(routeLogin)

let token;
beforeEach((done) => {
    return request(app)
        .post('/login')
        .send({ id: 2, password: "1234" })
        .type('form')
        .end((err, response) => {
            if (err) {
                console.log(err)
                done();
            }
            token = response.body.token;
            done();
        })
})

describe('DELETE /api/transaction/', () => {
    // it('should delete a member', (done) => {
    //     return request(app)
    //         .delete('/api/members/45')
    // .set('Authorization', `Bearer ${token}`)
    //         .expect(200, done)
    // })
    it('should receive status code 400 if transaction not found', (done) => {
        return request(app)
            .delete('/api/transaction/68')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
    it('should not allow null values(400)', (done) => {
        return request(app)
            .delete(`/api/transaction/${null}`)
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })
    it('should not allow characters to delete a transaction(400)', (done) => {
        return request(app)
            .delete('/api/transaction/abc')
            .set('Authorization', `Bearer ${token}`)
            .expect(400, done)
    })


})
