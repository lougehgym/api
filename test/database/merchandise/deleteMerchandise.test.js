const config = require('../../../database/config');
const db = require('../../../database/db')
const merchandise = require('../../../database/merchandise')
describe('Can delete merchandise', () => {


    // test('Can Delete Merchandise', async (done) => {
    //     const id = 46;
    //     const result = await d.query('DELETE FROM merchandise WHERE id=$1', [id], config.devb)
    //     expect(result.rowCount).toEqual(1);
    //     await db.pool(config.dev).end();
    //     done();
    // })

    test('Cannot Delete Merchandise if it does not exist', async (done) => {
        const id = 1234;
        const result = await merchandise.queryMerchandise({ id })
        expect(result.rowCount).toEqual(0);
        await db.pool(config.dev).end();
        done();
    })
})