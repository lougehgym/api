

const config = require('../../../database/config');
const db = require('../../../database/db')

describe('Insert Merchandise from the database', () => {

    test('Can Insert a Merchandise', async (done) => {
        const name = "Vince", price = 3600;
        const result = await db.query('INSERT INTO MEMBERS(name,zip) VALUES($1,$2);', [name, price], config.dev);
        expect(result.rowCount).toEqual(1);
        await db.pool(config.dev).end();
        done();
    })
})