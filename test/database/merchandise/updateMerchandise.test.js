

const config = require('../../../database/config');
const db = require('../../../database/db')
const merchandise = require('../../../database/merchandise')
describe('UPDATE MERCHANDISE', () => {

    test('Can Update a Merchandise', async (done) => {
        const id = 11
        const name = "Vince", price = 3600;
        const result = await db.query(
            `UPDATE merchandise SET name=$1,price=$2 WHERE id=$3`,
            [name, price, id], config.dev)
        expect(result.rowCount).toEqual(1);
        await db.pool(config.dev).end();

        done()
    })
    test('Cannot update Merchandise if it does not exist', async (done) => {
        const id = 3
        const result = await merchandise.queryMerchandise({ id })
        expect(result.rowCount).toEqual(0);
        await db.pool(config.dev).end();

        done()
    })
})