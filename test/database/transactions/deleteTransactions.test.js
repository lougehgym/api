const config = require('../../../../database/config');
const db = require('../../../database/db')

describe('', () => {


    test('Can Delete Transaction', async (done) => {
        const id = 2;
        const result = await db.query('DELETE FROM transaction WHERE id=$1', [id], config.dev)
        expect(result.rowCount).toEqual(1);
        await db.pool(config.dev).end();
        done();
    })
})