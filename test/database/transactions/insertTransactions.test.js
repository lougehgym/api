

const config = require('../../../database/config');
const db = require('../../../database/db')

describe('Insert Transaction from the database', () => {

    test('Can Insert a Transaction', async (done) => {
        const member_id = 24, date = "2019-09-20"
        const result = await db.query('INSERT INTO transaction(member_id,date) VALUES($1,$2,$3);', [member_id, date], config.dev);
        expect(result.rowCount).toEqual(1);
        await db.pool(config.dev).end();
        done();
    })
})