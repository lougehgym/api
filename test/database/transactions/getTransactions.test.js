
const config = require('../../../../database/config');
const db = require('../../../database/db')

describe(() => {
    test('Can Query Transaction', async (done) => {
        const result = await db.query("SELECT * FROM tranction", [], config.dev);
        expect(result.rows).toBeDefined();
        await db.pool(config.dev).end();

        done();
    })
    test('Can Query Specific Transaction', async (done) => {
        const id = 26;
        const result = await db.query("SELECT * FROM transaction WHERE ID=$1", [id], config.dev)
        expect(result.rows.length).toEqual(1);
        await db.pool(config.dev).end();
        done();
    })
})