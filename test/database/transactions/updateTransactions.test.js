

const config = require('../../../database/config');
const db = require('../../../database/db')


describe('UPDATE MERCHANDISE', () => {

    test('Can Update a Merchandise', async (done) => {
        const id = 3
        const member_id = 24,  date = "2019-09-20"
        const result = await db.query(
            `UPDATE transaction SET member_id=$1,date=$2 WHERE id=$3`,
            [member_id,date,id], config.dev)
        expect(result.rowCount).toEqual(1);
        await db.pool(config.dev).end();

        done()
    })
})