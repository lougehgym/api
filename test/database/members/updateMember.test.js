

const config = require('../../../database/config');
const db = require('../../../database/db')
const member = require('../../../database/members')

describe('Can Update Member', () => {

    test('Can Update a Member', async (done) => {
        const id = 60
        const name = "Vince", zip = 3600, membership_date = "2019-09-20";

        const result = await db.query(
            `UPDATE MEMBERS SET name=$1,zip=$2,membership_date=$3 WHERE id=$4`,
            [name, zip, membership_date, id], config.dev)
        expect(result.rowCount).toEqual(1);
        await db.pool(config.dev).end();

        done()
    })

})

describe('Cannot update member if not found', () => {
    const id = 70;
    test('Cannot update member if it does not exists', async (done) => {

        const hasMember = await member.queryMember({ id });
        expect(hasMember.rowCount).toBeFalsy();
        done();
    })
})