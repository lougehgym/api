
const config = require('../../../database/config');
const db = require('../../../database/db')

describe('SELECT * FROM MEMBERS', () => {
    test('Can Query Members', async (done) => {
        const result = await db.query("SELECT * FROM members", [], config.dev);
        expect(result.rows).toBeDefined();
        await db.pool(config.dev).end();

        done();
    })
    test('Can Query One Member', async (done) => {
        const id = 25;
        const result = await db.query("SELECT * FROM MEMBERS WHERE ID=$1", [id], config.dev)
        expect(result.rows.length).toEqual(1);
        await db.pool(config.dev).end();
        done();
    })
})