


const config = require('../../../database/config');
const db = require('../../../database/db')
const members = require('../../../database/members')
describe('', () => {


    // test('Can Delete a Member', async (done) => {
    //     const id = 60;
    //     const result = await db.query('DELETE FROM MEMBERS WHERE id=$1', [id], config.dev)
    //     expect(result.rowCount).toEqual(1);
    //     await db.pool(config.dev).end();
    //     done();
    // })
    test('Could not delete member if it does not exist', async (done) => {
        const id = 70;
        const hasMembers = await members.queryMember({ id });

        expect(hasMembers.rowCount).toBeFalsy();
        done();
    })
})