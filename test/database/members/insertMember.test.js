

const config = require('../../../database/config');
const db = require('../../../database/db')
const pgtools = require('pgtools')
const members = require('../../../database/members')
// describe('Create Database', () => {

//     beforeEach(done =>
//         pgtools.createdb(config.test,  function (err, res) {
//             if (err) {
//                 console.error(err);
//             }
//             console.log(`Created Database`)
//             done();
//         })
//     )

// })
beforeAll((done) => {


    // pgtools.createdb(config.test, 'fitness_db_test', (err, res) => {
    //     if (err) {
    //         console.log(err)
    //         done();
    //     }
    // })
    // done();
    db.client(config.test).connect();

    db.client(config.test).query('CREATE DATABASE fitness_db_test');
    db.client(config.test).end();
    done();
})

describe('Insert Member from the database', () => {

    beforeAll(async done => {
        await members.createTable('members', { ...config.test, database: 'fitness_db_test' })
        done();
    }
    );

    test('Can Insert a Member', async (done) => {
        const name = "Juswa", zip = 3600, membership_date = "2019-09-20";
        const result = await db.query('INSERT INTO MEMBERS(name,zip,membership_date) VALUES($1,$2,$3);', [name, zip, membership_date], { ...config.test, database: 'fitness_db_test' });

        expect(result.rowCount).toEqual(1);

        done();
    })

    afterAll(async (done) => {

        db.client(config.test).connect();

        db.client(config.test).query('DROP DATABASE fitness_db_test');
        db.client(config.test).end();
        done();
        // pgtools.dropdb(config.test, 'fitness_db_test', (err, res) => {
        //     if (err) {
        //         console.error(err)
        //         done()
        //     }
        //     done();

        // })
    })
})



