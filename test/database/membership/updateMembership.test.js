

const config = require('../../../database/config');
const db = require('../../../database/db')
const membership = require('../../../database/membership')

describe('Can Update Member', () => {

    test('Can Update a Member', async (done) => {
        const id = 11
        const name = "Partial", price = 1200, member_id = 46;
        const result = await db.query(
            `UPDATE MEMBERSHIP SET name=$1,price=$2,member_id=$3 WHERE id=$4`,
            [name, price, member_id, id], config.dev)
        expect(result.rowCount).toEqual(1);
        await db.pool(config.dev).end();

        done()
    })
    test('Cannot update Member if it does not exist', async (done) => {
        const id = 70
        const hasMembership = await membership.queryMembership({ id })
        expect(hasMembership.rowCount).toEqual(0);
        await db.pool(config.dev).end();

        done()
    })
})