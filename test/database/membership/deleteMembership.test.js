


const config = require('../../../database/config');
const db = require('../../../database/db')
const membership = require('../../../database/membership')
describe('', () => {


    // test('Can Delete a Member', async (done) => {
    //     const id = 10;
    //     const result = await db.query('DELETE FROM MEMBERSHIP WHERE id=$1', [id], config.dev)
    //     expect(result.rowCount).toEqual(1);
    //     await db.pool(config.dev).end();
    //     done();
    // })

    test('Cannot delete a Member if it does not exist', async (done) => {
        const id = 70;
        const result = await membership.queryMembership({ id })
        expect(result.rowCount).toEqual(0);
        await db.pool(config.dev).end();
        done();
    })
})