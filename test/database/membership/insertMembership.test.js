

const config = require('../../../database/config');
const db = require('../../../database/db')

describe('Insert Member from the database', () => {

    test('Can Insert a Member', async (done) => {
        const name = "Full", price = 3600, member_id = 46;
        const result = await db.query('INSERT INTO MEMBERSHIP(name,price,member_id) VALUES($1,$2,$3);', [name, price, member_id], config.dev);
        expect(result.rowCount).toEqual(1);
        await db.pool(config.dev).end();
        done();
    })
})