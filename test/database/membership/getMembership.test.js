
const config = require('../../../database/config');
const db = require('../../../database/db')

describe("SELECT * FROM membership",() => {
    test('Can Query Membership', async (done) => {
        const result = await db.query("SELECT * FROM membership", [], config.dev);
        expect(result.rows).toBeDefined();
        await db.pool(config.dev).end();

        done();
    })
    test('Can Query Specific Membership', async (done) => {
        const id = 11;
        const result = await db.query("SELECT * FROM membership WHERE ID=$1", [id], config.dev)
        expect(result.rows.length).toEqual(1);
        await db.pool(config.dev).end();
        done();
    })
})