

const config = require('../../../database/config');
const db = require('../../../database/db')
const passCategory = require('../../../database/pass_category')

describe('Can Update Member', () => {

    test('Can Update a Member', async (done) => {
        const id = 46
        const name = "Vince", price = 3600;
        const result = await db.query(
            `UPDATE MEMBERS SET name=$1,price=$2 WHERE id=$3`,
            [name, price, id], config.dev)
        expect(result.rowCount).toEqual(1);
        await db.pool(config.dev).end();

        done()
    })

    test('Cannot Update a Member if it does not exist', async (done) => {
        const id = 46
        const result = await passCategory.queryPassCategory({ id })
        expect(result.rowCount).toEqual(0);
        await db.pool(config.dev).end();

        done()
    })
})