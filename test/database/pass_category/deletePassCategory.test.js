const config = require('../../../../database/config');
const db = require('../../../database/db')
const passCategory = require('../../../database/pass_category')
describe('', () => {


    // test('Can Delete Pass Category', async (done) => {
    //     const id = 46;
    //     const result = await db.query('DELETE FROM pass_category WHERE id=$1', [id], config.dev)
    //     expect(result.rowCount).toEqual(1);
    //     await db.pool(config.dev).end();
    //     done();
    // })

    test('Cannot Delete Pass Category', async (done) => {
        const id = 123;
        const result = passCategory.queryPassCategory({ id })
        expect(result.rowCount).toEqual(0);
        await db.pool(config.dev).end();
        done();
    })
})