

const config = require('../../../database/config');
const db = require('../../../database/db')

describe('Insert Pass Category from the database', () => {

    test('Can Insert a Pass Category', async (done) => {
        const name = "Vince", price = 3600;
        const result = await db.query('INSERT INTO pass_category(name,price) VALUES($1,$2);', [name, price], config.dev);
        expect(result.rowCount).toEqual(1);
        await db.pool(config.dev).end();
        done();
    })
})