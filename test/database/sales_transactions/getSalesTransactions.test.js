
const config = require('../../../database/config');
const db = require('../../../database/db')

describe(() => {
    test('Can Query Sales Transactions', async (done) => {
        const result = await db.query("SELECT * FROM sales_transactions", [], config.dev);
        expect(result.rows).toBeDefined();
        await db.pool(config.dev).end();

        done();
    })
    test('Can Query Specific Sales Transactions', async (done) => {
        const id = 26;
        const result = await db.query("SELECT * FROM sales_transactions WHERE ID=$1", [id], config.dev)
        expect(result.rows.length).toEqual(1);
        await db.pool(config.dev).end();
        done();
    })
})