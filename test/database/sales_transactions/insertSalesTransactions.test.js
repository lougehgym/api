

const config = require('../../../database/config');
const db = require('../../../database/db')

describe('Insert One Day Pass from the database', () => {

    test('Can Insert a One Day Pass', async (done) => {
        const member_id = 24, passcat_id = 2;
        const result = await db.query('INSERT INTO one_day_pass(member_id,passcat_id) VALUES($1,$2);', [member_id, passcat_id], config.dev);
        expect(result.rowCount).toEqual(1);
        await db.pool(config.dev).end();
        done();
    })
})