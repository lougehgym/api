const config = require('../../../../database/config');
const db = require('../../../database/db')
const salesTransaction = require('../../../database/sales_transaction')
describe('', () => {


    // test('Can Delete One Day Pass', async (done) => {
    //     const id = 2;
    //     const result = await db.query('DELETE FROM one_day_pass WHERE id=$1', [id], config.dev)
    //     expect(result.rowCount).toEqual(1);
    //     await db.pool(config.dev).end();
    //     done();
    // })
    test('Cannot Delete One Day Pass if it does not exist', async (done) => {
        const id = 1234;
        const result = await salesTransaction.querySalesTransaction({ id })
        expect(result.rowCount).toEqual(0);
        await db.pool(config.dev).end();
        done();
    })
})