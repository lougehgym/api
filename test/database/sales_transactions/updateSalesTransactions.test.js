

const config = require('../../../database/config');
const db = require('../../../database/db')
const salesTransaction = require('../../../database/sales_transaction')

describe('UPDATE ONE DAY PASS', () => {

    // test('Can Update a ONE DAY PASS', async (done) => {
    //     const id = 3
    //     const member_id = 24, passcat_id
    //     const result = await db.query(
    //         `UPDATE transaction SET member_id=$1,passcat_id=$2 WHERE id=$3`,
    //         [member_id, passcat_id, id], config.dev)
    //     expect(result.rowCount).toEqual(1);
    //     await db.pool(config.dev).end();

    //     done()
    // })

    test('Cannot Update a ONE DAY PASS if it does not exist', async (done) => {
        const id = 1234
        const result = salesTransaction.querySalesTransaction({ id })
        expect(result.rowCount).toEqual(0);
        await db.pool(config.dev).end();
        done()
    })
})