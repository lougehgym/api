# Lou Geh Gym Fitness API Links


Note: This API Link is only for CRUD.

## REQUIREMENTS 📦

**Install Axios**

```sh
npm install axios --save
```

### Sequelize
```sh
# Membership
sequelize model:create --name membership --attributes name:string,price:double,status:boolean


# Members
sequelize model:create --name members --attributes first_name:string,middle_name:string,last_name:string,zip:string
,membership_start_date:date,membership_end_date:date,contact_number:integer,address:string,email:string,membership_id
:integer

# Pass Category
sequelize model:create --name pass_category --attributes name:string,price:double,status:string 

# Merchandise
sequelize model:create --name merchandise --attributes name:string,price:double,quantity:integer

# One Day Pass
sequelize model:create --name one_day_pass --attributes member_id:integer,passcat_id:integer,date:date

# Transaction
sequelize model:create --name transaction --attributes member_id:integer,date:date

# Sales Transaction
sequelize model:create --name sales_transaction --attributes transaction_id:integer,merchandise_id:integer

# Seed individual table
sequelize seed:generate --name one-day-pass
# Sequelize Initialize DB SEED AND MIGRATION
sequelize db:drop && sequelize db:create && sequelize db:migrate && sequelize db:seed:all
```

### Members API

#### GET
```javascript

// USE AXIOS: localhost:3000/api/members
// FETCH ALL MEMBERS
async function fetchAllMembers(){
    const token = localStorage.token;
    await axios({
        method:'GET',
        url:`http://localhost:3000/api/members`,
        headers:{
            authorization:`Bearer ${token}` // Token must be saved in your local storage
        }

    })
    .then(response =>{
        console.log(response)
    })
    .catch(error => console.log(error))
}

//FETCH SPECIFIC MEMBER
async function fetchMember(){
    const token = localStorage.token
    
    await axios({
        method:"GET",
        url:`http://localhost:3000/api/members/${id}`, // ID must be specified, or else it will throw an error
        headers:{
            authorization:`Bearer ${token}`;
        }
    })
    .then(response => console.log(response))
    .catch(error => console.log(error))
}
```


#### POST
```javascript

async function addMember(){
    const token = localStorage.token;

    await axios({
        method:"POST",
        url:`http://localhost:3000/api/members`,
        headers:{
            authorization:`Bearer ${token}`
        },
        data:{
            membership_id: Number,          // Data types shown in the ff. are only accepted
            first_name: String,             // It will throw an error from the server
            middle_name: String | null,     // Can be optional to the user
            last_name:String,
            membership_start_date: Date,
            membership_end_date:Date,
            // contact_number: Number,      // Not implemented
            // address: String,             // Not implemented
            // email: String,               // Must be in a proper format
        }
    })
    .then(response=> console.log(response))
    .catch(error => console.log(error))
}
```


#### PUT
```javascript

async function updateMember(){
    const token = localStorage.token;

    await axios({
        method:"PUT",
        url:`http://localhost:3000/api/members/${id}`,
        headers:{
            authorization:`Bearer ${token}`
        },
        data:{
            membership_id: Number,         
            first_name: String,             
            middle_name: String | null,     
            last_name:String,
            membership_start_date: Date,
            membership_end_date:Date,
            // contact_number: Number,      
            // address: String,             
            // email: String,               
        }
    })
    .then(response=> console.log(response))
    .catch(error => console.log(error))
}
```

#### DELETE
```javascript

async function deleteMember(){
    const token = localStorage.token;

    await axios({
        method:"DELETE",
        url:`http://localhost:3000/api/members/${id}`,
        headers:{
            authorization:`Bearer ${token}`
        },
    })
    .then(response=> console.log(response))
    .catch(error => console.log(error))
}
```


### Membership API

#### GET
```javascript

// USE AXIOS: localhost:3000/api/members
// FETCH ALL MEMBERS
async function fetchAllMembership(){
    const token = localStorage.token;
    await axios({
        method:'GET',
        url:`http://localhost:3000/api/membership`,
        headers:{
            authorization:`Bearer ${token}` // Token must be saved in your local storage
        }

    })
    .then(response =>{
        console.log(response)
    })
    .catch(error => console.log(error))
}

//FETCH SPECIFIC MEMBER
async function fetchMembership(){
    const token = localStorage.token
    
    await axios({
        method:"GET",
        url:`http://localhost:3000/api/membership/${id}`, // ID must be specified, or else it will throw an error
        headers:{
            authorization:`Bearer ${token}`;
        }
    })
    .then(response => console.log(response))
    .catch(error => console.log(error))
}
```


#### POST
```javascript

async function addMembership(){
    const token = localStorage.token;

    await axios({
        method:"POST",
        url:`http://localhost:3000/api/membership`,
        headers:{
            authorization:`Bearer ${token}`
        },
        data:{
            membership_id: Number,          // Data types shown in the ff. are only accepted
            first_name: String,             // It will throw an error from the server
            middle_name: String | null,     // Can be optional to the user
            last_name:String,
            membership_start_date: Date,
            membership_end_date:Date,
            // contact_number: Number,      // Not implemented
            // address: String,             // Not implemented
            // email: String,               // Must be in a proper format
        }
    })
    .then(response=> console.log(response))
    .catch(error => console.log(error))
}
```


#### PUT
```javascript

async function updateMembership(){
    const token = localStorage.token;

    await axios({
        method:"PUT",
        url:`http://localhost:3000/api/membership/${id}`,
        headers:{
            authorization:`Bearer ${token}`
        },
        data:{
            membership_id: Number,         
            first_name: String,             
            middle_name: String | null,     
            last_name:String,
            membership_start_date: Date,
            membership_end_date:Date,
            // contact_number: Number,      
            // address: String,             
            // email: String,               
        }
    })
    .then(response=> console.log(response))
    .catch(error => console.log(error))
}
```

#### DELETE
```javascript

async function deleteMembership(){
    const token = localStorage.token;

    await axios({
        method:"DELETE",
        url:`http://localhost:3000/api/membership/${id}`,
        headers:{
            authorization:`Bearer ${token}`
        },
    })
    .then(response=> console.log(response))
    .catch(error => console.log(error))
}
```

