


/**
 * @param {Model} Model
 */
module.exports = function makeMembershipModel({ membership }) {
    return Object.freeze({
        findAll,
        findById,
        add,
        update,
        deleteById
    })

    async function findAll() {
        return membership.findAll({
        });
    }
    /** @param {Member} member */
    async function findById({ id }) {
        return membership.findByPk(id)
    }

    /** @param {Member} member */
    async function add({
        name,
        price, }) {
        return membership.create({ name, price })
    }
    /** @param {Member} member */
    async function update({
        id,
        name,
        price }) {
        return membership.update({
            name, price
        }, {
            where: { id },
        });
    }
    /** @param {Member} member */
    async function deleteById({ id }) {

        return membership.destroy({
            where: {
                id
            }
        })
    }

}

/**
 * @typedef {Object} Member
 * @prop {number=} id
 * @prop {string=} name,
 * @prop {string=} price
 *
 */
