


/**
 * @param {one_day_pass} one_day_pass
 */
module.exports = function makeOneDayPass({ one_day_pass, member, pass_category }) {
    return Object.freeze({
        findAll,
        findById,
        add,
        update,
        deleteById
    })

    async function findAll() {
        return one_day_pass.findAll({
            include: [
                {
                    model: member,
                    as: "member",
                }, {
                    model: pass_category,
                    as: 'pass_category'
                }
            ]
        })
    }
    /** @param {PassCategory} passCategory */
    async function findById({ id }) {
        return await one_day_pass.findByPk(id, {
            include: [
                {
                    model: member,
                    as: "member"
                },
                {
                    model: pass_category,
                    as: 'pass_category'
                }
            ]
        })
    }

    /** @param {PassCategory} passCategory */
    async function add({
        memberId,
        passCategoryId,
        date }) {

        return await one_day_pass.create({
            memberId,
            passCategoryId,
            date
        })
    }
    /** @param {PassCategory} passCategory */
    async function update({
        id, memberId, passCategoryId, date,  }) {
        return await one_day_pass.update({
            memberId,
            passCategoryId,
            date,
        }, {
            where: { id }
        })
    }
    /** @param {PassCategory} passCategory */
    async function deleteById({ id }) {
        return await one_day_pass.destroy({
            where: { id }
        })
    }

}

/**
 * @typedef {Object} PassCategory
 * @prop {number=} id
 * @prop {string=} name
 * @prop {number=} price
 *
 */
