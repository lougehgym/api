//@ts-check

module.exports = function makePassCategory({ sales_transaction, transaction, merchandise }) {
    return Object.freeze({
        findAll,
        findById,
        add,
        update,
        deleteById
    })

    async function findAll() {
        return await sales_transaction.findAll({
            include: [
                {
                    model: transaction,
                    as: "transaction"
                },
                {
                    model: merchandise,
                    as: "merchandise"
                }
            ]
        })
    }
    /** @param {SalesTransaction} SalesTransaction */
    async function findById({ id }) {
        return await sales_transaction.findByPk(id, {
            include: [
                {
                    model: transaction,
                    as: "transaction"
                },
                {
                    model: merchandise,
                    as: "merchandise"
                }
            ]
        })
    }

    /** @param {SalesTransaction} SalesTransaction */
    async function add({
        transactionId, merchandiseId, quantity_items }) {
        return await sales_transaction.create({
            transactionId,
            merchandiseId,
            quantity_items
        })
    }
    /** @param {SalesTransaction} SalesTransaction */
    async function update({
        id, transactionId, merchandiseId, quantity_items }) {
        return await sales_transaction.update({
            transactionId,
            merchandiseId,
            quantity_items
        }, {
            where: {
                id
            }
        })
    }
    /** @param {SalesTransaction} SalesTransaction */
    async function deleteById({ id }) {
        return await sales_transaction.destroy({
            where: { id }
        })
    }

}

/**
 * @typedef {Object} SalesTransaction
 * @prop {number=} id
 * @prop {string=} transactionId
 * @prop {number=} merchandiseId
 * @prop {number=} quantity_items
 *
 */
