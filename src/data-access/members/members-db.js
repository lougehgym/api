//@ts-check



module.exports = function makeMemberPool({ member, membership }) {
    return Object.freeze({
        findAll,
        findById,
        add,
        update,
        deleteById
    })

    async function findAll() {
        return member.findAll({
        })
    }
    /** @param {Member} member */
    async function findById({ id }) {
        return member.findByPk(id)
    }

    /** @param {Member} member */
    async function add({
        first_name,
        middle_name,
        last_name,
        zip,
        membership_start_date,
        membership_end_date,
        membershipId,
        email,
        address,
        contact_number

    }) {

        return member.create({
            first_name,
            middle_name,
            last_name,
            zip,
            membership_start_date,
            membership_end_date,
            membershipId,
            email,
            address,
            contact_number

        })
    }
    /** @param {Member} member */
    async function update({
        id,
        first_name,
        middle_name,
        last_name,
        zip,
        membership_start_date,
        membership_end_date,
        membershipId,
        email,
        address,
        contact_number }) {

        return member.update({

            first_name,
            middle_name,
            last_name,
            zip,
            membership_start_date,
            membership_end_date,
            membershipId,
            email,
            address,
            contact_number
        }, {
            where: {
                id
            }
        })
    }
    /** @param {Member} member */
    async function deleteById({ id }) {
        return member.destroy({
            where: { id }
        })
    }

}

/**
 * @typedef {Object} Member
 * @prop {number=} id
 * @prop {number=} membershipId
 * @prop {string=} first_name
 * @prop {string=} last_name
 * @prop {number=} zip
 * @prop {string=} middle_name
 * @prop {string=} membership_start_date,
 * @prop {string=} membership_end_date,
 * @prop {string=} address,
 * @prop {string=} contact_number
 * @prop {string=} email
 *
 */
