


/**
 * @param {transaction} transaction
 */
module.exports = function makeTransactionDb({ transaction, member }) {
    return Object.freeze({
        findAll,
        findById,
        add,
        update,
        deleteById
    })

    async function findAll() {
        return await transaction.findAll({})
    }
    /** @param {Transaction} Transaction */
    async function findById({ id }) {

        return await transaction.findByPk(id, {
            include: [
                {
                    model: member,
                    as: "member"
                },

            ]
        })
    }

    /** @param {Transaction} Transaction */
    async function add({
        date, memberId }) {
        return await transaction.create({
            memberId,
            date
        })
    }
    /** @param {Transaction} Transaction */
    async function update({
        id,
        date, memberId, }) {
        return await transaction.update({
            memberId, date
        }, {
            where: {
                id
            }
        })
    }
    /** @param {Transaction} Transaction */
    async function deleteById({ id }) {
        return await transaction.destroy({
            where: { id }
        })
    }

}

/**
 * @typedef {Object} Transaction
 * @prop {number=} id
 * @prop {string=} date
 * @prop {number=} memberId
 *
 */
