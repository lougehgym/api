const makeTransactionDb = require('./transaction-db');
const { transaction,member } = require('../../database/models')
const transactionDb = makeTransactionDb({ transaction,member });


module.exports = transactionDb;