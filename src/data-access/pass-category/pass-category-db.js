


/**
 * @param {pass_category} pass_category
 */
module.exports = function makePassCategory({ pass_category }) {
    return Object.freeze({
        findAll,
        findById,
        add,
        update,
        deleteById
    })

    async function findAll() {
        return await pass_category.findAll({})
    }
    /** @param {PassCategory} passCategory */
    async function findById({ id }) {
        return await pass_category.findByPk(id)
    }

    /** @param {PassCategory} passCategory */
    async function add({
        name,
        price, }) {
        return await pass_category.create({
            name, price
        })
    }
    /** @param {PassCategory} passCategory */
    async function update({
        id,
        name,
        price, }) {
        return await pass_category.update({
            name,
            price,
        }, {
            where: {
                id
            }
        })
    }
    /** @param {PassCategory} passCategory */
    async function deleteById({ id }) {

        return pass_category.destroy({
            where: {
                id
            }
        })
    }

}

/**
 * @typedef {Object} PassCategory
 * @prop {number=} id
 * @prop {string=} name
 * @prop {number=} price
 *
 */
