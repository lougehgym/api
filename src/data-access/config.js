
const Pool = require('pg').Pool;

const db = Object.freeze({
    user: 'postgres',
    host: 'localhost',
    port: 5432,
    password: 'root'
})
/** @param {Config} config */
const pool = (config = { ...db, database: 'fitness_db' }) => new Pool(config)

module.exports = pool;




/**
 * @typedef {Object} Config
 * @prop {string} user
 * @prop {string} host
 * @prop {number} port
 * @prop {string} password
 * @prop {string} database
 */