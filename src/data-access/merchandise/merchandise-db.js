


/**
 * @param {merchandise} merchandise
 */
module.exports = function makeMerchandiseDb({ merchandise }) {
    return Object.freeze({
        findAll,
        findById,
        add,
        update,
        deleteById
    })

    async function findAll() {
        return await merchandise.findAll({})
    }
    /** @param {Merchandise} Merchandise */
    async function findById({ id }) {
        return await merchandise.findByPk(id)
    }

    /** @param {Merchandise} Merchandise */
    async function add({
        name,
        price,
        quantity }) {
        return await merchandise.create({
            name,
            price,
            quantity
        })
    }
    /** @param {Merchandise} Merchandise */
    async function update({
        id,
        name,
        price, quantity }) {
        return await merchandise.update({
            name, price, quantity
        }, {
            where: { id }
        })
    }
    /** @param {Merchandise} Merchandise */
    async function deleteById({ id }) {
        return await merchandise.destroy({
            where: { id }
        })
    }

}

/**
 * @typedef {Object} Merchandise
 * @prop {number=} id
 * @prop {string=} name
 * @prop {number=} price
 * @prop {number=} quantity
 *
 */
