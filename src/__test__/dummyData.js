const faker = require('faker')
const moment = require('moment')
const makeFakeMember = (overrides) => {
    const member = {
        id: faker.random.number(),
        first_name: faker.name.firstName(),
        middle_name: faker.name.lastName(),
        last_name: faker.name.lastName(),
        zip: faker.address.zipCode(),
        membership_id: faker.random.number(),
        membership_start_date: new Date(),
        membership_end_date: new Date(),
        createdAt: new Date(),
        updatedAt:new Date(),
    }

    return {
        ...member,
        ...overrides
    }
}
const makeFakeMembership = (overrides) => {
    const membership = {
        id: faker.random.number(),
        name: faker.random.word(),
        price: faker.commerce.price(),
        status: `Active`,
    }

    return {
        ...membership,
        ...overrides
    }
}
module.exports = { makeFakeMember, makeFakeMembership }