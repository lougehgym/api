'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('sales_transaction', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      transactionId: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: 'transaction',
            schema: 'public'
          },
          key: 'id'
        }
      },
      merchandiseId: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: 'merchandise',
            schema: 'public'
          },
          key: 'id'
        }
      },
      quantity_items: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('sales_transaction');
  }
};