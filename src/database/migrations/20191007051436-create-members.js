'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('member', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      membershipId: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: 'membership',
            schema: 'public',

          },
          key: 'id'
        },
        allowNull: false,
      },
      first_name: {
        type: Sequelize.STRING
      },
      middle_name: {
        type: Sequelize.STRING
      },
      last_name: {
        type: Sequelize.STRING
      },
      zip: {
        type: Sequelize.STRING
      },
      membership_start_date: {
        type: Sequelize.DATE
      },
      membership_end_date: {
        type: Sequelize.DATE
      },
      contact_number: {
        type: Sequelize.INTEGER
      },
      address: {
        type: Sequelize.STRING
      },
      email: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('member');
  }
};