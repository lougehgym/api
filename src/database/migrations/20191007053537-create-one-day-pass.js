'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('one_day_pass', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      memberId: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: 'member',
            schema: 'public',
          },
          key: 'id',
        },
        allowNull:false,
      },
      passCategoryId: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: 'pass_category',
            schema: 'public'
          },
          key: 'id'
        },
        allowNull:false
      },
      date: {
        type: Sequelize.DATE
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('one_day_pass');
  }
};