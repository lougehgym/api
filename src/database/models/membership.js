'use strict';
module.exports = (sequelize, DataTypes) => {
  const membership = sequelize.define('membership', {
    name: DataTypes.STRING,
    price: DataTypes.DOUBLE,
    status: DataTypes.STRING
  }, { freezeTableName: true });
  membership.associate = function (models) {
    // associations can be defined here
    membership.hasMany(models.member)
  };
  return membership;
};