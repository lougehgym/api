'use strict';
module.exports = (sequelize, DataTypes) => {
  const merchandise = sequelize.define('merchandise', {
    name: DataTypes.STRING,
    price: DataTypes.DOUBLE,
    quantity: DataTypes.INTEGER
  }, {freezeTableName: true});
  merchandise.associate = function(models) {
    // associations can be defined here
    merchandise.hasOne(models.sales_transaction)
  };
  return merchandise;
};