'use strict';
module.exports = (sequelize, DataTypes) => {
  const members = sequelize.define('member', {
    first_name: DataTypes.STRING,
    middle_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    zip: DataTypes.STRING,
    membership_start_date: DataTypes.DATE,
    membership_end_date: DataTypes.DATE,
    contact_number: DataTypes.INTEGER,
    address: DataTypes.STRING,
    email: DataTypes.STRING,
    membershipId: DataTypes.INTEGER
  }, {
    freezeTableName: true
  });
  members.associate = function (models) {
    // associations can be defined here
    members.belongsTo(models.membership)
    members.hasMany(models.transaction)
  };
  return members;
};