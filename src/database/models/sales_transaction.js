'use strict';
module.exports = (sequelize, DataTypes) => {
  const sales_transaction = sequelize.define('sales_transaction', {
    transactionId: DataTypes.INTEGER,
    merchandiseId: DataTypes.INTEGER,
    quantity_items: DataTypes.INTEGER
  }, { freezeTableName: true });
  sales_transaction.associate = function (models) {
    // associations can be defined here
    sales_transaction.belongsTo(models.merchandise)
    sales_transaction.belongsTo(models.transaction)
  };
  return sales_transaction;
};