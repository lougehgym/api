'use strict';
module.exports = (sequelize, DataTypes) => {
  const one_day_pass = sequelize.define('one_day_pass', {
    memberId: DataTypes.INTEGER,
    passCategoryId: DataTypes.INTEGER,
    date: DataTypes.DATE
  }, { freezeTableName: true });
  one_day_pass.associate = function (models) {
    // associations can be defined here
    one_day_pass.belongsTo(models.member)
    one_day_pass.belongsTo(models.pass_category)
    
  };
  return one_day_pass;
};