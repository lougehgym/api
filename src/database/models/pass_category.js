'use strict';
module.exports = (sequelize, DataTypes) => {
  const pass_category = sequelize.define('pass_category', {
    name: DataTypes.STRING,
    price: DataTypes.DOUBLE,
    status: DataTypes.STRING
  }, {freezeTableName: true});
  pass_category.associate = function(models) {
    // associations can be defined here
    pass_category.hasMany(models.one_day_pass)
  };
  return pass_category;
};