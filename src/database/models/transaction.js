'use strict';
module.exports = (sequelize, DataTypes) => {
  const transaction = sequelize.define('transaction', {
    memberId: DataTypes.INTEGER,
    date: DataTypes.DATE
  }, { freezeTableName: true });
  transaction.associate = function (models) {
    // associations can be defined here
    transaction.belongsTo(models.member)
    transaction.hasMany(models.sales_transaction)

  };
  return transaction;
};