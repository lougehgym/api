'use strict';
const faker = require('faker')
module.exports = {
  up: (queryInterface, Sequelize) => {

    return queryInterface.bulkInsert('member', [
      {

        first_name: faker.name.firstName(),
        middle_name: faker.name.lastName(),
        last_name: faker.name.lastName(),
        zip: faker.address.zipCode(),
        membershipId: 1,
        membership_start_date: new Date(),
        membership_end_date: new Date(),
        createdAt: new Date(),
        updatedAt: new Date(),
        email: "xyz@gmail.com",
        contact_number:1234956
      },
      {

        first_name: faker.name.firstName(),
        middle_name: faker.name.lastName(),
        last_name: faker.name.lastName(),
        zip: faker.address.zipCode(),
        membershipId: 2,
        membership_start_date: new Date(),
        membership_end_date: new Date(),
        createdAt: new Date(),
        updatedAt: new Date(),
        email: "abcz@gmail.com",
        contact_number:442315
      },
      {

        first_name: faker.name.firstName(),
        middle_name: faker.name.lastName(),
        last_name: faker.name.lastName(),
        zip: faker.address.zipCode(),
        membershipId: 3,
        membership_start_date: new Date(),
        membership_end_date: new Date(),
        email: faker.internet.email(),
        contact_number: faker.random.number(),
        address: faker.address.streetAddress(),
        createdAt: new Date(),
        updatedAt: new Date(),
        email: "qwe@gmail.com",
        contact_number:567234
      }
    ])
  },

  down: (queryInterface, Sequelize) => {

    return queryInterface.bulkDelete('members', null, {})
  }
};
