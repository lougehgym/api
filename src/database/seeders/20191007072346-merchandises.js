'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    return queryInterface.bulkInsert('merchandise', [
      {
        name: 'Gatorade',
        price: 20,
        quantity: 20,
        createdAt:new Date(),
        updatedAt:new Date(),
      },
      {
        name: 'Lemonade',
        price: 10,
        quantity: 70,
        createdAt:new Date(),
        updatedAt:new Date(),
      },
      {
        name: 'Red Bull',
        price: 75,
        quantity: 10,
        createdAt:new Date(),
        updatedAt:new Date(),
      },
      {
        name: 'Pancit Canton',
        price: 20,
        quantity: 20,
        createdAt:new Date(),
        updatedAt:new Date(),
      },

    ])
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
   return queryInterface.bulkDelete('merchandise', null, {});
  }
};
