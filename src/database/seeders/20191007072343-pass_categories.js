'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    return queryInterface.bulkInsert('pass_category', [
      {
        name: 'Zumba',
        price: 300,
        status: 'Active',
        createdAt:new Date(),
        updatedAt:new Date()
      },

      {
        name: 'Aerobic Centers',
        price: 500,
        status: 'Active',
        createdAt:new Date(),
        updatedAt:new Date()
      },

      {
        name: 'Yoga Centers',
        price: 200,
        status: 'Active',
        createdAt:new Date(),
        updatedAt:new Date()
      },

      {
        name: 'Dance Centers',
        price: 500,
        status: 'Active',
        createdAt:new Date(),
        updatedAt:new Date()
      },
      {
        name: 'Pilates Centers',
        price: 700,
        status: 'Active',
        createdAt:new Date(),
        updatedAt:new Date()
      },
    ])
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
    return queryInterface.bulkDelete('pass_categories', null, {});
  }
};
