'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    return queryInterface.bulkInsert('membership', [
      {
        name: '1 month',
        price: 2000,
        status: `Active`,
        createdAt: new Date(),
        updatedAt: new Date(),
      }, {
        name: '6 month',
        price: 12000,
        status: `Active`,
        createdAt: new Date(),
        updatedAt: new Date(),
      }, {
        name: '12 month',
        price: 24000,
        status: `Active`,
        createdAt: new Date(),
        updatedAt: new Date(),
      }
    ])
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('memberships', null, {});
  }
};
