'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    return queryInterface.bulkInsert('transaction',[
      {
        memberId: 1,
        date: new Date(),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        memberId: 2,
        date: new Date(),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        memberId: 3,
        date: new Date(),
        createdAt: new Date(),
        updatedAt: new Date(),
      }
    ])
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
    return queryInterface.bulkDelete('transaction', null, {})
  }
};
