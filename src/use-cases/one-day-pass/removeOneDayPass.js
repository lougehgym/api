//@ts-check
module.exports = function makeRemoveOneDayPass({ oneDayPassDb }) {
    return async function removeOneDayPass({ id }) {
        if (isNaN(id)) {
            throw new Error(`Id must be a character`)
        }

        const hasOnedayPass = await oneDayPassDb.findById({ id });
        if (!hasOnedayPass) {
            throw new Error('One Day Pass does not exist')
        }
        const result = await oneDayPassDb.deleteById({ id });

        if (!result) {
            throw new Error('No One Day Pass found')
        }
        return {
            message: `Successfully deleted One Day Pass `,
            one_day_pass: hasOnedayPass
        };
    }
}