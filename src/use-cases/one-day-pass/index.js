//@ts-check

//Database
const membersDb = require('../../data-access/members')
const passCategoryDb = require('../../data-access/pass-category')
const oneDayPassDb = require('../../data-access/one-day-pass')


const makeAddOneDayPass = require('./addOneDayPass')
const makeFindAllOneDayPass = require('./findAllOneDayPass')
const makeFindOneDayPass = require('./findOneDayPass')
const makeUpdateOneDayPass = require('./updateOneDayPass')
const makeRemoveOneDayPass = require('./removeOneDayPass')


const addOneDayPass = makeAddOneDayPass({ membersDb, passCategoryDb, oneDayPassDb })
const findAllOneDayPass = makeFindAllOneDayPass({ oneDayPassDb })
const findOneDayPass = makeFindOneDayPass({ oneDayPassDb })
const updateOneDayPass = makeUpdateOneDayPass({ oneDayPassDb, membersDb, passCategoryDb })
const removeOneDayPass = makeRemoveOneDayPass({ oneDayPassDb })
module.exports = {
    addOneDayPass,
    findAllOneDayPass,
    findOneDayPass,
    updateOneDayPass,
    removeOneDayPass
}