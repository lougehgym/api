//@ts-check
const makeOneDayPass = require('../../entities/one-day-pass')
module.exports = function makeUpdateOneDayPass({ oneDayPassDb, membersDb, passCategoryDb }) {

    return async function updateOneDayPass(oneDayPassInfo) {
        const oneDayPass = makeOneDayPass(oneDayPassInfo);
        const hasOneDayPass = await oneDayPassDb.findById({ id: oneDayPass.getId() });
        const hasMembers = await membersDb.findById({ id: oneDayPass.getMemberId() });
        const hasPassCategory = await passCategoryDb.findById({ id: oneDayPass.getPassCatId() })
        if (!hasOneDayPass) {
            throw new Error('One Day Pass not found')
        }
        if (!hasMembers) {
            throw new Error('Member not found, Unable to update One Day Pass')
        }
        if (!hasPassCategory) {
            throw new Error(`Pass Category not found, Unable to update One Day Pass`)
        }
        const result = await oneDayPassDb.update({
            id: oneDayPass.getId(),
            memberId: oneDayPass.getMemberId(),
            passCategoryId: oneDayPass.getPassCatId(),
            date: oneDayPass.getDate(),

        })
        if (!result) {
            throw new Error('Unable to add One Day Pass')
        }
        return {
            status: `Updated`,
            message: `Updated One Day Pass`,
            id: oneDayPass.getId(),
            member: hasMembers,
            pass_category: hasPassCategory,
            date: oneDayPass.getDate(),

        };

    }
};


/**
 * @typedef {Object} MerchandiseInfo
 * @prop {number=} id
 * @prop {number=} price
 * @prop {string=} name
 * @prop {number=} quantity
 *
 */
