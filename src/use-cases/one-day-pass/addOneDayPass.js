//@ts-check
const buildOneDayPass = require('../../entities/one-day-pass')
module.exports = function makeAddOneDayPass({ oneDayPassDb, membersDb, passCategoryDb }) {
    const addOneDayPass = async (oneDayPassInfo) => {
        const oneDayPass = buildOneDayPass(oneDayPassInfo);
        const hasMember = await membersDb.findById({ id: oneDayPass.getMemberId() })
        const hasPassCategory = await passCategoryDb.findById({ id: oneDayPass.getPassCatId() })
        if (!hasMember) {
            throw new Error('Member not found,Unable to Add One Day Pass')
        }
        if (!hasPassCategory) {
            throw new Error('Pass Category not found, Unable to Add One Day Pass')
        }
        const result = await oneDayPassDb.add({
            passCategoryId: oneDayPass.getPassCatId(),
            memberId: oneDayPass.getMemberId(),
            date: oneDayPass.getDate(),

        })
        if (!result) {
            throw new Error('Unable to add One Day Pass')
        }
        return {
            message: `Added One Day Pass`,
            pass_category: hasPassCategory,
            member: hasMember,
            date: oneDayPass.getDate(),

        };

    }
    return addOneDayPass;
};


/**
 * @typedef {Object} OneDayPass
 * @prop {number=} id
 * @prop {number=} passCatId
 * @prop {number=} memberId
 * @prop {string=} date
 *
 */
