//@ts-check
const buildOneDayPass = require('../../entities/one-day-pass');
/** @param {MakeDatabase} databases */
module.exports = function makeFindAllOneDayPass({ oneDayPassDb }) {
    const findAllOneDayPass = async () => {
        const result = await oneDayPassDb.findAll()
        if (!result) {
            throw new Error('No One Day Pass Found')
        }
        return result;

    }
    return findAllOneDayPass;
};


/**
 * @typedef {Object} OneDayPass
 * @prop {number=} id
 * @prop {number=} passCatId
 * @prop {number=} memberId
 * @prop {string=} date
 *
 */


/**
 *
 * @typedef {Object} Database
 * @prop {function=} findAll
 * @prop {function(OneDayPass)=} findById
 * @prop {function=} add
 * @prop {function=} update
 * @prop {function=} deleteById
 */

/**
 * @typedef {Object} MakeDatabase
 * @prop {Database} oneDayPassDb
 *
 */
