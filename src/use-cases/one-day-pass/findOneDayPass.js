//@ts-check

/** @param {MakeDatabase} databases */
module.exports = function makeFindOneDayPass({ oneDayPassDb }) {
    const findOneDayPass = async ({ id }) => {
        const result = await oneDayPassDb.findById({ id })
        if (!result) {
            throw new Error('No One Day Pass Found')
        }
        return result;

    }
    return findOneDayPass;
};


/**
 * @typedef {Object} OneDayPass
 * @prop {number=} id
 * @prop {number=} passCatId
 * @prop {number=} memberId
 * @prop {string=} date
 *
 */


/**
 *
 * @typedef {Object} Database
 * @prop {function=} findAll
 * @prop {function(OneDayPass)=} findById
 * @prop {function=} add
 * @prop {function=} update
 * @prop {function=} deleteById
 */

/**
 * @typedef {Object} MakeDatabase
 * @prop {Database} oneDayPassDb
 *
 */
