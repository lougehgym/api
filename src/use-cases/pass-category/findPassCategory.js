//@ts-check
const makePassCategory = require('../../entities/pass-category')
module.exports = function makeFindPassCategory({ passCategoryDb }) {
    return async function findPassCategory({ id }) {
        if (isNaN(id)) {
            throw new Error(`Id must be a character`)
        }
        const result = await passCategoryDb.findById({ id });
        if (!result) {
            throw new Error('No Pass Category found')
        }
        return result;
    }
}


