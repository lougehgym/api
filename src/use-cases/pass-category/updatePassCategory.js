const makePassCategory = require('../../entities/pass-category')

module.exports = function makeUpdatePassCategory({ passCategoryDb }) {
    return async function updatePassCategory(passCategoryInfo) {
        const passCategory = makePassCategory(passCategoryInfo)
        const hasPassCategory = passCategoryDb.findById({ id: passCategory.getId() })
        if (!hasPassCategory) {
            throw new Error('Pass Category does not exist')
        }
        const result = await passCategoryDb.update({
            id: passCategory.getId(),
            name: passCategory.getName(),
            price: passCategory.getPrice(),
        })
        if (!result) {
            throw new Error('Unable to update pass category')
        }
        return {
            status: "Update",
            message: "Updated Member",
            name: passCategory.getName(),
            price: passCategory.getPrice(),
        }
    }
}