//@ts-check

const passCategoryDb = require('../../data-access/pass-category');
const makeAddPassCategory = require('./addPassCategory')
const makeFindPassCategory = require('./findPassCategory')
const makeFindAllPassCategory = require('./findAllPassCategory')
const makeUpdatePassCategory = require('./updatePassCategory')
const makeRemovePassCategory = require('./removePassCategory');
const addPassCategory = makeAddPassCategory({ passCategoryDb })
const findPassCategory = makeFindPassCategory({ passCategoryDb })
const findAllPassCategory = makeFindAllPassCategory({ passCategoryDb })
const updatePassCategory = makeUpdatePassCategory({ passCategoryDb })
const removePassCategory = makeRemovePassCategory({passCategoryDb})
module.exports = { addPassCategory, findPassCategory, findAllPassCategory,updatePassCategory,removePassCategory }