//@ts-check
const buildPassCategory = require('../../entities/pass-category')
module.exports = function makeAddPassCategory({ passCategoryDb }) {
    const addPassCategory = async (passCategoryInfo) => {
        const passCategory = buildPassCategory(passCategoryInfo);
        const result = await passCategoryDb.add({
            name: passCategory.getName(),
            price: passCategory.getPrice(),

        })
        if (!result) {
            throw new Error('Unable to add Pass Category')
        }
        return {
            status:`Added`,
            message: `Added Pass Category`,
            name: passCategory.getName(),
            price: passCategory.getPrice(),

        };

    }
    return addPassCategory;
};


/**
 * @typedef {Object} MembershipInfo
 * @prop {number=} id
 * @prop {number=} price
 * @prop {string=} name
 *
 */
