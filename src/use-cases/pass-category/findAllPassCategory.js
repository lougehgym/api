//@ts-check

module.exports = function makeFindPassCategory({ passCategoryDb }) {
    return async function findAllPassCategory() {
        const result = await passCategoryDb.findAll();

        if (!result) {
            throw new Error('No Pass Category found')
        }
        return result;
    }
}