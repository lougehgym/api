const makePassCategory = require('../../entities/pass-category')

module.exports = function makeRemovePassCategory({ passCategoryDb }) {
    const removeCategoryInfo = async ({ id }) => {
        if (isNaN(id)) {
            throw new Error(`Id must be a character`)
        }
        const hasPassCategory = await passCategoryDb.findById({ id })

        if (!hasPassCategory) {
            throw new Error(`Pass Category not found`)
        }
        const result = passCategoryDb.deleteById({ id })

        if (!result) {
            throw new Error('Unable to delete Pass category')
        }
        return {
            status: "Deleted",
            message: "Successfully deleted Pass Category",
            pass_category:hasPassCategory,
        }
    }
    return removeCategoryInfo;
}