const membersDb = require('../../data-access/members')
const transactionDb = require('../../data-access/transactions')


const makeAddTransaction = require('./addTransactions');
const makeFindAllTransaction = require('./findAllTransactions')
const makeFindTransaction = require('./findTransaction')
const makeUpdateTransaction = require('./updateTransaction')
const makeRemoveTransaction = require('./removeTransaction')

const addTransaction = makeAddTransaction({ transactionDb, membersDb });
const findAllTransaction = makeFindAllTransaction({ transactionDb });
const findTransaction = makeFindTransaction({ transactionDb })
const updateTransaction = makeUpdateTransaction({ transactionDb, membersDb })
const removeTransaction = makeRemoveTransaction({ transactionDb })
module.exports = { addTransaction, findAllTransaction, findTransaction, updateTransaction,removeTransaction } 