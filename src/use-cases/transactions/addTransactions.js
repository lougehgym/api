const buildTransaction = require('../../entities/transactions');

module.exports = function makeAddTransaction({ transactionDb, membersDb }) {
    const addTransaction = async (transactionInfo) => {
        const transaction = buildTransaction(transactionInfo);
        let hasMembers
        if (transaction.getMemberId() !== null) {
            hasMembers = await membersDb.findById({ id: transaction.getMemberId() })
            if (!hasMembers) {
                throw new Error(`Member not found, Unable to add Transaction`)
            }
        }

        const result = await transactionDb.add({
            date: transaction.getDate(),
            memberId: transaction.getMemberId(),
        })

        if (!result) {
            throw new Error(`Unable to add Transaction`)
        }
        return {
            status: `Added`,
            message: `Added Transaction`,
            date: transaction.getDate(),
            member: hasMembers,
        };
    }
    return addTransaction
}