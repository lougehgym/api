const makeFindTransaction = ({ transactionDb }) => {


    const findTransaction = async ({ id }) => {
        if (isNaN(id)) {
            throw new Error(`Id must be a number`)
        }
        const result = await transactionDb.findById({ id })
        if (!result) {
            throw new Error(`Transaction not found`)
        }
        return result;
    }
    return findTransaction;
}

module.exports = makeFindTransaction;