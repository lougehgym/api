
const makeFindAllTransaction = ({ transactionDb }) => {

    const findAllTransaction = async () => {

        const result = await transactionDb.findAll();

        if (!result) {
            throw new Error('No Transactions found')
        }
        return result;

    }
    return findAllTransaction;
}

module.exports = makeFindAllTransaction;