
const makeRemoveTransaction = ({ transactionDb }) => {


    const removeTransaction = async ({ id }) => {
        if (isNaN(id)) {
            throw new Error(`Id must be a number`)
        }
        const hasTransaction = await transactionDb.findById({ id });
        if (!hasTransaction) {
            throw new Error(`Transaction not found, Unable to delete Transaction`)
        }
        const result = await transactionDb.deleteById({ id });

        if (!result) {
            throw new Error(`Unable to delete transaction, something went wrong`)
        }
        return {
            status: `Deleted`,
            message: `Deleted Transaction`,
            transaction: hasTransaction
        }

    }
    return removeTransaction;
}

module.exports = makeRemoveTransaction;