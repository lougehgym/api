const buildTransaction = require('../../entities/transactions')

const makeUpdateTransaction = ({ transactionDb, membersDb }) => {
    const updateTransaction = async (transactionInfo) => {
        const transaction = buildTransaction(transactionInfo);
        const hasTransaction = await transactionDb.findById({ id: transaction.getId() });
        let hasMember;
        if (!hasTransaction) {
            throw new Error(`Transaction not found. Unable to update transaction`)
        }
        if (transaction.getMemberId()) {
            hasMember = await membersDb.findById({ id: transaction.getMemberId() });
            if (!hasMember) {
                throw new Error(`Member not found. Unable to update Transaction`)
            }
        }
        const result = await transactionDb.update({
            id: transaction.getId(),
            date: transaction.getDate(),
            memberId: transaction.getMemberId()
        });
        if (!result) {
            throw new Error(`Unable to update transaction, something went wrong`)
        }
        return {
            status: `Updated`,
            message: `Updated Transaction`,
            id: transaction.getId(),
            date: transaction.getDate(),
            member: hasMember,
        }
    }
    return updateTransaction;

}

module.exports = makeUpdateTransaction