//@ts-check
const makeMerchandise = require('../../entities/merchandise')
module.exports = function makeRemoveMerchandise({ merchandiseDb }) {
    return async function removeMerchandise({ id }) {
        if (isNaN(id)) {
            throw new Error(`Id must be a character`)
        }

        const hasMerchandise = await merchandiseDb.findById({ id });
        if (!hasMerchandise) {
            throw new Error('merchandise does not exist')
        }
        const result = await merchandiseDb.deleteById({ id });

        if (!result) {
            throw new Error('Unable to delete Merchandise, something went wrong')
        }
        return {
            status: 'Deleted',
            message: `Successfully deleted Merchandise `,
            merchandise: hasMerchandise
        };
    }
}