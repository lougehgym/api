//@ts-check

const merchandiseDb = require('../../data-access/merchandise');


const makeAddMerchandise = require('./addMerchandise');
const makeFindAllMerchandise = require('./findAllMerchandise')
const makeFindMerchandise = require('./findMerchandise')
const makeUpdateMerchandise = require('./updateMerchandise')
const makeRemoveMerchandise = require('./removeMerchandise')

const addMerchandise = makeAddMerchandise({ merchandiseDb })
const findAllMerchandise = makeFindAllMerchandise({ merchandiseDb })
const findMerchandise = makeFindMerchandise({ merchandiseDb })
const updateMerchandise = makeUpdateMerchandise({ merchandiseDb })
const removeMerchandise = makeRemoveMerchandise({ merchandiseDb })


module.exports = { addMerchandise, findAllMerchandise, findMerchandise, updateMerchandise, removeMerchandise }