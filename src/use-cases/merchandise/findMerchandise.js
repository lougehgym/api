//@ts-check
const makeMerchandise = require('../../entities/merchandise')
module.exports = function makeFindMembership({ merchandiseDb }) {
    return async function findMerchandise({ id }) {
        if (isNaN(id)) {
            throw new Error(`Id must be a number`)
        }
        const result = await merchandiseDb.findById({ id });
        if (!result) {
            throw new Error('No Merchandise found')
        }
        return result;
    }
}


