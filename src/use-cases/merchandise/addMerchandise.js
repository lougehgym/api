//@ts-check
const buildMerchandise = require('../../entities/merchandise')
module.exports = function makeAddMerchandise({ merchandiseDb }) {

    return async function addMerchandise(merchandiseInfo) {

        const merchandise = buildMerchandise(merchandiseInfo);
        const result = await merchandiseDb.add({
            name: merchandise.getName(),
            price: merchandise.getPrice(),
            quantity: merchandise.getQuantity(),
        })
        if (!result) {
            throw new Error('Unable to add Merchandise')
        }
        return {
            message: `Added Merchandise`,
            name: merchandise.getName(),
            price: merchandise.getPrice(),
            quantity: merchandise.getQuantity(),
        };

    }
};


/**
 * @typedef {Object} MembershipInfo
 * @prop {number=} id
 * @prop {number=} price
 * @prop {string=} name
 *
 */
