//@ts-check
const makeMerchandise = require('../../entities/merchandise')
module.exports = function makeUpdateMerchandise({ merchandiseDb }) {

    return async function updateMerchandise(merchandiseInfo) {
        /** @param {MerchandiseInfo} merchandise*/
        const merchandise = makeMerchandise(merchandiseInfo);
        const hasMerchandise = await merchandiseDb.findById({ id: merchandise.getId() });
        if (!hasMerchandise) {
            throw new Error('Merchandise not found')
        }
        const result = await merchandiseDb.update({
            id: merchandise.getId(),
            name: merchandise.getName(),
            price: merchandise.getPrice(),
            quantity: merchandise.getQuantity(),

        })
        if (!result) {
            throw new Error('Unable to add Merchandise')
        }
        return {
            message: `Updated Merchandise`,
            id: merchandise.getId(),
            name: merchandise.getName(),
            price: merchandise.getPrice(),
            quantity:merchandise.getQuantity(),
        };

    }
};


/**
 * @typedef {Object} MerchandiseInfo
 * @prop {number=} id
 * @prop {number=} price
 * @prop {string=} name
 * @prop {number=} quantity
 *
 */
