//@ts-check

module.exports = function makeFindAllMerchandise({ merchandiseDb }) {
    return async function findAllMember() {
        const result = await merchandiseDb.findAll();

        if (!result) {
            throw new Error('No Merchandise found')
        }
        return result;
    }
}