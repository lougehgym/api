//@ts-check
const buildMembers = require('../../entities/members')
/**@param {MakeDatabase} Database */
module.exports = function MakeDatabase({ membersDb, membershipDb }) {
    /**
     * @returns {Promise<AddedMember>}
     * @param {Member} memberInfo
     */
    const addMember = async (memberInfo) => {

        /**  @param {Member} info  */
        const member = buildMembers(memberInfo);
        const hasMembership = await membershipDb.findById({ id: member.getMembershipId() })
        if (!hasMembership) {
            throw new Error('Unable to add Member, Membership not found')
        }
        const result = await membersDb.add({
            first_name: member.getFirstName(),
            middle_name: member.getMiddleName(),
            last_name: member.getLastName(),
            zip: member.getZip(),
            membershipId: member.getMembershipId(),
            membership_start_date: member.getMembershipStartDate(),
            membership_end_date: member.getMembershipEndDate(),
            email: member.getEmail(),
            contact_number: member.getContactNumber(),
            address: member.getAddress(),

        })
        if (!result) {
            throw new Error('Unable to add member')
        }
        return {
            message: `Added Member`,
            first_name: member.getFirstName(),
            middle_name: member.getMiddleName(),
            last_name: member.getLastName(),
            zip: member.getZip(),
            membershipId: member.getMembershipId(),
            membership_start_date: member.getMembershipStartDate(),
            membership_end_date: member.getMembershipEndDate(),
        };
    }
    return addMember;
};


/**
 * @typedef {Object} Member
 * @prop {number=} id
 * @prop {number=} membershipId
 * @prop {string=} firstName
 * @prop {string=} lastName
 * @prop {string=} zip
 * @prop {string=} middleName
 * @prop {string=} membershipStartDate,
 * @prop {string=} membershipEndDate
 *
 */

/**
 * @typedef {Object} AddedMember
 * @prop {string=} message
 * @prop {string=} first_name
 * @prop {string=} middle_name
 * @prop {string=} last_name
 * @prop {string=} zip
 * @prop {number=} membershipId
 * @prop {string=} membership_end_date
 * @prop {string=} membership_start_date
 */
/**
 *
 * @typedef {Object} Database
 * @prop {function=} findAll
 * @prop {function=} findById
 * @prop {function=} add
 * @prop {function=} update
 * @prop {function=} deleteById
 */

/**
 * @typedef {Object} MakeDatabase
 * @prop {Database} membersDb
 * @prop {Database} membershipDb
 *
 */