//@ts-check
/**@param {MakeDatabase} membersDb */
module.exports = function makeFindAllMember({ membersDb }) {
    return async function findAllMember() {
        /** @type {Array<string | number>} */
        const result = await membersDb.findAll();

        if (!result) {
            throw new Error('No members found')
        }
        return result;
    }
}
/**
 *
 * @typedef {Object} Database
 * @prop {function=} findAll
 * @prop {function=} findById
 * @prop {function=} add
 * @prop {function=} update
 * @prop {function=} deleteById
 */

/**
 * @typedef {Object} MakeDatabase
 * @prop {Database} membersDb
 *
 */