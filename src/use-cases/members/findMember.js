//@ts-check
const makeMember = require('../../entities/members')
/** @param {makeDatabase} makeFindMember*/
module.exports = function makeFindMember({ membersDb }) {
    return async function findMember({ id }) {
        if (isNaN(id)) {
            throw new Error('Id must be a number')
        }
        const result = await membersDb.findById({ id });
        if (!result) {
            throw new Error('No members found')
        }
        return result;
    }
}
/**
 *
 * @typedef {Object} Database
 * @prop {function=} findAll
 * @prop {function=} findById
 * @prop {function=} add
 * @prop {function=} update
 * @prop {function=} deleteById
 */

/**
 * @typedef {Object} makeDatabase
 * @prop {Database} membersDb
 *
 */