//@ts-check
const buildMembers = require('../../entities/members')
/** @param {makeDatabase} database  */
module.exports = function makeDeleteMember({ membersDb }) {
    return async function deleteMember({ id }) {
        /** @param {Member} member*/

        if (isNaN(id)) {
            throw new Error('Id must be a number')
        }
        const hasMember = await membersDb.findById({ id });
        if (!hasMember) {
            throw new Error('Member does not exist');
        }
        const result = await membersDb.deleteById({ id });

        if (!result) {
            throw new Error('No members found')
        }
        return {
            status: `Deleted`,
            message: `Successfully deleted member`,
            member: hasMember,
        };
    }
}

/**
 * @typedef {Object} Member
 * @prop {number=} id
 * @prop {number=} membership_id
 * @prop {string=} first_name
 * @prop {string=} last_name
 * @prop {string=} zip
 * @prop {string=} middle_name
 * @prop {string=} membership_start_date,
 * @prop {string=} membership_end_date
 *
 */

/**
 *
 * @typedef {Object} Database
 * @prop {function=} findAll
 * @prop {function=} findById
 * @prop {function=} add
 * @prop {function=} update
 * @prop {function=} deleteById
 */

/**
 * @typedef {Object} makeDatabase
 * @prop {Database} membersDb
 *
 */