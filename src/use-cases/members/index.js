//@ts-check

const makeAddMember = require('./addMember')
const membersDb = require('../../data-access/members')
const membershipDb = require('../../data-access/membership')
const makeFindAllMembers = require('./findAllMembers')
const makeFindMembers = require('./findMember')
const makeUpdateMember = require('./updateMember');
const makeDeleteMember = require('./deleteMember');
const addMember = makeAddMember({ membersDb, membershipDb })
const findAllMembers = makeFindAllMembers({ membersDb })
const findMember = makeFindMembers({ membersDb })
const updateMember = makeUpdateMember({ membersDb,membershipDb })
const deleteMember = makeDeleteMember({ membersDb })
module.exports = Object.freeze({
    addMember,
    findAllMembers,
    findMember,
    updateMember,
    deleteMember

})

