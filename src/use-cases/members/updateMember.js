//@ts-check
const makeMember = require('../../entities/members')
/** @param {MakeDatabase} database */
module.exports = function makeUpdateMember({ membersDb, membershipDb }) {

    return async function updateMember(memberInfo) {

        const member = makeMember(memberInfo);
        /**@type {Array} */

        const hasMember = await membersDb.findById({ id: member.getId() });
        if (!hasMember) {
            throw new Error('Member not found')
        }
        const hasMembership = await membershipDb.findById({ id: member.getMembershipId() })
        if (!hasMembership) {
            throw new Error('Unable to update Member, membership not found')
        }
        const result = await membersDb.update({
            id: member.getId(),
            first_name: member.getFirstName(),
            middle_name: member.getMiddleName(),
            last_name: member.getLastName(),
            membershipId: member.getMembershipId(),
            membership_start_date: member.getMembershipStartDate(),
            membership_end_date: member.getMembershipEndDate(),

        })
        if (!result) {
            throw new Error('Unable to add member')
        }
        return {
            message: `Updated Member`,
            id: member.getId(),
            first_name: member.getFirstName(),
            middle_name: member.getMiddleName(),
            last_name: member.getLastName(),
            membership_start_date: member.getMembershipStartDate(),
            membership_end_date: member.getMembershipEndDate(),
            membership: hasMembership
        };

    }
};


/**
 * @typedef {Object} MemberInfo
 * @prop {number=} id
 * @prop {number=} membershipId
 * @prop {string=} firstName
 * @prop {string=} lastName
 * @prop {number=} zip
 * @prop {string=} middleName
 * @prop {string=} membershipStartDate,
 * @prop {string=} membershipEndDate
 *
 */

/**
 *
 * @typedef {Object} Database
 * @prop {function=} findAll
 * @prop {function=} findById
 * @prop {function=} add
 * @prop {function=} update
 * @prop {function=} delete
 */

/**
 * @typedef {Object} MakeDatabase
 * @prop {Database} membersDb
 * @prop {Database} membershipDb
 *
 */