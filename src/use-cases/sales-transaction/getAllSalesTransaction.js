//@ts-check

module.exports = function makeFindAllSalesTransaction({ salesTransactionDb }) {
    const findAllSalesTransaction = async () => {

        const result = await salesTransactionDb.findAll()

        if (!result) {
            throw new Error('Unable to add Sales Transaction')
        }
        return result;
    }
    return findAllSalesTransaction;
};


