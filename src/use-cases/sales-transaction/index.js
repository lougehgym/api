//@ts-check

const salesTransactionDb = require('../../data-access/sales-transaction');
const merchandiseDb = require('../../data-access/merchandise');
const transactionDb = require('../../data-access/transactions');
const makeAddSalesTransaction = require('./addSalesTransaction')
const makeGetAllSalesTransaction = require('./getAllSalesTransaction');
const makeUpdateSalesTransaction = require('./updateSalesTransaction')
const makeRemoveSalesTransaction = require('./removeSalesTransaction')
const makeGetSalesTransaction = require('./getSalesTransaction')
const addSalesTransaction = makeAddSalesTransaction({ salesTransactionDb, merchandiseDb, transactionDb })
const findAllSalesTransaction = makeGetAllSalesTransaction({ salesTransactionDb })
const findSalesTransaction = makeGetSalesTransaction({ salesTransactionDb })
const updateSalesTransaction = makeUpdateSalesTransaction({ salesTransactionDb, merchandiseDb, transactionDb })
const removeSalesTransaction = makeRemoveSalesTransaction({ salesTransactionDb })
module.exports = { addSalesTransaction, findAllSalesTransaction, updateSalesTransaction, removeSalesTransaction, findSalesTransaction }