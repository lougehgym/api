//@ts-check

module.exports = function makeFindSalesTransaction({ salesTransactionDb }) {
    const findSalesTransaction = async ({ id }) => {
        if (isNaN(id)) {
            throw new Error(`ID must be a number`)
        }
        const result = await salesTransactionDb.findById({ id })
        console.log(result)
        if (!result) {
            throw new Error('Sales Transaction not found')
        }
        return result;
    }
    return findSalesTransaction;
};

