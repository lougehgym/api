//@ts-check

const buildSalesTransaction = require('../../entities/sales-transactions')

const makeUpdateSalesTransaction = ({ salesTransactionDb, merchandiseDb, transactionDb }) => {
    const updateSalesTransaction = async (salesTransactionInfo) => {
        const salesTransaction = buildSalesTransaction(salesTransactionInfo)

        const hasMerchandise = await merchandiseDb.findById({ id: salesTransaction.getMerchandiseId() });
        if (!hasMerchandise) {
            throw new Error(`Unable to update Sales Transaction, Merchandise not found`)
        }
        const hasTransaction = await transactionDb.findById({ id: salesTransaction.getTransactionId() })
        if (!hasTransaction) {
            throw new Error(`Unable to update Sales Transaction, Transaction not found`)
        }
        const result = await salesTransactionDb.update({
            id: salesTransaction.getId(),
            transactionId: salesTransaction.getTransactionId(),
            merchandiseId: salesTransaction.getMerchandiseId(),
            quantity_items: salesTransaction.getQuantity(),
        })

        if (!result) {
            throw new Error(`Unable to update Sales Transaction, something went wrong.`)
        }

        return {
            status: `Updated`,
            message: `Updated Sales Transaction`,
            id: salesTransaction.getId(),
            quantity_items: salesTransaction.getQuantity(),
            transaction:hasTransaction,
            merchandise:hasMerchandise,
        }
    }
    return updateSalesTransaction;
}

module.exports = makeUpdateSalesTransaction