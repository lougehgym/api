//@ts-check
const makeSalesTransaction = require('../../entities/sales-transactions')
module.exports = function makeAddSalesTransaction({ salesTransactionDb, merchandiseDb, transactionDb }) {
    const addSalesTransaction = async (salesTransactionInfo) => {
        const salesTransaction = makeSalesTransaction(salesTransactionInfo);

        const hasMerchandise = await merchandiseDb.findById({ id: salesTransaction.getMerchandiseId() })
        if (!hasMerchandise) {
            throw new Error(`Unable to add sales transaction, Merchandise not found`)
        }
        const hasTransaction = await transactionDb.findById({ id: salesTransaction.getTransactionId() })
        if (!hasTransaction) {
            throw new Error(`Unable to add sales transaction, Transaction not found`)
        }
        const result = await salesTransactionDb.add({
            transactionId: salesTransaction.getTransactionId(),
            merchandiseId: salesTransaction.getMerchandiseId(),
            quantity_items: salesTransaction.getQuantity(),

        })

        if (!result) {
            throw new Error('Unable to add Sales Transaction')
        }
        return {
            status: `Added`,
            message: `Added Sales Transaction`,
            quantity_items: salesTransaction.getQuantity(),
            transaction: hasTransaction,
            merchandise: hasMerchandise,

        };

    }
    return addSalesTransaction;
};


/**
 * @typedef {Object} MembershipInfo
 * @prop {number=} id
 * @prop {number=} price
 * @prop {string=} name
 *
 */
