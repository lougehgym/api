const makeRemoveSalesTransaction = ({ salesTransactionDb }) => {
    const removeSalesTransaction = async ({ id }) => {
        if (isNaN(id)) {
            throw new Error(`ID must be a character`)
        }

        const hasSalesTransaction = await salesTransactionDb.findById({ id });
        if (!hasSalesTransaction) {
            throw new Error(`Sales Transaction not found, Unable to delete Sales Transaction`)
        }
        const result = await salesTransactionDb.deleteById({ id })
        if (!result) {
            throw new Error(`Unable to delete Sales Transaction, something went wrong`)
        }
        return {
            status: `Deleted`,
            message: `Deleted Sales Transaction`,
            sales_transaction: hasSalesTransaction
        }
    }
    return removeSalesTransaction
}
module.exports = makeRemoveSalesTransaction;