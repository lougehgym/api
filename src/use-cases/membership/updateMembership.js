//@ts-check
const makeMembership = require('../../entities/membership')
module.exports = function makeUpdateMembership({ membershipDb }) {

    return async function updateMembership(membershipInfo) {

        const membership = makeMembership(membershipInfo);
        /**@type {Array} */

        const hasMembership = await membershipDb.findById({ id: membership.getId() });
        if (!hasMembership) {
            throw new Error('Membership not found')
        }
        const result = await membershipDb.update({
            id: membership.getId(),
            name: membership.getName(),
            price: membership.getPrice()

        })
        if (!result) {
            throw new Error('Unable to add member')
        }
        return {
            message: `Updated Membership`,
            id: membership.getId(),
            name: membership.getName(),
            price: membership.getPrice(),
        };

    }
};


/**
 * @typedef {Object} membershipInfo
 * @prop {number=} id
 * @prop {number=} membershipId
 * @prop {string=} firstName
 * @prop {string=} lastName
 * @prop {number=} zip
 * @prop {string=} middleName
 * @prop {string=} membershipStartDate,
 * @prop {string=} membershipEndDate
 *
 */
