//@ts-check

module.exports = function makeDeleteMembership({ membershipDb }) {
    return async function deleteMembership({ id }) {
        if (isNaN(id)) {
            throw new Error(`Id must be a number`)
        }

        const hasMember = await membershipDb.findById({ id });
        if (!hasMember) {
            throw new Error('Membership does not exist')
        }
        const result = await membershipDb.deleteById({ id });

        if (!result) {
            throw new Error('No members found')
        }
        return {
            status: `Deleted`,
            message: `Successfully deleted member`,
            member: hasMember,
        };
    }
}