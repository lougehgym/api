//@ts-check
const buildMembership = require('../../entities/membership')
module.exports = function makeAddMembership({ membershipDb }) {

    return async function addMembership(memberInfo) {

        const membership = buildMembership(memberInfo);
        const result = await membershipDb.add({
            name: membership.getName(),
            price: membership.getPrice(),
        })
        if (!result) {
            throw new Error('Unable to add membership')
        }
        return {
            message: `Added Membership`,
            name: membership.getName(),
            price: membership.getPrice(),
        };

    }
};


/**
 * @typedef {Object} MembershipInfo
 * @prop {number=} id
 * @prop {number=} price
 * @prop {string=} name
 *
 */
