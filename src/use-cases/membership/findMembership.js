//@ts-check
const makeMembership = require('../../entities/membership')
module.exports = function makeFindMembership({ membershipDb }) {
    return async function findMembership({ id }) {
        if (isNaN(id)) {

        }
        const result = await membershipDb.findById({ id });
        if (!result) {
            throw new Error('No membership found')
        }
        return result;
    }
}