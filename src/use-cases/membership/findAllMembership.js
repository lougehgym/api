//@ts-check

module.exports = function makeFindAllMember({ membershipDb }) {
    return async function findAllMember() {
        const result = await membershipDb.findAll();
        
        if (!result) {
            throw new Error('No membership found')
        }
        return result;
    }
}