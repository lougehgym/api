const makeAddMembership = require('./addMembership')
const makeFindAllMembership = require('./findAllMembership')
const makeFindMembership = require('./findMembership')
const makeUpdateMembership = require('./updateMembership')
const makeDeleteMembership = require('./deleteMembership')
const membershipDb = require('../../data-access/membership')

const addMembership = makeAddMembership({ membershipDb })
const findAllMembership = makeFindAllMembership({ membershipDb })
const findMembership = makeFindMembership({ membershipDb })
const updateMembership = makeUpdateMembership({ membershipDb })
const deleteMembership = makeDeleteMembership({ membershipDb })
module.exports = Object.freeze({
    addMembership,
    findAllMembership,
    findMembership,
    updateMembership,
    deleteMembership
})