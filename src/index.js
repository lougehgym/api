//@ts-check
require('dotenv').config;
const express = require('express')
const bodyParser = require('body-parser')
const app = express();
const PORT = process.env.PORT || 3000;
const routeMember = require('./routes/members')
const routeMembership = require('./routes/membership')
const routePass = require('./routes/oneDayPass')
const routeTransaction = require('./routes/transaction')
const routeMerchandise = require('./routes/merchandise')
const routePassCategory = require('./routes/passCategory')
// const routeMembersWithMembership = require('../routes/members_with_membership')
const routeSalesTransaction = require('./routes/sales_transaction')
// const routeMembersWithPass = require('../routes/members_day_pass');
// const routeMerchTransaction = require('../routes/transaction_merchandise')
const routeLogin = require('./routes/login');
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(routeMember)
app.use(routeTransaction)
app.use(routeMembership)
app.use(routePass)
app.use(routeMerchandise)
app.use(routePassCategory)
// app.use(routeMembersWithMembership)
app.use(routeSalesTransaction)
// app.use(routeMembersWithPass)
// app.use(routeMerchTransaction)
app.use(routeLogin)


app.listen(PORT, () => {

    console.log(`Server at localhost:${PORT}`)
})
