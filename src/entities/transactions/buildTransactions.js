

module.exports = ({ moment }) => {
    const makeTransaction = ({
        id = 1,
        date = moment(),
        memberId = null,
    } = {}) => {
        if (!id) {
            throw new Error(`Id must be provided`)
        }
        if (isNaN(id)) {
            throw new Error(`Id must be a number`)
        }
        if (!date) {
            throw new Error(`Date must be provided`)
        }
        if (isNaN(memberId) && memberId !== null) {
            throw new Error(`Member must be a number`)
        }

        return Object.freeze({
            getId: () => id,
            getDate: () => date,
            getMemberId: () => memberId
        })
    }
    return makeTransaction;
}