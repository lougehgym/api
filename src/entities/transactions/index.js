const buildTransaction = require('./buildTransactions');
const moment = require('moment')
const makeTransaction = buildTransaction({ moment });

module.exports = makeTransaction;