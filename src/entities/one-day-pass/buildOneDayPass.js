module.exports = () => {
    /** @param {OneDayPass} OneDayPass */
    const makeOneDayPass = ({
        id = 1,
        passcat_id = 1,
        date = new Date().toString(),
        member_id = 1,

    } = {}) => {
        if (!id) {
            throw new Error('ID must be provided')
        }
        if (isNaN(id)) {
            throw new Error('ID must be a number')
        }
        if (!passcat_id) {
            throw new Error('Pass Category ID must be provided')
        }
        if (isNaN(passcat_id)) {
            throw new Error('Pass Category must be a number')
        }
        if (!member_id) {
            throw new Error('Member ID must be provided')
        }
        if (isNaN(member_id)) {
            throw new Error('Member ID must be a number')
        }
        if (!date) {
            throw new Error('Date must be provided')
        }
        return Object.freeze({
            getId: () => id,
            getPassCatId: () => passcat_id,
            getMemberId: () => member_id,
            getDate: () => date,
        })
    }
    return makeOneDayPass;
}

/**
 * @typedef {Object} OneDayPass
 * @prop {number=} id
 * @prop {number=} passcat_id
 * @prop {number=} member_id
 * @prop {string=} date
 */