//@ts-check

const buildPassCategory = require('./buildPassCategory');

const makePassCategory = buildPassCategory();

module.exports = makePassCategory;