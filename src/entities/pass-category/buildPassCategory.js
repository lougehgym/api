//@ts-check

module.exports = () => {
    /**@param {PassCategory} passCategory */
    const makePassCategory = ({
        id = 1,
        name = 'a',
        price = 1,
    } = {}) => {
        if (!id) { 
            throw new Error('ID must be provided')
        }
        if (isNaN(id)) {
            throw new Error('Id must be a number')
        }
        if (!name) {
            throw new Error('Name must be provided')
        }
        if (!price) {
            throw new Error('Price must be provided')
        }
        if (isNaN(price)) {
            throw new Error('Price must be a number')
        }

        return Object.freeze({
            getId: () => id,
            getName: () => name,
            getPrice: () => price,
        })
    }
    return makePassCategory;
}


/**
 * @typedef {Object} PassCategory
 * @prop {number=} id
 * @prop {string=} name
 * @prop {number=} price
 */