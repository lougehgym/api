//@ts-check

module.exports = () => {
    /**
     * 
     * @param {Merchandise} merchandise 
     */
    const buildMerchandise = ({
        id = 1,
        name = 'a',
        quantity = 1,
        price = 1
    }) => {
        if (!id) {
            throw new Error('Id must be provided')
        }
        if (isNaN(id)) {
            throw new Error('Id must be a number')
        }
        if (!name) {
            throw new Error('Name must be provided')
        }
        if (!quantity) {
            throw new Error('Quantity must be provided')
        }
        if (isNaN(quantity)) {
            throw new Error('Quantity must be a number')
        }
        if (!price) {
            throw new Error('Price must be provided')
        }
        if (isNaN(price)) {
            throw new Error('Price must be a number')
        }
        return Object.freeze({
            getId: () => id,
            getName: () => name,
            getQuantity: () => quantity,
            getPrice: () => price
        })
    }

    return buildMerchandise
}


/**
 * @typedef {Object} Merchandise
 * @prop {number=} id
 * @prop {string=} name
 * @prop {number=} quantity
 * @prop {number=} price
 */