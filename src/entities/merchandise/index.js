const buildMerchandise = require('./buildMerchandise');

const makeMerchandise = buildMerchandise();

module.exports = makeMerchandise;
