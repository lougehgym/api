
const { makeFakeMembership } = require('../../__test__/dummyData')
const makeMembership = require('./')
describe('Entities:membership Input Validation', () => {
    beforeAll(() => {

        const membership = makeFakeMembership();
        console.log(membership)
    })
    it('should require a name', () => {
        const membership = makeFakeMembership({ name: null })
        expect(() => makeMembership(membership)).toThrow()
    })
    it('should have no numbers in name', () => {
        const membership = makeFakeMembership({ name: "1234" })
        expect(() => makeMembership(membership)).toThrow()
    })


    it('should have  require price', () => {
        const membership = makeFakeMembership({ price: null })
        expect(() => makeMembership(membership)).toThrow()
    })
    it('should have no character in price', () => {
        const membership = makeFakeMembership({ price: "abc" })
        expect(() => makeMembership(membership)).toThrow()
    })

    it('should pass using number', () => {
        const membership = makeFakeMembership({ price: 5123.6 })
        expect(() => makeMembership(membership)).toBeTruthy()
    })
    it('should pass all input field', () => {
        //Random data from Faker.js
        const membership = makeFakeMembership();
        expect(() => makeMembership(membership)).toBeTruthy();
    })
})