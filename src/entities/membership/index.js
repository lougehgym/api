const buildMembership = require('./buildMembership');

const makeMembership = buildMembership();

module.exports = makeMembership;
