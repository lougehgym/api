//@ts-check

module.exports = () => {


    /**@param {membership} membership*/
    const buildMembership = ({
        id = 1,
        name,
        price,
    } = {}) => {
        if (!id) {
            throw new Error('Id must be provided')
        }
        if (isNaN(id)) {
            throw new Error('Id must be a number')
        }
        if (!name) {
            throw new Error('name must be provided')
        }
        if(name.match(/^[0-9]+$/g)){
            throw new Error('Membership should not have a number')
        }
        if (!price) {
            throw new Error('Price must be provided')
        }
        if (isNaN(price)) {
            throw new Error('Price must be a number')
        }
        return Object.freeze({
            getId: () => id,
            getName: () => name,
            getPrice: () => price,

        })
    }

    return buildMembership;
}


/**
 * @typedef {Object} membership
 * @prop {string=} name
 * @prop {number=} id
 * @prop {number=} price
 *
 */
