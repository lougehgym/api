const buildMembers = require('./buildMembers');
const moment = require('moment')
const makeMembers = buildMembers({moment});

module.exports = makeMembers;
