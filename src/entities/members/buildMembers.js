//@ts-check

module.exports = ({ moment }) => {


    /**@param {Member} member*/
    const buildMembers = ({
        id = 1,
        membershipId,
        first_name,
        last_name,
        middle_name,
        zip,
        membership_start_date = moment(),
        membership_end_date = moment(),
        contact_number,
        email,
        address
    } = {}) => {
        if (!id) {
            throw new Error('Id must be provided')
        }
        if (isNaN(id)) {
            throw new Error('Id must be a number')
        }
        if (!first_name) {
            throw new Error('First name must be provided')
        }
        if (first_name.match(/\d/)) {
            throw new Error('First name must be a character')
        }
        if ((last_name.match(/\d/))) {
            throw new Error('Last name must be a character')
        }

        if (zip.match(/^[A-Za-z]+$/g)) {
            throw new Error('Zip must be a valid format')
        }
        if (!membershipId) {
            throw new Error('Membership ID must be provided')
        }
        if (isNaN(membershipId)) {
            throw new Error('Membership ID must be a number')
        }
        if (!membership_start_date) {
            throw new Error('Membership start date must be provided')
        }
        if (!membership_end_date) {
            throw new Error('Membership end date must be provided')
        }
        if (!address) {
            throw new Error('Email must be provided')
        }
        if(!contact_number){
            throw new Error('Contact Number must be provided')
        }
        return Object.freeze({
            getId: () => id,
            getFirstName: () => first_name,
            getLastName: () => last_name,
            getMiddleName: () => middle_name,
            getMembershipId: () => membershipId,
            getMembershipStartDate: () => membership_start_date,
            getMembershipEndDate: () => membership_end_date,
            getZip: () => zip,
            getContactNumber: () => contact_number,
            getEmail: () => email,
            getAddress: () => address
        })
    }

    return buildMembers;
}


/**
 * @typedef {Object} Member
 * @prop {number=} id
 * @prop {number=} membershipId
 * @prop {string=} first_name
 * @prop {string=} last_name
 * @prop {string=} zip
 * @prop {string=} middle_name
 * @prop {string=} membership_start_date,
 * @prop {string=} membership_end_date
 * @prop {string=} address
 * @prop {string=} contact_number
 * @prop {string=} email
 *
 */
