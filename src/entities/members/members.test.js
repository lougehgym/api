const makeMember = require('./')
const { makeFakeMember } = require('../../__test__/dummyData')


describe('Entities:Member Input Validation', () => {
    beforeAll(() => {

        const member = makeFakeMember();
        console.log(member)
    })
    it('should require first name', () => {
        const member = makeFakeMember({ first_name: null })
        expect(() => makeMember(member)).toThrow('First name must be provided')
    })
    it('should have no numbers in first name', () => {
        const member = makeFakeMember({ first_name: "1234" })
        expect(() => makeMember(member)).toThrow('First name must be a character')
    })
    it('should have no characters in Id', () => {
        const member = makeFakeMember({ id: `qwe` })
        expect(() => makeMember(member)).toThrow('Id must be a number')
    })
    it('should not pass if zip has a character', () => {
        const member = makeFakeMember({ zip: "abc" })
        expect(() => makeMember(member)).toThrow('Zip must be a valid format')
    })

    it('should not pass if membership_id is a character', () => {
        const member = makeFakeMember({ membership_id: "abc" })
        expect(() => makeMember(member)).toThrow('Membership ID must be a number')
    })
    it('should not pass if membership_id is null', () => {
        const member = makeFakeMember({ membership_id: null })
        expect(() => makeMember(member)).toThrow('Membership ID must be provided')
    })
    it('should pass all input field', () => {
        //Random data from Faker.js
        const member = makeFakeMember();
        expect(() => makeMember(member)).toBeTruthy();
    })
})