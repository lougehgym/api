//@ts-check

module.exports = () => {

    const buildTransaction = ({
        id = 1,
        merchandiseId,
        transactionId,
        quantity_items,
    }) => {
        if (!id) {
            throw new Error(`Id must be provided`)
        }
        if (isNaN(id)) {
            throw new Error(`Id must be a character`)
        }
        if (!merchandiseId) {
            throw new Error(`Merchandise ID must be provided`)
        }
        if (isNaN(merchandiseId)) {
            throw new Error(`Merchandise ID must be a number`)
        }
        if (!transactionId) {
            throw new Error(`Transaction ID must be provided`)
        }
        if (isNaN(id)) {
            throw new Error(`Transaction ID must be a number`)
        }
        if (!quantity_items) {
            throw new Error(`quantity_items must be provided`)
        }
        if (isNaN(quantity_items)) {
            throw new Error(`quantity_items must be a number`)
        }
        return Object.freeze({
            getId: () => id,
            getTransactionId: () => transactionId,
            getMerchandiseId: () => merchandiseId,
            getQuantity: () => quantity_items
        })
    }
    return buildTransaction;
}