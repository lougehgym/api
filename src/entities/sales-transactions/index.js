//@ts-check

const buildSalesTransaction = require('./buildSalesTransaction');

const makeSalesTransaction = buildSalesTransaction();

module.exports = makeSalesTransaction;