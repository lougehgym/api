//@ts-check

const express = require('express')
const router = express.Router();
const makeCallBack = require('../express-callback')
const { postTransaction, getAllTransaction, getTransaction, putTransaction,deleteTransaction } = require('../controllers/transactions');
const middleware = require('./verification')
router.get('/api/transaction', middleware.verifyToken, makeCallBack(getAllTransaction))
router.get('/api/transaction/:id', middleware.verifyToken, makeCallBack(getTransaction))
router.post('/api/transaction', middleware.verifyToken, makeCallBack(postTransaction))
router.put('/api/transaction/:id', middleware.verifyToken, makeCallBack(putTransaction))
router.delete('/api/transaction/:id', middleware.verifyToken, makeCallBack(deleteTransaction))
module.exports = router;

