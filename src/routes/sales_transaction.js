//@ts-check

const express = require('express')
const router = express.Router();
const salesTransaction = require('../../database/sales_transaction');
const makeCallBack = require('../express-callback');
const { postSalesTransaction, getAllSalesTransaction, putSalesTransaction, deleteSalesTransaction, getSalesTransaction } = require('../controllers/sales-transaction');

const middleware = require('./verification')
router.get('/api/sales_transaction', middleware.verifyToken, makeCallBack(getAllSalesTransaction))
router.get('/api/sales_transaction/:id', middleware.verifyToken, makeCallBack(getSalesTransaction))
router.post('/api/sales_transaction', middleware.verifyToken, makeCallBack(postSalesTransaction))
router.put('/api/sales_transaction/:id', middleware.verifyToken, makeCallBack(putSalesTransaction))
router.delete('/api/sales_transaction/:id', middleware.verifyToken, makeCallBack(deleteSalesTransaction))
module.exports = router;

