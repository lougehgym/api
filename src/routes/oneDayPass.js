//@ts-check

const express = require('express')
const router = express.Router();
const oneDayPass = require('../../database/oneDaypass');
const makeCallback = require('../express-callback')
const { postOneDayPass, getAllOneDayPass, getOneDayPass, putOneDayPass, deleteOneDayPass } = require('.././controllers/one-day-pass')
const middleware = require('./verification')
router.get('/api/one_day_pass', middleware.verifyToken, makeCallback(getAllOneDayPass))
router.get('/api/one_day_pass/:id', middleware.verifyToken, makeCallback(getOneDayPass))
router.post('/api/one_day_pass', middleware.verifyToken, makeCallback(postOneDayPass))
router.put('/api/one_day_pass/:id', middleware.verifyToken, makeCallback(putOneDayPass))
router.delete('/api/one_day_pass/:id', middleware.verifyToken, makeCallback(deleteOneDayPass))
module.exports = router;

