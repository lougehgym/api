//@ts-check

const express = require('express')
const router = express.Router();
const merchandise = require('../../database/merchandise');
const makeCallback = require('../express-callback')
const { postMerchandise, getAllMerchandise, getMerchandise, putMerchandise, deleteMerchandise } = require('../controllers/merchandise')

const middleware = require('./verification')
router.get('/api/merchandise', middleware.verifyToken, makeCallback(getAllMerchandise))
router.get('/api/merchandise/:id', middleware.verifyToken, makeCallback(getMerchandise))
router.post('/api/merchandise', middleware.verifyToken, makeCallback(postMerchandise))
router.put('/api/merchandise/:id', middleware.verifyToken, makeCallback(putMerchandise))
router.delete('/api/merchandise/:id', middleware.verifyToken, makeCallback(deleteMerchandise))
module.exports = router;

