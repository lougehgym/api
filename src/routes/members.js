//@ts-check

const express = require('express')
const router = express.Router();
const members = require('../../database/members');
const middleware = require('./verification');
const { postMember, getAllMember, getMember, putMember, deletedMember } = require('../controllers/members')
const makeCallBack = require('../express-callback');
router.get('/api/members', middleware.verifyToken, makeCallBack(getAllMember))
router.get('/api/members/:id', middleware.verifyToken, makeCallBack(getMember))
router.post('/api/members/', middleware.verifyToken, makeCallBack(postMember))
router.put('/api/members/:id', middleware.verifyToken, makeCallBack(putMember))
router.delete('/api/members/:id', middleware.verifyToken, makeCallBack(deletedMember))
module.exports = router;

