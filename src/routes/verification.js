//@ts-check
const db = require('../../database/db');
const jwt = require('jsonwebtoken');


const validateToken = async ({ token }) => {
    const result = await db.query("SELECT token FROM USERS WHERE ID=$1", [token])
    return result;
}
const verifyToken = async (req, res, next) => {
    let id;
    const bearerHeader = req.headers['authorization'];
    try {
        if (typeof bearerHeader !== 'undefined') {
            const bearer = bearerHeader.split(" ");
            const bearerToken = bearer[1];
            req.token = bearerToken;
            let decodeToken = jwt.decode(req.token)
            if (!decodeToken) {
                throw 403
            }
            if (validateTokenValue(decodeToken) === true) {
                throw 403
            }
            jwt.verify(req.token, 'secretkey', (err, authData) => {
                if (err) { throw 403 }
                if (authData) { id = authData.id; }
            })

            const token = await validateToken({ token: id });
            if (!token.rowCount) {
                throw 403;
            }
            next();
        }
        else {
            throw 403;
        }
    } catch (err) {
        res.sendStatus(err);
    }
}
const validateTokenValue = (decodeToken) => {
    if (!decodeToken.id) {
        return true
    }
}
module.exports = { verifyToken, validateToken };