//@ts-check

const express = require('express')
const router = express.Router();
const membersWithPass = require('../database/members_day_pass');

router.get('/api/members_membership', membersWithPass.getMembersWithPass)
module.exports = router;

