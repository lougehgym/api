//@ts-check

const express = require('express')
const router = express.Router();
const transactionMerchandise = require('../database/transaction_merchandise');

router.get('/api/transaction_merchandise', transactionMerchandise.getAllMerchTransaction)
module.exports = router;

