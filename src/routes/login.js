const login = require('../../database/login')
const express = require('express');
const router = express.Router();
const middleware = require('./verification')

router.post('/login/', login.loginUser);
router.post('/api/users/',middleware.verifyToken,login.createUser);

module.exports = router