//@ts-check

const express = require('express')
const router = express.Router();
const membersWithMembership = require('../../database/members_membership');

router.get('/api/members_membership', membersWithMembership.getAllMembersWithMembership)
module.exports = router;

