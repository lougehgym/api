//@ts-check

const express = require('express')
const router = express.Router();
const membership = require('../../database/membership');
const { postMembership, getAllMembership, getMembership, putMembership, deletedMembership } = require('../controllers/membership')
const makeCallBack = require('../express-callback')
const middleware = require('./verification')
router.get('/api/membership', middleware.verifyToken, makeCallBack(getAllMembership))
router.get('/api/membership/:id', middleware.verifyToken, makeCallBack(getMembership))
router.post('/api/membership', middleware.verifyToken, makeCallBack(postMembership))
router.put('/api/membership/:id', middleware.verifyToken, makeCallBack(putMembership))
router.delete('/api/membership/:id', middleware.verifyToken, makeCallBack(deletedMembership))
module.exports = router;

