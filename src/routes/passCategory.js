//@ts-check
'use strict'
const express = require('express')
const router = express.Router();
const makeCallBack = require('../express-callback');
const { postPassCategory, getPassCategory, getAllPassCategory, putPassCategory, deletePassCategory } = require('../controllers/pass-category')
const middleware = require('./verification')
router.get('/api/pass_category', middleware.verifyToken, makeCallBack(getAllPassCategory))
router.get('/api/pass_category/:id', middleware.verifyToken, makeCallBack(getPassCategory))
router.post('/api/pass_category', middleware.verifyToken, makeCallBack(postPassCategory))
router.put('/api/pass_category/:id', middleware.verifyToken, makeCallBack(putPassCategory))
router.delete('/api/pass_category/:id', middleware.verifyToken, makeCallBack(deletePassCategory))
module.exports = router;

