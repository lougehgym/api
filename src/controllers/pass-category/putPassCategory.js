//@ts-check

const makePutPassCategory = ({ updatePassCategory }) => {
    const putPassCategory = async (httpRequest) => {
        try {
            const { source = {}, ...merchandiseInfo } = httpRequest.body;
            const { id } = httpRequest.params;
            const updatedPassCategory = await updatePassCategory({
                ...merchandiseInfo,
                id
            })
            return {
                headers: {
                    'Content-Type': 'application/json',
                },
                statusCode: 200,
                body: { ...updatedPassCategory }
            }
        } catch (err) {
            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }
    }
    return putPassCategory;
}
module.exports = makePutPassCategory