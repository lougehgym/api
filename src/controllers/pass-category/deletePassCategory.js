const makeDeletePassCategory = ({ removePassCategory }) => {
    const deletePassCategory = async (httpRequest) => {
        try {
            const id = httpRequest.params.id
            const deletedMerchandise = await removePassCategory({ id })
            return {
                headers: {
                    'Content-Type': 'application/json',
                },
                statusCode: 200,
                body: deletedMerchandise
            }
        } catch (err) {
            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }
    }
    return deletePassCategory;
}
module.exports = makeDeletePassCategory;

