const makeGetAllPassCategory = ({ findAllPassCategory }) => {
    const getAllPassCategory = async (httpRequest) => {
        try {
            const foundPassCategory = await findAllPassCategory()
            return {
                headers: {
                    'Content-Type': 'application/json',
                },
                statusCode: 200,
                body: foundPassCategory
            }
        } catch (err) {
            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }
    }
    return getAllPassCategory;
}
module.exports = makeGetAllPassCategory