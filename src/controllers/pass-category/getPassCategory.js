const makeGetPassCategory = ({ findPassCategory }) => {
    const getPassCategory = async (httpRequest) => {
        try {
            const id = httpRequest.params.id
            const foundPassCategory = await findPassCategory({ id })
            return {
                headers: {
                    'Content-Type': 'application/json',
                },
                statusCode: 200,
                body: foundPassCategory
            }
        } catch (err) {
            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }
    }
    return getPassCategory;
}
module.exports = makeGetPassCategory