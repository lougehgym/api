//@ts-check
const { addPassCategory, findAllPassCategory, findPassCategory, updatePassCategory, removePassCategory } = require('../../use-cases/pass-category')


const makePostPassCategory = require('./postPassCategory')
const makeGetAllPassCategory = require('./getAllPassCategory')
const makeGetPassCategory = require('./getPassCategory')
const makePutPassCategory = require('./putPassCategory')
const makeDeletePassCategory = require('./deletePassCategory')
const postPassCategory = makePostPassCategory({ addPassCategory })
const getAllPassCategory = makeGetAllPassCategory({ findAllPassCategory })
const getPassCategory = makeGetPassCategory({ findPassCategory })
const putPassCategory = makePutPassCategory({ updatePassCategory })
const deletePassCategory = makeDeletePassCategory({ removePassCategory })
const merchandiseController = Object.freeze({
    postPassCategory,
    getAllPassCategory,
    getPassCategory,
    putPassCategory,
    deletePassCategory,
});

module.exports = merchandiseController;

module.exports = {
    postPassCategory,
    getAllPassCategory,
    getPassCategory,
    putPassCategory,
    deletePassCategory,
}