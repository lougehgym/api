
const makePostPassCategory = ({ addPassCategory }) => {
    const postPassCategory = async (httpRequest) => {
        try {
            const { source = {}, ...passCategoryInfo } = httpRequest.body;
            const postedPassCategory = await addPassCategory({
                ...passCategoryInfo
            })
            return {
                headers: {
                    'Content-Type': 'application/json',

                },
                statusCode: 201,
                body: { ...postedPassCategory }
            }
        } catch (err) {
            console.log(err.message)
            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }
    }
    return postPassCategory;
}
module.exports = makePostPassCategory