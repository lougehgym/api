
const makePostOneDayPass = ({ addOneDayPass }) => {
    const postOneDayPass = async (httpRequest) => {
        try {
            const { source = {}, ...oneDayPassInfo } = httpRequest.body;
            const postedOneDayPass = await addOneDayPass({
                ...oneDayPassInfo
            })
            return {
                headers: {
                    'Content-Type': 'application/json',

                },
                statusCode: 201,
                body: { ...postedOneDayPass }
            }
        } catch (err) {
            console.log(err)
            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }
    }
    return postOneDayPass;
}
module.exports = makePostOneDayPass