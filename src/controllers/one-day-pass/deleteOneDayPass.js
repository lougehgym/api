const makeDeleteOneDayPass = ({ removeOneDayPass }) => {
    const deleteOneDayPass = async (httpRequest) => {
        try {
            const id = httpRequest.params.id
            const deletedOneDayPass = await removeOneDayPass({ id })
            return {
                headers: {
                    'Content-Type': 'application/json',
                },
                statusCode: 200,
                body: deletedOneDayPass
            }
        } catch (err) {
            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }
    }
    return deleteOneDayPass;
}
module.exports = makeDeleteOneDayPass