//@ts-check

const makeGetAllOneDayPass = ({ findAllOneDayPass }) => {
    const getAllOneDayPass = async (httpRequest) => {
        try {
            const foundAllOneDayPass = await findAllOneDayPass()
            return {
                headers: {
                    'Content-Type': 'application/json',
                },
                statusCode: 200,
                body: foundAllOneDayPass
            }
        } catch (err) {
            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }
    }
    return getAllOneDayPass;
}
module.exports = makeGetAllOneDayPass