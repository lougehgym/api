
//@ts-check

const { addOneDayPass, findAllOneDayPass, findOneDayPass, updateOneDayPass, removeOneDayPass } = require('../../use-cases/one-day-pass')



const makePostOneDayPass = require('./postOneDayPass');
const makeGetAllOneDayPass = require('./getAllOneDayPass')
const makeGetOneDayPass = require('./getOneDayPass');
const makePutOneDayPass = require('./putOneDayPass');
const makeDeleteOneDayPass = require('./deleteOneDayPass')



const postOneDayPass = makePostOneDayPass({ addOneDayPass })
const getAllOneDayPass = makeGetAllOneDayPass({ findAllOneDayPass })
const getOneDayPass = makeGetOneDayPass({ findOneDayPass })
const putOneDayPass = makePutOneDayPass({ updateOneDayPass })
const deleteOneDayPass = makeDeleteOneDayPass({ removeOneDayPass })

const OneDayPassController = Object.freeze({
    postOneDayPass,
    getAllOneDayPass,
    getOneDayPass,
    putOneDayPass,
    deleteOneDayPass
})


module.exports = OneDayPassController;
module.exports = {
    postOneDayPass,
    getAllOneDayPass,
    getOneDayPass,
    putOneDayPass,
    deleteOneDayPass
}