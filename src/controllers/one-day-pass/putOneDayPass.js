//@ts-check

const makePutOneDayPass = ({ updateOneDayPass }) => {
    const putOneDayPass = async (httpRequest) => {
        try {
            const { source = {}, ...oneDayPassInfo } = httpRequest.body;
            const { id } = httpRequest.params;
            const updatedOneDayPass = await updateOneDayPass({
                ...oneDayPassInfo,
                id
            })
            return {
                headers: {
                    'Content-Type': 'application/json',
                },
                statusCode: 200,
                body: { ...updatedOneDayPass }
            }
        } catch (err) {
            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }
    }
    return putOneDayPass;
}
module.exports = makePutOneDayPass