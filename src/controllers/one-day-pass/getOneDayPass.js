//@ts-check

const makeGetOneDayPass = ({ findOneDayPass }) => {
    const getOneDayPass = async (httpRequest) => {
        try {
            const { id } = httpRequest.params;
            const foundOneDayPass = await findOneDayPass({
                id
            })
            return {
                headers: {
                    'Content-Type': 'application/json',
                },
                statusCode: 200,
                body: foundOneDayPass
            }
        } catch (err) {
            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }
    }
    return getOneDayPass;
}
module.exports = makeGetOneDayPass