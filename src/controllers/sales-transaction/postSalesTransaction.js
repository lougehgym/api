
const makePostSalesTransaction = ({ addSalesTransaction }) => {
    const postSalesTransaction = async (httpRequest) => {
        try {
            const { source = {}, ...salesTransactionInfo } = httpRequest.body;
            const postedSalesTransaction = await addSalesTransaction({
                ...salesTransactionInfo
            })
            return {
                headers: {
                    'Content-Type': 'application/json',

                },
                statusCode: 201,
                body: { ...postedSalesTransaction }
            }
        } catch (err) {
            console.log(err.message)
            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }
    }
    return postSalesTransaction;
}
module.exports = makePostSalesTransaction