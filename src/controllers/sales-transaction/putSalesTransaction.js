
const makePutSalesTransaction = ({ updateSalesTransaction }) => {
    const putSalesTransaction = async (httpRequest) => {
        try {
            const { id } = httpRequest.params;
            const { source = {}, ...salesTransactionInfo } = httpRequest.body;
            const updatedSalesTransaction = await updateSalesTransaction({
                ...salesTransactionInfo,
                id
            })
            return {
                headers: {
                    'Content-Type': 'application/json',

                },
                statusCode: 201,
                body: { ...updatedSalesTransaction }
            }
        } catch (err) {
            console.log(err.message)
            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }
    }
    return putSalesTransaction;
}
module.exports = makePutSalesTransaction