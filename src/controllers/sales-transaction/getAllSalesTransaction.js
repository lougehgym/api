const makeGetAllTransaction = ({ findAllSalesTransaction }) => {
    const getAllSalesTransaction = async (httpRequest) => {
        try {
            const foundAllSalesTransaction = await findAllSalesTransaction();
            return {
                headers: {
                    'Content-Type': 'application/json',

                },
                statusCode: 201,
                body: foundAllSalesTransaction 
            }
        } catch (err) {
            console.log(err.message)
            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }
    }
    return getAllSalesTransaction;
}

module.exports = makeGetAllTransaction;