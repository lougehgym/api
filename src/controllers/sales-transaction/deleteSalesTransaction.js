const makeDeleteSalesTransaction = ({ removeSalesTransaction }) => {
    const deleteSalesTransaction = async (httpRequest) => {
        try {
            const { id } = httpRequest.params
            const deletedSalesTransaction = await removeSalesTransaction({ id });
            return {
                headers: {
                    'Content-Type': 'application/json',

                },
                statusCode: 200,
                body: deletedSalesTransaction
            }
        } catch (err) {
            console.log(err)
            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }
    }
    return deleteSalesTransaction;
}

module.exports = makeDeleteSalesTransaction;