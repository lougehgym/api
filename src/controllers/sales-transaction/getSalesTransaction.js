const makeGetSalesTransaction = ({ findSalesTransaction }) => {
    const getSalesTransaction = async (httpRequest) => {
        try {
            const id = httpRequest.params.id
            const foundSalesTransaction = await findSalesTransaction({ id })
            return {
                headers: {
                    'Content-Type': 'application/json',
                },
                statusCode: 200,
                body: foundSalesTransaction
            }
        } catch (err) {
            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }
    }
    return getSalesTransaction;
}
module.exports = makeGetSalesTransaction