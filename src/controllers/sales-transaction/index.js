//@ts-check
const makePostSalestransaction = require('./postSalesTransaction');
const makeGetAllSalesTransaction = require('./getAllSalesTransaction');
const makePutSalesTransaction = require('./putSalesTransaction')
const makeDeleteSalesTransaction = require('./deleteSalesTransaction')
const makeGetSalesTransaction = require('./getSalesTransaction')
const { addSalesTransaction, findAllSalesTransaction, updateSalesTransaction, removeSalesTransaction, findSalesTransaction } = require('../../use-cases/sales-transaction')
const postSalesTransaction = makePostSalestransaction({ addSalesTransaction });
const getAllSalesTransaction = makeGetAllSalesTransaction({ findAllSalesTransaction })
const getSalesTransaction = makeGetSalesTransaction({ findSalesTransaction })
const putSalesTransaction = makePutSalesTransaction({ updateSalesTransaction })
const deleteSalesTransaction = makeDeleteSalesTransaction({ removeSalesTransaction })
const salesTransactionController = Object.freeze({
    postSalesTransaction, getAllSalesTransaction, putSalesTransaction, deleteSalesTransaction, getSalesTransaction
})

module.exports = salesTransactionController;
module.exports = { postSalesTransaction, getAllSalesTransaction, putSalesTransaction, deleteSalesTransaction, getSalesTransaction }



