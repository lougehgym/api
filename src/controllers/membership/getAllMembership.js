const makeGetAllMembership = ({ findAllMembership }) => {
    const getAllMembership = async (httpRequest) => {
        try {
            const foundMembers = await findAllMembership()
            return {
                headers: {
                    'Content-Type': 'application/json',
                },
                statusCode: 200,
                body: foundMembers
            }
        } catch (err) {
            console.log(err)
            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }
    }
    return getAllMembership;
}
module.exports = makeGetAllMembership