const makeGetMembership = ({ findMembership }) => {
    const getMembership = async (httpRequest) => {
        try {
            const id = httpRequest.params.id
            const foundMembers = await findMembership({ id })
            return {
                headers: {
                    'Content-Type': 'application/json',
                },
                statusCode: 200,
                body: foundMembers
            }
        } catch (err) {
            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }
    }
    return getMembership;
}
module.exports = makeGetMembership