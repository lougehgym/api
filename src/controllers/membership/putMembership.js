const makePutMembership = ({ updateMembership }) => {
    const putMembership = async (httpRequest) => {
        try {
            const { source = {}, ...memberInfo } = httpRequest.body;
            const { id } = httpRequest.params;
            const updatedMember = await updateMembership({
                ...memberInfo,
                id
            })
            return {
                headers: {
                    'Content-Type': 'application/json',
                },
                statusCode: 200,
                body: { ...updatedMember }
            }
        } catch (err) {
            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }
    }
    return putMembership;
}
module.exports = makePutMembership