const makeDeleteMembership = ({ deleteMembership }) => {
    const deletedMembership = async (httpRequest) => {
        try {
            const id = httpRequest.params.id
            const foundDeletedMembership = await deleteMembership({ id })
            return {
                headers: {
                    'Content-Type': 'application/json',
                },
                statusCode: 200,
                body: foundDeletedMembership
            }
        } catch (err) {
            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }
    }
    return deletedMembership;
}
module.exports = makeDeleteMembership