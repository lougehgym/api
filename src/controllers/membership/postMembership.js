//@ts-check

const makePostMembership = ({ addMembership }) => {
    const postMembership = async (httpRequest) => {
        try {
            const { source = {}, ...membershipInfo } = httpRequest.body;
            const postedMembership = await addMembership({
                ...membershipInfo
            })
            return {
                headers: {
                    'Content-Type': 'application/json',

                },
                statusCode: 201,
                body: { ...postedMembership }
            }
        } catch (err) {
            return {
                headers: { 
                    'Content-Type': 'application/json'
                },
                statusCode: 400,
                body: {  
                    error: err.message
                }
            }
        }
    }
    return postMembership;
}
module.exports = makePostMembership