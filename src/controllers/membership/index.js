const makePostMembership = require('.././membership/postMembership')
const { addMembership, findAllMembership, findMembership, updateMembership, deleteMembership } = require('../../use-cases/membership');
const makeGetAllMembership = require('.././membership/getAllMembership')
const makeGetMembership = require('.././membership/getMembership')
const makePutMembership = require('.././membership/putMembership')
const makeDeleteMembership = require('.././membership/deleteMembership');
const postMembership = makePostMembership({ addMembership })
const getAllMembership = makeGetAllMembership({ findAllMembership })
const getMembership = makeGetMembership({ findMembership })
const putMembership = makePutMembership({ updateMembership })
const deletedMembership = makeDeleteMembership({ deleteMembership })
const membershipController = Object.freeze({
    postMembership,
    getAllMembership,
    getMembership,
    putMembership,
    deletedMembership
})
module.exports = membershipController;
module.exports = { postMembership, getAllMembership, getMembership, putMembership, deletedMembership }