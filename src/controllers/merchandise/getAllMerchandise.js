const makeGetAllMerchandise = ({ findAllMerchandise }) => {
    const getAllMerchandise = async (httpRequest) => {
        try {
            const foundMerchandise = await findAllMerchandise()
            return {
                headers: {
                    'Content-Type': 'application/json',
                },
                statusCode: 200,
                body: foundMerchandise
            }
        } catch (err) {
            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }
    }
    return getAllMerchandise;
}
module.exports = makeGetAllMerchandise