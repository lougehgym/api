const makeDeleteMerchandise = ({ removeMerchandise }) => {
    const deleteMerchandise = async (httpRequest) => {
        try {
            const id = httpRequest.params.id
            const deletedMerchandise = await removeMerchandise({ id })
            return {
                headers: {
                    'Content-Type': 'application/json',
                },
                statusCode: 200,
                body: deletedMerchandise
            }
        } catch (err) {
            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }
    }
    return deleteMerchandise;
}
module.exports = makeDeleteMerchandise