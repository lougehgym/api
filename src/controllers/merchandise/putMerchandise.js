//@ts-check

const makePutMerchandise = ({ updateMerchandise }) => {
    const putMerchandise = async (httpRequest) => {
        try {
            const { source = {}, ...merchandiseInfo } = httpRequest.body;
            const { id } = httpRequest.params;
            const updatedMerchandise = await updateMerchandise({
                ...merchandiseInfo,
                id
            })
            return {
                headers: {
                    'Content-Type': 'application/json',
                },
                statusCode: 200,
                body: { ...updatedMerchandise }
            }
        } catch (err) {
            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }
    }
    return putMerchandise;
}
module.exports = makePutMerchandise