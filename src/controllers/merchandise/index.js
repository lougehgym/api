//@ts-check

const { addMerchandise, findAllMerchandise, findMerchandise, updateMerchandise, removeMerchandise } = require('../../use-cases/merchandise')

const makePostMerchandise = require('./postMerchandise')
const makeGetAllMerchandise = require('./getAllMerchandise')
const makeGetMerchandise = require('./getMerchandise')
const makeUpdateMerchandise = require('./putMerchandise');
const makeDeleteMerchandise = require('./deleteMerchandise')

const postMerchandise = makePostMerchandise({ addMerchandise })
const getAllMerchandise = makeGetAllMerchandise({ findAllMerchandise })
const getMerchandise = makeGetMerchandise({ findMerchandise })
const putMerchandise = makeUpdateMerchandise({ updateMerchandise })
const deleteMerchandise = makeDeleteMerchandise({ removeMerchandise })


const merchandiseController = Object.freeze({
    postMerchandise,
    getAllMerchandise,
    getMerchandise,
    putMerchandise,
    deleteMerchandise
});

module.exports = merchandiseController;

module.exports = { postMerchandise, getAllMerchandise, getMerchandise, putMerchandise, deleteMerchandise }