
const makePostMerchandise = ({ addMerchandise }) => {
    const postMerchandise = async (httpRequest) => {
        try {
            const { source = {}, ...merchandiseInfo } = httpRequest.body;
            const postedMerchandise = await addMerchandise({
                ...merchandiseInfo
            })
            return {
                headers: {
                    'Content-Type': 'application/json',

                },
                statusCode: 201,
                body: { ...postedMerchandise }
            }
        } catch (err) {
            console.log(err.message)
            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }
    }
    return postMerchandise;
}
module.exports = makePostMerchandise