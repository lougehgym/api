const makeGetTransaction = ({ findTransaction }) => {

    const getTransaction = async (httpRequest) => {
        try {
            
            const { id } = httpRequest.params;

            const foundTransaction = await findTransaction({ id });

            return {
                headers: {
                    'Content-Type': 'application/json',
                },
                statusCode: 200,
                body: foundTransaction
            }
        } catch (err) {
            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }
    }
    return getTransaction;
}

module.exports = makeGetTransaction