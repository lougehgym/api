
const makePutTransaction = ({ updateTransaction }) => {
    const putTransaction = async (httpRequest) => {
        try {
            const { source = {}, ...transaction } = httpRequest.body;
            const { id } = httpRequest.params;
            const updatedTransaction = await updateTransaction({
                ...transaction,
                id
            })
            return {
                headers: {
                    'Content-Type': 'application/json',

                },
                statusCode: 201,
                body: { ...updatedTransaction }
            }
        } catch (err) {
            console.log(err)
            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }
    }
    return putTransaction;
}
module.exports = makePutTransaction