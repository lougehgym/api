
const makePostTransaction = ({ addTransaction }) => {
    const postTransaction = async (httpRequest) => {
        try {
            const { source = {}, ...transaction } = httpRequest.body;
            const postedTransaction = await addTransaction({
                ...transaction
            })
            return {
                headers: {
                    'Content-Type': 'application/json',

                },
                statusCode: 201,
                body: { ...postedTransaction }
            }
        } catch (err) {
            console.log(err.message)
            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }
    }
    return postTransaction;
}
module.exports = makePostTransaction