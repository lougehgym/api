const makeGetAllTransaction = ({ findAllTransaction }) => {

    const getAllTransaction = async (httpRequest) => {
        try {
            const foundAllTransaction = await findAllTransaction();

            return {
                headers: {
                    'Content-Type': 'application/json',
                },
                statusCode: 200,
                body: foundAllTransaction
            }
        } catch (err) {
            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }
    }
    return getAllTransaction;
}

module.exports = makeGetAllTransaction