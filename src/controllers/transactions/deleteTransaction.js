const makeDeleteTransaction = ({ removeTransaction }) => {

    const getTransaction = async (httpRequest) => {
        try {

            const { id } = httpRequest.params;

            const deletedTransaction = await removeTransaction({ id });

            return {
                headers: {
                    'Content-Type': 'application/json',
                },
                statusCode: 200,
                body: deletedTransaction 
            }
        } catch (err) {
            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }
    }
    return getTransaction;
}

module.exports = makeDeleteTransaction