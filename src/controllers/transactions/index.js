//@ts-check
const makePostTransaction = require('./postTransaction');
const makeGetAllTransaction = require('./getAllTransaction')
const makeGetTransaction = require('./getTransaction');
const makePutTransaction = require('./puttransaction');
const makeDeleteTransaction = require('./deleteTransaction')
const { addTransaction, findAllTransaction, findTransaction, updateTransaction, removeTransaction } = require('../../use-cases/transactions');

const postTransaction = makePostTransaction({ addTransaction })
const getAllTransaction = makeGetAllTransaction({ findAllTransaction })
const getTransaction = makeGetTransaction({ findTransaction });
const putTransaction = makePutTransaction({ updateTransaction })
const deleteTransaction = makeDeleteTransaction({ removeTransaction })
const transactionController = Object.freeze({
    postTransaction, getAllTransaction, getTransaction, putTransaction, deleteTransaction
})


module.exports = transactionController;

module.exports = { postTransaction, getAllTransaction, getTransaction, putTransaction, deleteTransaction }