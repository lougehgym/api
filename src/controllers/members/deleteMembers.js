const makeDeleteMember = ({ deleteMember }) => {
    const deletedMember = async (httpRequest) => {
        try {
            const id = httpRequest.params.id
            const foundDeletedMember = await deleteMember({ id })
            return {
                headers: {
                    'Content-Type': 'application/json',
                },
                statusCode: 200,
                body: foundDeletedMember
            }
        } catch (err) {
            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }
    }
    return deletedMember;
}
module.exports = makeDeleteMember