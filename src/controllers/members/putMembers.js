const makePutMember = ({ updateMember }) => {
    const putMember = async (httpRequest) => {
        try {
            const { source = {}, ...memberInfo } = httpRequest.body;
            const { id } = httpRequest.params;

            const updatedMember = await updateMember({
                ...memberInfo,
                id
            })
            return {
                headers: {
                    'Content-Type': 'application/json',
                },
                statusCode: 200,
                body: { ...updatedMember }
            }
        } catch (err) {
            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }
    }
    return putMember;
}
module.exports = makePutMember