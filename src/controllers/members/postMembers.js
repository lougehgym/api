//@ts-check
const makePostMember = ({ addMember }) => {
    const postMember = async (httpRequest) => {
        try {
            const { source = {}, ...memberInfo } = httpRequest.body;
            const postedMember = await addMember({
                ...memberInfo
            })
            return {
                headers: {
                    'Content-Type': 'application/json',

                },
                statusCode: 201,
                body: { ...postedMember }
            }
        } catch (err) {
            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }
    }
    return postMember;
}
module.exports = makePostMember