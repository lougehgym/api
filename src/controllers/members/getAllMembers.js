const makeGetAllMember = ({ findAllMembers }) => {
    const getAllMember = async (httpRequest) => {
        try {
            const foundMembers = await findAllMembers()
            return {
                headers: {
                    'Content-Type': 'application/json',
                },
                statusCode: 200,
                body: foundMembers
            }
        } catch (err) {
            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }
    }
    return getAllMember;
}
module.exports = makeGetAllMember