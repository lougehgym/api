const makeGetMember = ({ findMember }) => {
    const getMember = async (httpRequest) => {
        try {
            const id = httpRequest.params.id
            const foundMembers = await findMember({ id })
            return {
                headers: {
                    'Content-Type': 'application/json',
                },
                statusCode: 200,
                body: foundMembers
            }
        } catch (err) {
            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }
    }
    return getMember;
}
module.exports = makeGetMember