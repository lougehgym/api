//@ts-check

const { addMember, findAllMembers, findMember, updateMember, deleteMember } = require('../../use-cases/members');
const makePostMember = require('.././members/postMembers')
const makeGetAllMember = require('.././members/getAllMembers')
const makeGetMember = require('.././members/getMember')
const makePutMember = require('.././members/putMembers');
const makeDeleteMember = require('.././members/deleteMembers')
const postMember = makePostMember({ addMember })
const getAllMember = makeGetAllMember({ findAllMembers })
const getMember = makeGetMember({ findMember })
const putMember = makePutMember({ updateMember })
const deletedMember = makeDeleteMember({ deleteMember })
const memberController = Object.freeze({
    postMember,
    getAllMember,
    getMember,
    putMember,
    deletedMember
})
module.exports = memberController;
module.exports = { postMember, getAllMember, getMember, putMember, deletedMember }