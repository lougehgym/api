const Pool = require('pg').Pool;
const Client = require('pg').Client;

const pgtools = require('pgtools')

const dbConfig = Object.freeze({
    user: 'postgres',
    host: 'localhost',
    port: 5432,
    password: 'root'
})


/**
 * Create a new Pool based on the Configuration to connect to the database
 * @param {Config=} config 
 */
const pool = (config = { ...dbConfig }) => new Pool(config)
const client = (config = { ...dbConfig }) => new Client(config)
/**
 * ### Customized Query based on the configuration
 * @param {Config=} config - Configuration 
 * @param {string} query 
 * @param {Array=} params 
 */
const query = async (query, params = null, config = {
    ...dbConfig,
    database: 'fitness_db'
}) => {

    const result = await pool(config).query(query, params ? params : null)

    return result;
}
/**
 * Create database
 * @param {Config=} config 
 * @param {string} database
 */
const createDB = async (database = 'fitnes_db_test', config = dbConfig) => {

    // pool(config).query(`CREATE DATABASE ${database}
    // WITH 
    // OWNER = postgres
    // ENCODING = 'UTF8'
    // CONNECTION LIMIT = -1;`)
    return await pgtools.createdb(config, database, (err, res) => {
        if (err) {
            console.log(err)
        }
        console.log(res)
    })
}

/**
 * 
 * @param {Config=} config 
 * @param {string} database
 */
const dropDB = async (database = 'fitness_db_test', config = dbConfig) => {
    await pgtools.dropdb(config, database, (err, res) => {
        if (err) {
            console.log(err)
        }
        console.log(res)
    })
    // pool(config).query(`DROP DATABASE IF EXISTS ${database}`)
}
module.exports = { pool, createDB, dropDB, dbConfig, query, client };
