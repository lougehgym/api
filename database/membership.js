//@ts-check
const db = require('./db')
//@ts-ignore
const db_types = require('./db_types')
const jwt = require('jsonwebtoken')

/**
 * 
 * @param {Membership} membership
 * @return {ThrowException} Will throw an error if it violates any condition given
 * 
 * @example
 * 
 * const validateMembership = validateMembershipInput({name:"Full",price:3600,member_id:4,id:2})
 *
 */
const validateMembershipInput = ({ name, price, member_id, id = 1 }) => {
    if (!id) {
        return { statusCode: 400, data: `Unable to find Membership without an ID` }
    }

    if (isNaN(id)) {
        return { statusCode: 400, data: `ID must be integer` }
    }
    if (!name) {
        return { statusCode: 400, data: `Membership name must be provided` }
    }
    if (!isNaN(name)) {
        return { statusCode: 400, data: `Membership name must not be a number` }
    }
    if (!price) {
        return { statusCode: 400, data: `price must be provided` }
    }
    if (isNaN(price)) {
        return {
            statusCode: 400, data: `Price must be a number`
        }
    }
    if (!member_id) {
        return { statusCode: 400, data: `Member must be provided` }
    }
    if (isNaN(member_id)) {
        return {
            statusCode: 400, data: `Invalid Input for Member`
        }
    }
}
const createTable = async () => {
    try {

        const client = await db.pool().connect();
        await client.query(`CREATE TABLE IF NOT EXISTS public.membership
    (
        id serial NOT NULL,
        name character varying(20) NOT NULL,
        price double precision NOT NULL,
        member_id integer NOT NULL,
        PRIMARY KEY (id),
        FOREIGN KEY (member_id)
            REFERENCES public.members (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION
    )
    WITH (
        OIDS = FALSE
    );`).then(res => {
            db.pool().end()
        })
    } catch (err) {
        db.pool().end()
    }

}


/**
 * 
 * @param {Queries} queries 
 */
const queryMembership = async ({ id, name }) => {
    let sql = 'SELECT * FROM MEMBERSHIP WHERE'
    let params = [];
    if (id) {
        sql += ` ID=$1`
        params = [id]
    }
    if (name && !id) {
        sql += ` name=$1`
        params = [name]
    }
    if (name && id) {
        sql += ` OR name=$2`
        params = [id, name]
    }
    const result = await db.query(sql, params);

    return result;

}

const getAllMembership = async (req, res) => {
    
    try {
        const results = await db.query('SELECT * FROM membership')
        if (!results.rows) {
            throw { statusCode: 400, data: "No membership found" }
        }
        if (results.rows.length < 1) {
            res.status(200).send({ data: "No Membership found" });
        }
        res.status(200).send(results.rows);

    } catch (err) {
        res.status(err.statusCode).send(err.data)
    }
}
const getMembership = async (req, res) => {
    const id = req.params.id;
   
    try {
        if (!id) {

            throw { statusCode: 400, data: "ID must be provided" }
        }
        if (isNaN(id)) {

            throw { statusCode: 400, data: "ID must be Number" }
        }

        const result = await db.query('SELECT * FROM membership WHERE ID=$1', [id])

        if (result.rows.length < 1) {
            res.status(400).send("No Membership found")
        }
        if (!result.rows) {
            throw { statusCode: 400, data: "No Membership found" }
        }
        res.status(200).send(result.rows)
    } catch (err) {
        res.status(err.statusCode).send(err.data)
    }
}

/**
 * POST MEMBER FROM THE DATABASE
 * @function
 * @param {Object} req
 * @param {Object} res
 * 
 */
const createMembership = async (req, res) => {
    
    createTable();
    /**
     * @type {Membership}
     */
    const { name, price, member_id } = req.body;

    /**
     * @type {ThrowException}
     */
    const validateMembership = validateMembershipInput({ name, price, member_id });

    try {
        if (validateMembership) {
            throw validateMembership;
        }
        const hasMembership = await queryMembership({ name });
        if (hasMembership.rows) {
            throw { statusCode: 400, data: `${name} is already exist` }
        }
        const result = await db.query('INSERT INTO membership(name,price,member_id) VALUES($1,$2,$3)', [name, price, member_id])
        if (!result.rowCount) {
            throw { statusCode: 400, data: `Unable to add ${name}` }
        }
        res.status(201).send({ data: `Added Membership ${name} to ${member_id} with a price of ${price}` })
    } catch (err) {

        res.status(err.statusCode).send(err.data)
    }
}
const updateMembership = async (req, res) => {
    
    /**
     * @type {Membership}
     */
    const { name, price, member_id } = req.body;
    /**
     * @type {number}
     */
    const id = req.params.id
    const validateMembership = validateMembershipInput({ name, price, member_id, id })
    try {

        if (validateMembership) {

            throw validateMembership;
        }
        const hasMembership = await queryMembership({ id, name })
        if (hasMembership.rows.length < 1) {
            throw { statusCode: 400, data: `No membership found` }
        }
        const result = await db.query('UPDATE MEMBERSHIP SET name=$1,price=$2,member_id=$3 WHERE id = $4', [name, price, member_id, id])
        if (!result.rowCount) {
            throw { statusCode: 400, data: "Unable to update membership" }
        }
        res.status(200).send(`Updated Membership ${name}`)
    } catch (err) {
        res.status(err.statusCode).send(err.data)
    }
}
const deleteMembership = async (req, res) => {
    
    /**
     * @type {Number}
     */
    const id = req.params.id;

    try {
        if (!id) {
            throw { statusCode: 400, data: `ID must be provided` }
        }
        if (isNaN(id)) {
            throw { statusCode: 400, data: `ID must be Number` }
        }

        const hasMembership = await queryMembership({ id });
        if (!hasMembership.rows.length) {
            throw { statusCode: 400, data: `No Membership found` }
        }
        const result = await db.query('DELETE FROM MEMBERSHIP WHERE id=$1', [id])
        if (!result.rowCount) {
            throw { statusCode: 400, data: "Unable to delete membership" }
        }
        res.status(200).send("Successfully deleted membership")
    } catch (err) {
        res.status(err.statusCode).send(err.data)
    }
}

module.exports = {
    queryMembership,
    getAllMembership,
    getMembership,
    createMembership,
    updateMembership,
    deleteMembership
}