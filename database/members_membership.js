//@ts-check
const db = require('./db');

/**
 * GET ALL MEMBERS FROM THE DATABASE
 * @function
 * @param {Object} req
 * @param {Object} res
 * 
 */
const getAllMembersWithMembership = async (req, res) => {
    const { from, to } = req.body;
    try {
        const result = await db.query(
            'SELECT mem.name,mem.zip,mem.membership_date,memship.name,memship.price from members \
            as mem inner join membership as memship \
            on mem.id = memship.member_id WHERE membership_date between $1 AND $2', [from, to])
        if (!result.rows) {
            throw { statusCode: 400, data: "Error:No members found" }
        }

        if (result.rows.length < 1) {
            res.status(200).send("No Members with membership found")
        }
        res.status(200).send(result.rows);

    } catch (err) {
        res.status(err.statusCode).send(err.data)
    }
}
module.exports = { getAllMembersWithMembership }