//@ts-check
const db = require('./db')
const jwt = require('jsonwebtoken')
/**
 * @typedef {Object} TransactionQueries
 * @prop {Number=} id
 * 
 */


/**
 * @typedef {Object} Transaction
 * @prop {Number=} id
 * @prop {string} date
 * @prop {Number} member_id
 * 
 */

/**
 * 
 * @typedef {Object} ThrowException
 * @prop {Number} statusCode
 * @prop {String} data 
 */

/**
 * 
 * @param {Transaction} transaction
 * @return {ThrowException}
 */
const validateTransactionInput = ({ id = 1, date, member_id }) => {
    if (!id) {
        return { statusCode: 400, data: `Unable to find Transaction without an ID` }
    }

    if (isNaN(id)) {
        return { statusCode: 400, data: `ID must be integer` }
    }
    if (!date) {
        return { statusCode: 400, data: `Date must be provided` }
    }

    if (!date.match(/^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/)) {
        return { statusCode: 400, data: `Date must be in YYYY-MM-DD` }
    }
    if (isNaN(member_id)) {
        return { statusCode: 400, data: `Member must be a Number` }
    }
    if (!member_id) {
        return {
            statusCode: 400, data: `Member must be provided`
        }
    }
    return null;

}

const createTable = async () => {
    try {

        const client = await db.pool().connect();
        await client.query(`CREATE TABLE IF NOT EXISTS public.transaction
                            (
                            id serial NOT NULL,
                            date date NOT NULL,
                            member_id integer NOT NULL,
                            merchandise_id integer NOT NULL,
                            PRIMARY KEY (id)
                            FOREIGN KEY (member_id)
                            REFERENCES public.members (id) MATCH SIMPLE
                            ON UPDATE NO ACTION
                            ON DELETE NO ACTION        
                            FOREIGN KEY (merchandise_id)
                            REFERENCES public.merchandise(id) MATCH SIMPLE
                            ON UPDATE NO ACTION
                            ON DELETE NO ACTION
                            )
                            WITH (
                              OIDS = FALSE
                                 );
                            `).then(res => {

            db.pool().end()
        })
    } catch (err) {
        db.pool().end()
    }

}

/**
 * 
 * @param {TransactionQueries} queries 
 */
const queryTransaction = async ({ id }) => {
    let sql = 'SELECT * FROM transaction WHERE'
    let params = [];
    if (id) {
        sql += ` ID=$1`
        params = [id]
    }
    const result = await db.query(sql, params);

    return result;

}
const getAllTransactions = async (req, res) => {

    try {
        const result = await db.query('SELECT * FROM transaction')
        if (!result.rows) {
            throw { statusCode: 400, data: "No Transactions found" }
        }
        if (result.rows.length < 1) {
            throw { statusCode: 400, data: "No Transactions found" }
        }
        res.status(200).send(result.rows);

    } catch (err) {
        res.status(err.statusCode).send(err.data)
    }
}
const getTransaction = async (req, res) => {

    const id = req.params.id;
    try {
        if (!id) {
            throw {
                statusCode: 400, data: `ID must be provided`
            }
        }
        if (isNaN(id)) {
            throw {
                statusCode: 400, data: `ID must be Number`
            }
        }
        const result = await db.query('SELECT * FROM transaction WHERE ID=$1', [id])

        if (!result.rows) {
            throw { statusCode: 400, data: "No Transaction found" }
        }
        if (result.rows.length < 1) {
            throw { statusCode: 400, data: "No Transaction found" }
        }
        res.status(200).send(result.rows)
    } catch (err) {
        res.status(err.statusCode).send(err.data)
    }
}

/**
 * POST MEMBER FROM THE DATABASE
 * @function
 * @param {Object} req
 * @param {Object} res
 * 
 */
const createTransaction = async (req, res) => {

    createTable();
    /**@type {Transaction} */
    const { date, member_id } = req.body;
    try {
        /**
         * @type {ThrowException}
         */
        const validateTransaction = validateTransactionInput({ date, member_id })

        if (validateTransaction) {
            throw validateTransaction
        }
        
        const result = await db.query('INSERT INTO transaction(date,member_id) VALUES($1,$2)', [date, member_id,])
        if (!result.rowCount) {
            throw { statusCode: 400, data: `Unable to add transaction` }
        }
        res.status(201).send({ data: `Added transaction for ${member_id} ` })
    } catch (err) {
        res.status(err.statusCode).send(err.data)
    }
}
const updateTransaction = async (req, res) => {

    const { date, member_id } = req.body;
    const id = req.params.id;
    try {
        const validateTransaction = validateTransactionInput({ id, date, member_id })
        if (validateTransaction) {
            throw validateTransaction
        }
        const hasTransaction = await queryTransaction({ id })
        if (!hasTransaction) {
            throw { statusCode: 400, data: "Transaction not found" }
        }
        const result = await db.query('UPDATE transaction SET date=$1,member_id=$2 WHERE id = $3', [date, member_id, id])
        if (!result.rowCount) {
            throw { statusCode: 400, data: "Unable to update transaction" }
        }
        res.status(200).send(`Updated transaction for ${member_id}`)
    } catch (err) {
        res.status(err.statusCode).send(err.data)
    }
}
const deleteTransaction = async (req, res) => {

    const { id } = req.params;
    try {
        if (!id) {
            throw {
                statusCode: 400, data: `ID must be provided`
            }
        }
        if (isNaN(id)) {
            throw {
                statusCode: 400, data: `ID must be Number`
            }
        }
        const hasTransaction = await queryTransaction({ id })
        if (!hasTransaction.rows.length) {
            throw {
                statusCode: 400, data: `No Transaction Found`
            }
        }
        const result = await db.query('DELETE FROM transaction WHERE id=$1', [id])
        if (!result.rows) {
            throw { statusCode: 400, data: "Unable to delete transaction" }
        }
        res.status(200).send("Successfully Deleted transaction")
    } catch (err) {
        res.status(err.statusCode).send(err.data)
    }
}

module.exports = { getAllTransactions, getTransaction, createTransaction, updateTransaction, deleteTransaction }