//@ts-check
const db = require('./db');
const jwt = require('jsonwebtoken')

const createTable = async (tableName = 'members', config = db.dbConfig) => {
    try {
        const client = await db.pool(config).connect();
        await client
            .query(`CREATE TABLE IF NOT EXISTS ${tableName}(id serial NOT NULL, 
        name character varying(50) NOT NULL, 
        zip integer,membership_date date,PRIMARY KEY (id))
        WITH (OIDS = FALSE);`).then(res => {
                console.log('created table members')
                db.pool().end();

            })
    } catch (err) {
        db.pool().end();
    }
}


/**
 * 
 * @param {Member} member
 * @return {ThrowException} Will throw an error if it violates any condition given
 * 
 *
 */
const validateMemberInput = ({ name, zip = 1, membership_date = new Date().toDateString(), id = 1 }) => {
    if (!id) {
        return { statusCode: 400, data: `Unable to find Membership without an ID` }
    }

    if (isNaN(id)) {
        return { statusCode: 400, data: `ID must be integer` }
    }
    if (!name) {
        return { statusCode: 400, data: `Name must be provided` }
    }
    if (isNaN(zip)) {
        return {
            statusCode: 400, data: `Zip must be a number`
        }
    }
    if (!membership_date.match(/^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/)) {
        return { statusCode: 400, data: `Membership Date must be in a proper format` }
    }
}


/**
 * 
 * @param {Queries} member 
 */
const queryMember = async ({ id, name }) => {
    let sql = `SELECT * FROM MEMBERS WHERE `;
    let params = [];
    if (id) {
        sql += ` ID=$1`
        params = [id]
    }
    if (name && !id) {
        sql += `name=$1`
        params = [name]
    }
    if (name && id) {
        sql += `OR name=$2`
        params = [id, name]
    }
    const result = await db.query(sql, params)
    return result;
}
/**
 * GET ALL MEMBERS FROM THE DATABASE
 * @function
 * @param {Object} req
 * @param {Object} res
 * 
 */

const getAllMembers = async (req, res) => {

    try {

        const result = await db.query("SELECT * FROM MEMBERS");
        if (!result.rows) {
            throw { statusCode: 400, data: "No members found" }
        }

        if (result.rows.length < 1) {
            throw { statusCode: 400, data: "No members found" }
        }
        res.status(200).send(result.rows);


    } catch (err) {
        res.status(err.statusCode).send(err.data)
    }
}


/**
 * GET MEMBERS FROM THE DATABASE
 * @function
 * @param {Object} req
 * @param {Object} res
 * 
 */
const getMember = async (req, res) => {
    /**
     * @type {Member}
     */
    const { id } = req.params;

    try {
        if (!id) {
            throw { statusCode: 400, data: "ID must be provided" }
        }
        if (isNaN(id)) {
            throw {
                statusCode: 400, data: "ID must be a number"
            }
        }
        const result = await queryMember({ id });

        if (!result.rows) {
            throw { statusCode: 400, data: "No member found" }
        }
        if (result.rows.length < 1) {
            throw { statusCode: 400, data: "No member found" }
        }
        res.status(200).send(result.rows)
    } catch (err) {
        res.status(err.statusCode).send(err.data)
    }
}

/**
 * POST MEMBER FROM THE DATABASE
 * @function
 * @param {Object} req
 * @param {Object} res
 * 
 */
const createMember = async (req, res) => {
    /** @type {Member}*/
    const { name, zip, membership_date } = req.body;
    try {

        const validateMember = validateMemberInput({ name, zip, membership_date })
        if (validateMember) {
            throw validateMember
        }
        const result = await insertQuery({ name, zip, membership_date })

        if (!result.rowCount) {
            throw { statusCode: 400, data: `Unable to add ${name}` }
        }
        res.status(201).send({ data: `Added ${name} with zipcode ${zip}` })
    } catch (err) {
        res.status(err.statusCode).send(err.data)
    }
}

const insertQuery = async ({ name, zip, membership_date, config = { ...db.dbConfig, database: 'fitness_db' } }) => {
    return await db.query(
        'INSERT INTO MEMBERS(name,zip,membership_date) VALUES($1,$2,$3)',
        [name, zip, membership_date], config);
}

/**
 * PUT MEMBER FROM THE DATABASE
 * @function
 * @param {Object} req
 * @param {Object} res
 * 
 */
const updateMember = async (req, res) => {
    /**@type {Member}*/
    const { name, zip, membership_date } = req.body;
    /** @type {Member}*/
    const { id } = req.params
    try {
        const validateMember = validateMemberInput({ name, zip, membership_date, id })
        if (validateMember) {
            throw validateMember
        }
        const hasMember = await queryMember({ id });

        if (hasMember.rows.length < 1) {
            throw { statusCode: 400, data: "No Member found" }
        }
        if (!hasMember.rows) {
            throw { statusCode: 400, data: "No member found" }
        }

        const result = await db.query('UPDATE MEMBERS SET name=$1,zip=$2,membership_date=$3 WHERE id = $4', [name, zip, membership_date, id])

        if (!result.rowCount) {
            throw { statusCode: 400, data: "Unable to update member" }
        }
        res.status(200).send(`Updated Member ${name}`)
    } catch (err) {
        res.status(err.statusCode).send(err.data)

    }
}

/**
 * DELETE MEMBER FROM THE DATABASE
 * @param {Object} req
 * @param {Object} res
 * 
 */
const deleteMember = async (req, res) => {

    /** @type {Member}*/
    const { id } = req.params;
    try {
        if (!id) {
            throw { statusCode: 400, data: `ID must be provided` }
        }
        if (isNaN(id)) {
            throw { statusCode: 400, data: `ID must be a number` }
        }
        const hasMember = await queryMember({ id })
        if (!hasMember.rows) {
            throw { statusCode: 400, data: "No Member found" }
        }
        if (hasMember.rows.length < 1) {
            throw { statusCode: 400, data: "No Member found" }
        }
        const result = await db.query('DELETE FROM MEMBERS WHERE id=$1', [id])
        if (!result.rowCount) {
            throw { statusCode: 400, data: "Unable to delete member" }
        }
        res.status(200).send("Successfully Deleted Member")
    } catch (err) {
        res.status(err.statusCode).send(err.data)
    }
}

module.exports = {
    getAllMembers,
    getMember,
    createMember,
    updateMember,
    deleteMember,
    createTable,
    queryMember,
    insertQuery,

}