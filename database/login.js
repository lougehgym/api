//@ts-check

const db = require('./db')
const jwt = require('jsonwebtoken')

/**
 * @typedef {Object} User
 * @prop {string=} first_name
 * @prop {number} id
 * @prop {string=} middle_name 
 * @prop {string=} last_name
 * @prop {string} password
 */

/**
 * 
 * @param {User} user
 */
const validateUser = ({ id, first_name, middle_name, last_name, password }) => {
    if (!id) {
        return { statusCode: 400, data: `ID must be provided` }
    }
    if (isNaN(id)) {
        return { statusCode: 400, data: `ID must be a number` }
    }
    if (!first_name) {
        return { statusCode: 400, data: `First name must be provided` }
    }
    if ((parseInt(first_name))) {
        return { statusCode: 400, data: `First name must not contain numbers` }
    }

    if (!last_name) {
        return { statusCode: 400, data: `Last name must be provided` }
    }
    if (parseInt(last_name)) {
        return { statusCode: 400, data: `Last name must not contain numbers` }
    }
    if (parseInt(middle_name)) {
        return { statusCode: 400, data: `Middle name must not contain numbers` }
    }
    if (!password) {
        return { statusCode: 400, data: `Password must have a value` }
    }
}
const queryUser = async ({ id }) => {
    const sql = `SELECT * FROM users WHERE ID = $1`;
    const params = [id]
    const result = await db.query(sql, params)
    return result;

}
const loginUser = async (req, res) => {
    const { id, password } = req.body;

    try {
        if (!id) {
            throw { statusCode: 400, data: `Id must be provided` }
        }
        if (isNaN(id)) {
            throw { statusCode: 400, data: `Id must be a number` }
        }
        if (!password) {
            throw { statusCode: 400, data: `Password must be provided` }
        }

        const hasUser = await queryUser({ id });
        if (!hasUser.rows.length) {
            throw { statusCode: 400, data: `User does not exist` }
        }
        jwt.sign({ id }, 'secretkey', async (err, token) => {

            const result = await db.query("UPDATE users SET token=$1 WHERE id=$2", [token, id])
            if (!result.rowCount) {
                throw { statusCode: 400, data: `Unable to update token` }
            }
            res.json({ token })
        });
    } catch (err) {
        res.status(err.statusCode).send(err.data)
    }
}
const createUser = async (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        }
    })
    /** @type {User} */
    const { id, first_name, middle_name, last_name, password } = req.body;
    try {
        const validateUserInput = validateUser({ id, first_name, middle_name, last_name, password })
        if (validateUserInput) {
            throw validateUserInput
        }
        const hasUser = await queryUser({ id });
        if (hasUser.rows.length) {
            throw { statusCode: 400, data: `User already exists` }
        }

        let sql = "INSERT INTO users(id,first_name,middle_name,last_name,password) VALUES($1,$2,$3,$4,$5)"
        let params = [id, first_name, middle_name, last_name, password]
        if (!middle_name) {
            sql = "INSERT INTO users(id,first_name,last_name,password) VALUES($1,$2,$3,$4);"
            params = [id, first_name, last_name, password]
        }
        console.log(sql, params)
        const result = await db.query(sql, params);
        if (!result.rowCount) {
            throw { statusCode: 400, data: `Unable to create user` }
        }
        res.status(201).send(`User Created`);

    } catch (err) {
        console.log(err)
        res.status(err.statusCode).send(err.data)
    }
}

module.exports = { loginUser, createUser };