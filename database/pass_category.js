//@ts-check
const db = require('./db')
const jwt = require('jsonwebtoken')
const createTable = async () => {
    try {

        const client = await db.pool().connect();
        await client.query(`CREATE IF NOT EXISTS TABLE public.pass_category
    (
    id serial NOT NULL,
    name character varying(100) NOT NULL,
    price double precision NOT NULL,
    PRIMARY KEY (id)
    )
    WITH (
    OIDS = FALSE
    );
`).then(res => {
            console.log("Table pass_category created")
            db.pool().end()
        })
    } catch (err) {
        db.pool().end()
    }
}
const validatePassCategoryInput = ({ category_name, price, id = 1 }) => {
    if (!id) {
        return { statusCode: 400, data: `Unable to find Membership without an ID` }
    }

    if (isNaN(id)) {
        return { statusCode: 400, data: `ID must be integer` }
    }
    if (!category_name) {
        return { statusCode: 400, data: `Name must be provided` }
    }
    if (!isNaN(category_name)) {
        return { statusCode: 400, data: `Name must not be a number` }
    }
    if (isNaN(price)) {
        return {
            statusCode: 400, data: `Price must be a number`
        }
    }
    if (!price) {
        return {
            statusCode: 400, data: `Price must have a value`
        }
    }

}
const queryPassCategory = async ({ id = 1, category_name = "a" }) => {
    let sql = `SELECT * FROM pass_category WHERE`;
    let params = [];
    if (id) {
        sql += ` ID=$1`
        params = [id]
    }
    if (category_name && !id) {
        sql += ` category_name=$1`
        params = [category_name]
    }
    if (category_name && id) {
        sql += `OR category_name=$2`
        params = [id, category_name]
    }
    const result = await db.query(sql, params)
    return result;
}

const getAllPassCategory = async (req, res) => {
    
    try {
        const result = await db.query('SELECT * FROM pass_category')
        if (!result.rows) {
            throw { statusCode: 400, data: "No Pass Category  found" }
        }
        if (result.rows.length < 1) {
            throw { statusCode: 400, data: "No Pass Category found" }
        }
        res.status(200).send(result.rows);

    } catch (err) {
        res.status(err.statusCode).send(err.data)
    }
}
const getPassCategory = async (req, res) => {
    
    
    const id = req.params.id;
    try {
        if (isNaN(id)) {
            throw { statusCode: 400, data: "ID must be a number" }
        }

        const result = await db.query('SELECT * FROM pass_category WHERE ID=$1', [id])

        if (!result.rows) {
            throw { statusCode: 400, data: "No Pass Category  found" }
        }
        if (result.rows.length < 1) {
            throw { statusCode: 400, data: "No Pass Category found" }
        }
        res.status(200).send(result.rows)
    } catch (err) {
        res.status(err.statusCode).send(err.data)
    }
}

/**
 * POST MEMBER FROM THE DATABASE
 * @function
 * @param {Object} req
 * @param {Object} res
 * 
 */
const createPassCategory = async (req, res) => {
    
    createTable();
    const { category_name, price } = req.body;
    try {
        const validatePassCategory = await validatePassCategoryInput({ category_name, price })
        if (validatePassCategory) {
            throw validatePassCategory
        }
        const hasCategory = await queryPassCategory({ category_name })
        const result = await db.query('INSERT INTO pass_category(category_name,price) VALUES($1,$2)', [category_name, price])
        if (!result.rowCount) {
            throw { statusCode: 400, data: `Unable to add Pass Category` }
        }
        res.status(201).send({ data: `Added Pass Category for ${category_name} ` })
    } catch (err) {
        res.status(err.statusCode).send(err.data)
    }
}
const updatePassCategory = async (req, res) => {
    
    const { category_name, price } = req.body;
    const id = req.params.id;

    try {

        const validatePassCategory = await validatePassCategoryInput({ category_name, price, id })
        if (validatePassCategory) {
            throw validatePassCategory
        }

        const hasCategory = await queryPassCategory({ id, category_name })
        if (hasCategory.rows.length < 1) {
            throw {
                statusCode: 400, data: `Pass Category not found`
            }
        }
        const result = await db.query('UPDATE pass_category SET category_name=$1,price=$2 WHERE id = $3',
            [category_name, price, id])
        if (!result.rowCount) {
            throw { statusCode: 400, data: "Unable to update Pass Category" }
        }
        res.status(200).send(`Updated Pass Category for ${category_name}`)
    } catch (err) {
        res.status(err.statusCode).send(err.data)
    }
}
const deletePassCategory = async (req, res) => {
    
    const id = req.params.id;
    try {
        if (isNaN(id)) {
            throw { statusCode: 400, data: `Id must be a number` }
        }
        const hasCategory = await queryPassCategory({ id })
        if (hasCategory.rows.length < 1) {
            throw {
                statusCode: 400, data: `No Pass Category found`
            }
        }
        const result = await db.query('DELETE FROM pass_category WHERE id=$1', [id])
        if (!result.rows) {
            throw { statusCode: 400, data: "Unable to deletePass Category" }
        }
        res.status(200).send("Successfully Deleted Pass Category")
    } catch (err) {
        res.status(err.statusCode).send(err.data)
    }
}

module.exports = { queryPassCategory, getAllPassCategory, getPassCategory, createPassCategory, updatePassCategory, deletePassCategory }