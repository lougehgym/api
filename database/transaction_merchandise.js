const db = require('./db');

const getAllMerchTransaction = async (req, res) => {
    try {
        const result = await db.query(
            'SELECT merch.name,merch.price,st.quantity_items,trans.date,trans.member_id,\
         (merch.price * st.quantity_items) as total_price from sales_transaction as st \
         inner join public.transaction as trans on st.transaction_id = trans.id \
         inner join merchandise as merch on st.merchandise_id = merch.id;')

        if (!result.rows) {
            throw { statusCode: 400, data: "No Transactions found" }
        }
        res.status(200).send(result.rows)
    } catch (err) {
        res.status(err.statusCode).send(err.data)
    }
}

module.exports = { getAllMerchTransaction }