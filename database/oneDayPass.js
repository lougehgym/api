//@ts-check
const db = require('./db')
const jwt = require('jsonwebtoken')
const createTable = async () => {
    try {

        const client = await db.pool().connect();
        await client.query(`CREATE TABLE IF NOT EXISTS public.one_day_pass
    (
        id serial NOT NULL,
        date date NOT NULL,
        member_id integer,
        passcat_id integer,
        PRIMARY KEY (id),
        FOREIGN KEY (member_id)
            REFERENCES public.members (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION,
        FOREIGN KEY (passcat_id)
            REFERENCES public.pass_category (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION
    )
    WITH (
        OIDS = FALSE
    );
`).then(res => {

            db.pool().end()
        })
    } catch (err) {
        db.pool().end()
    }

}

const validateDayPassInput = async ({ id = 1, member_id = 1, pass_date = "2019-09-23", passcat_id = 1 }) => {
    if (!id) {
        throw { statusCode: 400, data: `ID must be provided` }
    }
    if (isNaN(id)) {
        throw { statusCode: 400, data: `ID must be a number` }
    }
    if (!member_id) {
        throw { statusCode: 400, data: `Member ID must be provided` }
    }
    if (isNaN(member_id)) {
        throw { statusCode: 400, data: `Member ID must be a number` }
    }
    if (isNaN(passcat_id)) {
        throw {
            statusCode: 400, data: `Pass Category must be a number`
        }
    }
    if (!pass_date.match(/^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/)) {
        return { statusCode: 400, data: `Date must be in YYYY-MM-DD` }
    }

}

const queryDayPass = async ({ id }) => {

    let sql = 'SELECT * FROM one_day_pass WHERE'
    let params = [];
    if (id) {
        sql += ` ID=$1`
        params = [id]
    }
    const result = await db.query(sql, params);

    return result;

}
const getAllPass = async (req, res) => {
    
    try {
        const results = await db.query('SELECT * FROM one_day_pass')
        if (!results.rows) {
            throw { statusCode: 400, data: "No Pass found" }
        }
        if (results.rows.length < 1) {
            res.status(200).send({ data: "No Pass found" })
        }
        res.status(200).send(results.rows);

    } catch (err) {
        res.status(err.statusCode).send(err.data)
    }
}
const getPass = async (req, res) => {
    
    const id = req.params.id;
    try {
        if (isNaN(id)) {
            throw {
                statusCode: 400, data: `ID must be a number`
            }
        }
        const result = await db.query('SELECT * FROM one_day_pass WHERE ID=$1', [id])

        if (!result.rows) {
            throw { statusCode: 400, data: "No pass found" }
        }
        if (result.rows.length < 1) {
            res.status(400).send({ data: "No Pass found" })
        }
        res.status(200).send(result.rows)
    } catch (err) {
        res.status(err.statusCode).send(err.data)
    }
}

/**
 * POST MEMBER FROM THE DATABASE
 * @function
 * @param {Object} req
 * @param {Object} res
 * 
 */
const createPass = async (req, res) => {
    
    createTable();
    /**
     * @type {string}
     */
    const sql = 'INSERT INTO one_day_pass(member_id,pass_date,passcat_id) VALUES($1,$2,$3)';
    const { member_id, pass_date, passcat_id } = req.body;
    try {
        const validateDayPass = await validateDayPassInput({ member_id, pass_date, passcat_id })
        if (validateDayPass) {
            throw validateDayPass
        }
        const result = await db.query(sql, [member_id, pass_date, passcat_id])
        if (!result.rowCount) {
            throw { statusCode: 400, data: `Unable to add ${member_id}` }
        }
        res.status(201).send({ data: `Added Member ID:${member_id} with date ${pass_date}` })
    } catch (err) {
        res.status(err.statusCode).send(err.data)
    }
}
const updatePass = async (req, res) => {
    
    const { member_id, pass_date, passcat_id } = req.body;
    const id = req.params.id;
    try {
        const validateDayPass = await validateDayPassInput({ member_id, pass_date, passcat_id, id })
        if (validateDayPass) {
            throw validateDayPass
        }
        const hasDayPass = await queryDayPass({ id })
        if (!hasDayPass.rows) {
            throw { statusCode: 400, data: `Day Pass not found` }
        }
        const result = await db.query('UPDATE one_day_pass SET member_id=$1,pass_date=$2,passcat_id=$3 WHERE id = $4', [member_id, pass_date, passcat_id, id])
        if (!result.rowCount) {
            throw { statusCode: 400, data: "Unable to update pass" }
        }
        res.status(200).send(`Updated pass ${member_id}`)
    } catch (err) {
        res.status(err.statusCode).send(err.data)
    }
}
const deletePass = async (req, res) => {
    
    const id = req.params.id;
    try {
        if (isNaN(id)) {
            throw { statusCode: 400, data: `ID must be a Number` }
        }

        const hasDayPass = await queryDayPass({ id });
        if (!hasDayPass.rows) {
            throw {
                statusCode: 400, data: `No Day Pass found`
            }
        }
        const result = await db.query('DELETE FROM one_day_pass WHERE id=$1', [id])
        if (!result.rows) {
            throw { statusCode: 400, data: "Unable to delete day pass" }
        }
        if (result.rows.length < 1) {
            throw { statusCode: 400, data: `Unable to delete day pass` }
        }
        res.status(200).send("Successfully Deleted day pass")
    } catch (err) {
        res.status(err.statusCode).send(err.data)
    }
}

module.exports = { getAllPass, getPass, createPass, updatePass, deletePass, queryDayPass }