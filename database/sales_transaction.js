//@ts-check
const db = require('./db')
const jwt = require('jsonwebtoken')
const createTable = async () => {
    try {

        const client = await db.pool().connect();
        await client.query(`CREATE TABLE IF NOT EXISTS public.sales_transaction
                            (
                            id serial NOT NULL,
                            transaction_id integer NOT NULL,
                            merchandise_id integer NOT NULL,
                            quantity_items integer,
                            PRIMARY KEY (id)
                            FOREIGN KEY (transaction_id)
                            REFERENCES public.transaction (id) MATCH SIMPLE
                            ON UPDATE NO ACTION
                            ON DELETE NO ACTION        
                            FOREIGN KEY (merchandise_id)
                            REFERENCES public.merchandise(id) MATCH SIMPLE
                            ON UPDATE NO ACTION
                            ON DELETE NO ACTION
                            )
                            WITH (
                              OIDS = FALSE
                                 );
                            `).then(res => {

            db.pool().end()
        })
    } catch (err) {
        db.pool().end()
    }

}

const querySalesTransaction = async ({ id }) => {
    let sql = 'SELECT * FROM sales_transaction WHERE'
    let params = [];
    if (id) {
        sql += ` ID=$1`
        params = [id]
    }
    const result = await db.query(sql, params);

    return result;

}
const validateSalesTransactionInput = async ({ id = 1, transaction_id = 1, merchandise_id = 1, quantity_items = 1 }) => {
    if (!id) {
        return { statusCode: 400, data: `ID must be provided` }
    }
    if (isNaN(id)) {
        return { statusCode: 400, data: `ID must be a number` }
    }
    if (!transaction_id) {
        return { statusCode: 400, data: `Transaction ID must be provided` }
    }

    if (isNaN(transaction_id)) {
        return { statusCode: 400, data: `Transaction ID must be a number` }
    }
    if (!quantity_items) {
        return { statusCode: 400, data: `Quantity Items must be provided` }
    }
    if (isNaN(quantity_items)) {
        return { statusCode: 400, data: `ID must be a number` }
    }
    if (isNaN(merchandise_id)) {
        return { statusCode: 400, data: `Merchandise ID must be number` }
    }
    if (!merchandise_id) {
        return { statusCode: 400, data: `Merchandise ID must be provided` }
    }

}


const getAllSalesTransaction = async (req, res) => {
    
    try {
        const result = await db.query('SELECT * FROM sales_transaction')
        if (!result.rows) {
            throw { statusCode: 400, data: "No Sales Transactions found" }
        }
        if (result.rows.length < 1) {
            throw { statusCode: 400, data: "No Sales Transactions found" }
        }
        res.status(200).send(result.rows);

    } catch (err) {
        res.status(err.statusCode).send(err.data)
    }
}
const getSalesTransaction = async (req, res) => {
    
    const id = req.params.id;
    try {
        if (isNaN(id)) {
            throw { statusCode: 400, data: `ID must be a number` }
        }
        const result = await db.query('SELECT * FROM sales_transaction WHERE ID=$1', [id])

        if (!result.rows) {
            throw { statusCode: 400, data: "No Sales Transactions found" }
        }
        if (result.rows.length < 1) {
            throw { statusCode: 400, data: "No Sales Transactions found" }
        }
        res.status(200).send(result.rows)
    } catch (err) {
        res.status(err.statusCode).send(err.data)
    }
}

/**
 * POST MEMBER FROM THE DATABASE
 * @function
 * @param {Object} req
 * @param {Object} res
 * 
 */
const createSalesTransaction = async (req, res) => {
    
    createTable();
    const { transaction_id, merchandise_id, quantity_items } = req.body;
    try {
        const validateSalesTransaction = await validateSalesTransactionInput({ transaction_id, merchandise_id, quantity_items });
        if (validateSalesTransaction) {
            throw validateSalesTransaction;
        }

        const result = await db.query(`INSERT INTO
        sales_transaction(transaction_id,merchandise_id,quantity_items)
        VALUES($1,$2,$3)`,
            [transaction_id, merchandise_id, quantity_items])
        if (!result.rowCount) {
            throw { statusCode: 400, data: `Unable to add transaction` }
        }
        res.status(201).send({ data: `Added transaction for ${merchandise_id} ` })
    } catch (err) {
        res.status(err.statusCode).send(err.data)
    }
}
const updateSalesTransaction = async (req, res) => {
    
    const { transaction_id, merchandise_id, quantity_items } = req.body;
    const id = req.params.id;
    try {
        const validateSalesTransaction = await validateSalesTransactionInput({ id, transaction_id, merchandise_id, quantity_items })
        if (validateSalesTransaction) {
            throw validateSalesTransaction
        }
        const hasSalesTransaction = await querySalesTransaction({ id })
        if (!hasSalesTransaction) {
            throw { statusCode: 400, data: `No Sales transaction found` }
        }
        const result = await db.query('UPDATE sales_transaction SET transaction_id=$1,merchandise_id=$2,quantity_items=$3 WHERE id = $4', [transaction_id, merchandise_id, quantity_items, id])
        if (!result.rowCount) {
            throw { statusCode: 400, data: "Unable to update Sales Transactions" }
        }
        res.status(200).send(`Updated Sales Transactions for ${merchandise_id}`)
    } catch (err) {
        res.status(err.statusCode).send(err.data)
    }
}
const deleteSalesTransaction = async (req, res) => {
    
    const id = req.params.id;
    try {
        if (!id) {
            throw { statusCode: 400, data: `Id must be provided` }
        }
        if (isNaN(id)) {
            throw { statusCode: 400, data: `Id must be a number` }
        }

        const hasSalesTransaction = await querySalesTransaction({ id })
        if (hasSalesTransaction.rows.length < 1) {
            throw { statusCode: 400, data: `No Sales transaction found` }
        }

        const result = await db.query('DELETE FROM sales_transaction WHERE id=$1', [id])
        if (!result.rows) {
            throw { statusCode: 400, data: "Unable to delete Sales Transactions" }
        }
        res.status(200).send("Successfully Deleted Sales Transactions")
    } catch (err) {
        res.status(err.statusCode).send(err.data)
    }
}

module.exports = { querySalesTransaction,getAllSalesTransaction, getSalesTransaction, createSalesTransaction, updateSalesTransaction, deleteSalesTransaction }