//@ts-check
const db = require('./db')
//@ts-ignore
const db_types = require('./db_types')
const jwt = require('jsonwebtoken')
/**
 * @param {Merchandise} merchandise
 * @returns {ThrowException}
 */
const validateMerchandiseInput = ({ id = 1, name, price }) => {
    if (!id) {
        return {
            statusCode: 400, data: `ID must be provided`
        }
    }
    if (isNaN(id)) {
        return {
            statusCode: 400, data: `ID must be Number`
        }
    }
    if (!name) {
        return {
            statusCode: 400, data: `Name must be provided`
        }
    }
    if (name.length > 254) {
        return {
            statusCode: 400, data: `${name} is too long(${name.length})`
        }
    }
    if (!isNaN(name)) {
        return {
            statusCode: 400, data: `Name must not be a number`
        }
    }
    if (!price) {
        return {
            statusCode: 400, data: `Price must have a value`
        }
    }
    if (isNaN(price)) {
        return {
            statusCode: 400, data: `Price must be a Number`
        }
    }

}


/**
 * 
 * @param {Queries} queries 
 */
const queryMerchandise = async ({ id, name }) => {
    let sql = 'SELECT * FROM merchandise WHERE'
    let params = [];
    if (id) {
        sql += ` ID=$1`
        params = [id]
    }
    if (name && !id) {
        sql += ` name=$1`
        params = [name]
    }
    if (name && id) {
        sql += ` OR name=$2`
        params = [id, name]
    }
    const result = await db.query(sql, params);

    return result;

}


const createTable = async () => {
    try {

        const client = await db.pool().connect();
        await client.query(`CREATE TABLE IF NOT EXISTS public.merchandise
                            (
                            id serial NOT NULL,
                            name character varying(100) NOT NULL,
                            price double precision NOT NULL,
                            PRIMARY KEY (id)
                            )
                            WITH (
                              OIDS = FALSE
                                 );
                            `).then(res => {

            db.pool().end()
        })
    } catch (err) {
        db.pool().end()
    }

}
const getAllMerchandise = async (req, res) => {
    
    try {
        const result = await db.query('SELECT * FROM merchandise')
        if (!result.rows) {
            throw { statusCode: 400, data: "No Merchandise found" }
        }
        if (result.rows.length < 1) {
            throw { statusCode: 400, data: "No Merchandise found" }
        }
        res.status(200).send(result.rows);

    } catch (err) {
        res.status(err.statusCode).send(err.data)
    }
}
const getMerchandise = async (req, res) => {
    const id = req.params.id;
    
    try {
        if (isNaN(id)) {
            throw { statusCode: 400, data: `ID must be a number` }
        }
        const result = await db.query('SELECT * FROM merchandise WHERE ID=$1', [id])

        if (!result.rows) {
            throw { statusCode: 400, data: "No Merchandise found" }
        }
        if (result.rows.length < 1) {
            throw { statusCode: 400, data: "No Merchandise found" }
        }
        res.status(200).send(result.rows)
    } catch (err) {
        res.status(err.statusCode).send(err.data)
    }
}

/**
 * POST MEMBER FROM THE DATABASE
 * @function
 * @param {Object} req
 * @param {Object} res
 * 
 */
const createMerchandise = async (req, res) => {
    
    createTable();

    const { name, price } = req.body;
    try {
        const validateMerchandise = validateMerchandiseInput({ name, price })
        if (validateMerchandise) {
            throw validateMerchandise
        }

        const result = await db.query('INSERT INTO merchandise(name,price) VALUES($1,$2)', [name, price])
        if (!result.rowCount) {
            throw { statusCode: 400, data: `Unable to add ${name}` }
        }
        res.status(201).send({ data: `Added merchandise ${name} with a price of ${price}` })
    } catch (err) {
        res.status(err.statusCode).send(err.data)
    }
}
const updateMerchandise = async (req, res) => {
    
    /**
     * @type {Merchandise}
     */
    const { name, price } = req.body;
    /**
     * @type {Merchandise}
     */
    const { id } = req.params;
    try {

        const validateMerchandise = validateMerchandiseInput({ id, name, price })
        if (validateMerchandise) {
            throw validateMerchandise
        }
        const hasMerchandise = await queryMerchandise({ id, name });
        if (!hasMerchandise.rows) {
            throw { statusCode: 400, data: `Merchandise Item not found` }
        }
        const result = await db.query('UPDATE merchandise SET name=$1,price=$2 WHERE id = $3', [name, price, id])
        if (!result.rowCount) {
            /**
             * @throws {ThrowException}
             */
            throw { statusCode: 400, data: "Unable to update merchandise" }
        }
        res.status(200).send(`Updated merchandise ${name}`)
    } catch (err) {
        res.status(err.statusCode).send(err.data)
    }
}
const deleteMerchandise = async (req, res) => {
    
    /**
     * @type {Merchandise}
     */
    const { id } = req.params;
    try {
        if (!id) {
            throw {
                statusCode: 400, data: `ID must be provided`
            }
        }
        if (isNaN(id)) {
            throw {
                statusCode: 400, data: `ID must be a Number`
            }
        }
        const hasMerchandise = await queryMerchandise({ id })
        if (!hasMerchandise.rows.length) {
            throw {
                statusCode: 400, data: `No Merchandise Found`
            }
        }
        const result = await db.query('DELETE FROM merchandise WHERE id=$1', [id])
        if (!result.rows) {
            throw { statusCode: 400, data: "Unable to delete merchandise" }
        }
        res.status(200).send("Successfully Deleted merchandise transaction")
    }
    catch (err) {
        res.status(err.statusCode).send(err.data)
    }
}

module.exports = {
    getAllMerchandise,
    getMerchandise,
    createMerchandise,
    updateMerchandise,
    deleteMerchandise, queryMerchandise
}