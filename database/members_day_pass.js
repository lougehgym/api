//@ts-check
const db = require('./db')
//@ts-ignore
const db_types = require('./db_types')
const getMembersWithPass = async (req, res) => {
    try {
        const result = await db.query(`SELECT mem.name,pass.pass_date,pass_cat.category_name from members as mem 
        LEFT join one_day_pass as pass on mem.id = pass.member_id 
            inner join pass_category as pass_cat on pass_cat.id = pass.passcat_id`)
        if (!result.rows) {
            throw { statusCode: 400, data: "No Members with Day Pass found" }
        }
        if (result.rows.length < 1) {

            throw { statusCode: 400, data: "No Members with Day Pass found" }
        }
        res.status(200).send(result.rows)
    } catch (err) {
        res.status(err.statusCode).send(err.data)
    }
}

module.exports = { getMembersWithPass }